﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Check_Override
    {
        public Check_Override() { }
        public string Property_Value { get; set; }
        public bool Override_Found { get; set; }
    }
}