﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public enum ReturnStatusPropertyLookup { Value_ID_Found, Value_ID_Not_Found }
    public class PropertyValueIdLokup
    {
        public ReturnStatusPropertyLookup Status { get; set; }

        private string _ParentValueID;
        public string ParentValueID
        {
            get { return _ParentValueID; }
            set { _ParentValueID = value; }
        }

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }

        private string _PropertyValueDesc;
        public string PropertyValueDesc
        {
            get { return _PropertyValueDesc; }
            set { _PropertyValueDesc = value; }
        }

        private string _PropertyID;
        public string PropertyID
        {
            get { return _PropertyID; }
            set { _PropertyID = value; }
        }

        private string _PropertyValueID;
        public string PropertyValueID
        {
            get { return _PropertyValueID; }
            set { _PropertyValueID = value; }
        }

    }
}