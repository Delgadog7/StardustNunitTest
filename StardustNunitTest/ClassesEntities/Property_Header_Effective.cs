﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Property_Header_Effective
    {
        public Property_Header_Effective() { }
        public String Property_ID { get; set; }
        public String Property_Name { get; set; }
        public short Property_Usage { get; set; }
        public String Property_Usage_Description { get; set; }
        public short Property_Value_Type { get; set; }
        public String Propertry_Value_Type_Description { get; set; }
        public DataTable Lookup_Data_Table { get; set; }
        public bool Show_Property { get; set; }
    }
}