﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class ComponentArray
    {    

        private string _CatalogNumber;
        public string CatalogNumber
        {
            get { return _CatalogNumber; }
            set { _CatalogNumber = value; }
        }

        private string _MaterialTypeID;
        public string MaterialTypeID
        {
            get { return _MaterialTypeID; }
            set { _MaterialTypeID = value; }
        }

        private string _Quantity;
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        private string _QuantityUOM;
        public string QuantityUOM
        {
            get { return _QuantityUOM; }
            set { _QuantityUOM = value; }
        }

    }
}