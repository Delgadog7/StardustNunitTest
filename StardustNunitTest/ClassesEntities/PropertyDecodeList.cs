﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class PropertyDecodeList
    {

        private string _PropertyName;
        public string PropertyName
        {
            get { return _PropertyName; }
            set { _PropertyName = value; }
        }

        private string _PropertyID;
        public string PropertyID
        {
            get { return _PropertyID; }
            set { _PropertyID = value; }
        }

        private string _NameID;
        public string NameID
        {
            get { return _NameID; }
            set { _NameID = value; }
        }

        private string _NameDescription;
        public string NameDescription
        {
            get { return _NameDescription; }
            set { _NameDescription = value; }
        }

        private string _PropertyValue;
        public string PropertyValue
        {
            get { return _PropertyValue; }
            set { _PropertyValue = value; }
        }

        private string _PropertyValueDesc;
        public string PropertyValueDesc
        {
            get { return _PropertyValueDesc; }
            set { _PropertyValueDesc = value; }
        }

        private string _PropertyValueID;
        public string PropertyValueID
        {
            get { return _PropertyValueID; }
            set { _PropertyValueID = value; }
        }

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }

        private string _PropertyNameID;
        public string PropertyNameID
        {
            get { return _PropertyNameID; }
            set { _PropertyNameID = value; }
        }

        private string _PropertyNameDesc;
        public string PropertyNameDesc
        {
            get { return _PropertyNameDesc; }
            set { _PropertyNameDesc = value; }
        }

        private string _LocationSequence;
        public string LocationSequence
        {
            get { return _LocationSequence; }
            set { _LocationSequence = value; }
        }

        private string _LocationID;
        public string LocationID
        {
            get { return _LocationID; }
            set { _LocationID = value; }
        }

        private string _TetherSequence;
        public string TetherSequence
        {
            get { return _TetherSequence; }
            set { _TetherSequence = value; }
        }

        private string _TetherFiberCount;
        public string TetherFiberCount
        {
            get { return _TetherFiberCount; }
            set { _TetherFiberCount = value; }
        }

        private string _TetherFiberSequence;
        public string TetherFiberSequence
        {
            get { return _TetherFiberSequence; }
            set { _TetherFiberSequence = value; }
        }

        private string _TetherFiberSequenceOff;
        public string TetherFiberSequenceOff
        {
            get { return _TetherFiberSequenceOff; }
            set { _TetherFiberSequenceOff = value; }
        }

        private string _FiberID;
        public string FiberID
        {
            get { return _FiberID; }
            set { _FiberID = value; }
        }

        private string _TapSequence;
        public string TapSequence
        {
            get { return _TapSequence; }
            set { _TapSequence = value; }
        }

        private string _FiberSequence;
        public string FiberSequence
        {
            get { return _FiberSequence; }
            set { _FiberSequence = value; }
        }

        private string _COSequence;
        public string COSequence
        {
            get { return _COSequence; }
            set { _COSequence = value; }
        }

        private string _PortNumber;
        public string PortNumber
        {
            get { return _PortNumber; }
            set { _PortNumber = value; }
        }

        private string _TerminationCount;
        public string TerminationCount
        {
            get { return _TerminationCount; }
            set { _TerminationCount = value; }
        }

        private string _TetherFiberAssignMethod;
        public string TetherFiberAssignMethod
        {
            get { return _TetherFiberAssignMethod; }
            set { _TetherFiberAssignMethod = value; }
        }


    }
}