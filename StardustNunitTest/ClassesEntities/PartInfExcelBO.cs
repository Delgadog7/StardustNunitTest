﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkbenchLocalApps.BussinessObject
{
    public class PartInfExcelBO
    {

        private int _IdPI;
        public int IdPI
        {
            get { return _IdPI; }
            set { _IdPI = value; }
        }

        private string _FolioPI;
        public string FolioPI
        {
            get { return _FolioPI; }
            set { _FolioPI = value; }
        }

        private string _OU;
        public string OU
        {
            get { return _OU; }
            set { _OU = value; }
        }

        private string _Division;
        public string Division
        {
            get { return _Division; }
            set { _Division = value; }
        }

        private string _MatNumPI;
        public string MatNumPI
        {
            get { return _MatNumPI; }
            set { _MatNumPI = value; }
        }

        private string _CorningPN;
        public string CorningPN
        {
            get { return _CorningPN; }
            set { _CorningPN = value; }
        }

        private string _MatDesc;
        public string MatDesc
        {
            get { return _MatDesc; }
            set { _MatDesc = value; }
        }

       

        private string _UnitPI;
        public string UnitPI
        {
            get { return _UnitPI; }
            set { _UnitPI = value; }
        }

        private double _AnnualVolumen;
        public double AnnualVolumen
        {
            get { return _AnnualVolumen; }
            set { _AnnualVolumen = value; }
        }
    }



    public class ListEmployeeBO
    {

        private int _IdLE;
        public int IdLE
        {
            get { return _IdLE; }
            set { _IdLE = value; }
        }

        private string _TypeReq;
        public string TypeReq
        {
            get { return _TypeReq; }
            set { _TypeReq = value; }
        }

        private string _FolioLE;
        public string FolioLE
        {
            get { return _FolioLE; }
            set { _FolioLE = value; }
        }

        private string _NameEm;
        public string NameEm
        {
            get { return _NameEm; }
            set { _NameEm = value; }
        }
    }
}