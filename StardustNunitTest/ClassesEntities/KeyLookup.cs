﻿namespace StardustCoreLibs.App_Code.Classes
{
    public class KeyLookup
    {

        private string _ConfigurationID;
        public string ConfigurationID
        {
            get { return _ConfigurationID; }
            set { _ConfigurationID = value; }
        }

        private string _Manufacturing_Build_ID;
        public string Manufacturing_Build_ID
        {
            get { return _Manufacturing_Build_ID; }
            set { _Manufacturing_Build_ID = value; }
        }

        private string _Manufacturing_Build_Instance_ID;
        public string Manufacturing_Build_Instance_ID
        {
            get { return _Manufacturing_Build_Instance_ID; }
            set { _Manufacturing_Build_Instance_ID = value; }
        }

        private string _EventTypeID;
        public string EventTypeID
        {
            get { return _EventTypeID; }
            set { _EventTypeID = value; }
        }

        private string _EventTypeValueID;
        public string EventTypeValueID
        {
            get { return _EventTypeValueID; }
            set { _EventTypeValueID = value; }
        }

        private string _StreetName;
        public string StreetName
        {
            get { return _StreetName; }
            set { _StreetName = value; }
        }

        private string _CityName;
        public string CityName
        {
            get { return _CityName; }
            set { _CityName = value; }
        }

        private string _StateProvinceCountryName;
        public string StateProvinceCountryName
        {
            get { return _StateProvinceCountryName; }
            set { _StateProvinceCountryName = value; }
        }

        private string _ContactID;
        public string ContactID
        {
            get { return _ContactID; }
            set { _ContactID = value; }
        }

        private string _ContactName;
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        private string _ContactPhone;
        public string ContactPhone
        {
            get { return _ContactPhone; }
            set { _ContactPhone = value; }
        }

        private string _ContactEmail;
        public string ContactEmail
        {
            get { return _ContactEmail; }
            set { _ContactEmail = value; }
        }

        private string _BuildUOM;
        public string BuildUOM
        {
            get { return _BuildUOM; }
            set { _BuildUOM = value; }
        }

        private string _LocationSequence;
        public string LocationSequence
        {
            get { return _LocationSequence; }
            set { _LocationSequence = value; }
        }

        private string _BuildLength;
        public string BuildLength
        {
            get { return _BuildLength; }
            set { _BuildLength = value; }
        }

        private string _BuildLengthFeet;
        public string BuildLengthFeet
        {
            get { return _BuildLengthFeet; }
            set { _BuildLengthFeet = value; }
        }

        private string _BuildLengthMeters;
        public string BuildLengthMeters
        {
            get { return _BuildLengthMeters; }
            set { _BuildLengthMeters = value; }
        }

        private string _LocationCount;
        public string LocationCount
        {
            get { return _LocationCount; }
            set { _LocationCount = value; }
        }

        private string _TAPCount;
        public string TAPCount
        {
            get { return _TAPCount; }
            set { _TAPCount = value; }
        }

        private string _TetherCount;
        public string TetherCount
        {
            get { return _TetherCount; }
            set { _TetherCount = value; }
        }

        private string _AssignedFiberCount;
        public string AssignedFiberCount
        {
            get { return _AssignedFiberCount; }
            set { _AssignedFiberCount = value; }
        }

        private string _BuildDescription;
        public string BuildDescription
        {
            get { return _BuildDescription; }
            set { _BuildDescription = value; }
        }

        private string _BuildStatus;
        public string BuildStatus
        {
            get { return _BuildStatus; }
            set { _BuildStatus = value; }
        }

        private string _PropertyDescription;
        public string PropertyDescription
        {
            get { return _PropertyDescription; }
            set { _PropertyDescription = value; }
        }

        private string _PropertyValueDescription;
        public string PropertyValueDescription
        {
            get { return _PropertyValueDescription; }
            set { _PropertyValueDescription = value; }
        }


    }
}