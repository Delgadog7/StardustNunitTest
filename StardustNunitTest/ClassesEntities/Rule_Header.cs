﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Rule_Header
    {
        public Rule_Header() { }
        public string Rule_ID { get;set; }
        public string Rule_Description { get;set;}
    }
}