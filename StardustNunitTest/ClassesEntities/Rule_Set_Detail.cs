﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Rule_Set_Detail
    {
        public Rule_Set_Detail() { }
        public string Rule_Set_ID { get;set; }
        public int Evaluation_Sequence { get;set; }
        public string Rule_ID { get; set; }
    }
}