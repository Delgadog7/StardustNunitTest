﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class ReportData
    {

        private string _EffectiveDate;
        public string EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }

        private string _ReportTitleText;
        public string ReportTitleText
        {
            get { return _ReportTitleText; }
            set { _ReportTitleText = value; }
        }

        private string _DateValue;
        public string DateValue
        {
            get { return _DateValue; }
            set { _DateValue = value; }
        }

        private string _ReportNameText;
        public string ReportNameText
        {
            get { return _ReportNameText; }
            set { _ReportNameText = value; }
        }

        private string _SAPOrderText;
        public string SAPOrderText
        {
            get { return _SAPOrderText; }
            set { _SAPOrderText = value; }
        }

        private string _PageText;
        public string PageText
        {
            get { return _PageText; }
            set { _PageText = value; }
        }

        private string _BuildIDText;
        public string BuildIDText
        {
            get { return _BuildIDText; }
            set { _BuildIDText = value; }
        }

        private string _OrderNumber;
        public string OrderNumber
        {
            get { return _OrderNumber; }
            set { _OrderNumber = value; }
        }

        private string _BuildID;
        public string BuildID
        {
            get { return _BuildID; }
            set { _BuildID = value; }
        }

        private string _BuildIDBarcode;
        public string BuildIDBarcode
        {
            get { return _BuildIDBarcode; }
            set { _BuildIDBarcode = value; }
        }

        private string _BuildDescription;
        public string BuildDescription
        {
            get { return _BuildDescription; }
            set { _BuildDescription = value; }
        }

        private string _BuildFooter;
        public string BuildFooter
        {
            get { return _BuildFooter; }
            set { _BuildFooter = value; }
        }

        private string _StreetNameText;
        public string StreetNameText
        {
            get { return _StreetNameText; }
            set { _StreetNameText = value; }
        }

        private string _StreetNameValue;
        public string StreetNameValue
        {
            get { return _StreetNameValue; }
            set { _StreetNameValue = value; }
        }

        private string _PropertyDescription;
        public string PropertyDescription
        {
            get { return _PropertyDescription; }
            set { _PropertyDescription = value; }
        }

        private string _PropertyValueDescription;
        public string PropertyValueDescription
        {
            get { return _PropertyValueDescription; }
            set { _PropertyValueDescription = value; }
        }

        private Boolean _CO_Preterm_Present;
        public Boolean CO_Preterm_Present
        {
            get { return _CO_Preterm_Present; }
            set { _CO_Preterm_Present = value; }
        }

        private Boolean _Field_Preterm_Present;
        public Boolean Field_Preterm_Present
        {
            get { return _Field_Preterm_Present; }
            set { _Field_Preterm_Present = value; }
        }

        private string _CO_End_Installation;
        public string CO_End_Installation
        {
            get { return _CO_End_Installation; }
            set { _CO_End_Installation = value; }
        }

        private string _Field_End_Installation;
        public string Field_End_Installation
        {
            get { return _Field_End_Installation; }
            set { _Field_End_Installation = value; }
        }

    }
}