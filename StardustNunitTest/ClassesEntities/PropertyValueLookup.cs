﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{ 
    public enum PropertyValueLookupReturn { Value_ID_Found, Value_ID_Not_Found }
    public enum Object_Type { Mfg_Build_Instance, Mfg_Build, Build, Undetermined }
    public enum LocalOverrideFound { True, False }
    public enum Function_Found { Yes, No }
    public enum ReturnStatusProp { Property_Value_Set, Property_Value_Not_Set }

    public class PropertyValueLookup
    {
        public ReturnStatusProp StatProp { get; set; }
        public PropertyValueLookupReturn Status { get; set; }

        public Object_Type ObjectT { get; set; }

        public LocalOverrideFound Override { get; set; }

        public Function_Found FunctionF { get; set; }

        private string _ConfigurationID;
        public string ConfigurationID
        {
            get { return _ConfigurationID; }
            set { _ConfigurationID = value; }
        }

        private Boolean _OverwriteFlag;
        public Boolean OverwriteFlag
        {
            get { return _OverwriteFlag; }
            set { _OverwriteFlag = value; }
        }

        private string _FunctionValue;
        public string FunctionValue
        {
            get { return _FunctionValue; }
            set { _FunctionValue = value; }
        }

        private string _PropertyUsage;
        public string PropertyUsage
        {
            get { return _PropertyUsage; }
            set { _PropertyUsage = value; }
        }

        private string _PropertyValueText;
        public string PropertyValueText
        {
            get { return _PropertyValueText; }
            set { _PropertyValueText = value; }
        }

        private int _PropertyValueNumber;
        public int PropertyValueNumber
        {
            get { return _PropertyValueNumber; }
            set { _PropertyValueNumber = value; }
        }

        private string _PropertyValueType;
        public string PropertyValueType
        {
            get { return _PropertyValueType; }
            set { _PropertyValueType = value; }
        }

        private string _CategoryType;
        public string CategoryType
        {
            get { return _CategoryType; }
            set { _CategoryType = value; }
        }

        private string _OverridePropertyValue;
        public string OverridePropertyValue
        {
            get { return _OverridePropertyValue; }
            set { _OverridePropertyValue = value; }
        }

        private string _ManufacturingBuildID;
        public string ManufacturingBuildID
        {
            get { return _ManufacturingBuildID; }
            set { _ManufacturingBuildID = value; }
        }

        private string _ManufacturingBuildInstanceID;
        public string ManufacturingBuildInstanceID
        {
            get { return _ManufacturingBuildInstanceID; }
            set { _ManufacturingBuildInstanceID = value; }
        }

        private string _LocationSequence;
        public string LocationSequence
        {
            get { return _LocationSequence; }
            set { _LocationSequence = value; }
        }

        private string _TapSequence;
        public string TapSequence
        {
            get { return _TapSequence; }
            set { _TapSequence = value; }
        }

        private string _TetherSequence;
        public string TetherSequence
        {
            get { return _TetherSequence; }
            set { _TetherSequence = value; }
        }

        private string _FiberSequence;
        public string FiberSequence
        {
            get { return _FiberSequence; }
            set { _FiberSequence = value; }
        }

        private string _CategoryID;
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }

        private string _PropertyID;
        public string PropertyID
        {
            get { return _PropertyID; }
            set { _PropertyID = value; }
        }

        private string _AllowOverrideValueID;
        public string AllowOverrideValueID
        {
            get { return _AllowOverrideValueID; }
            set { _AllowOverrideValueID = value; }
        }

        private string _BasePropertyID;
        public string BasePropertyID
        {
            get { return BasePropertyID; }
            set { _BasePropertyID = value; }
        }

        private string _PropertyValue;
        public string PropertyValue
        {
            get { return PropertyValue; }
            set { _PropertyValue = value; }
        }

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return EffectiveDate; }
            set { _EffectiveDate = value; }
        }

        private string _ItemSequence;
        public string ItemSequence
        {
            get { return _ItemSequence; }
            set { _ItemSequence = value; }
        }

        private string _LegSequence;
        public string LegSequence
        {
            get { return _LegSequence; }
            set { _LegSequence = value; }
        }

        private string _FiberDirection;
        public string FiberDirection
        {
            get { return _FiberDirection; }
            set { _FiberDirection = value; }
        }

        private string _HeaderLookupDataTable;
        public string HeaderLookupDataTable
        {
            get { return _HeaderLookupDataTable; }
            set { _HeaderLookupDataTable = value; }
        }

        private string _HeaderPropertyID;
        public string HeaderPropertyID
        {
            get { return _HeaderPropertyID; }
            set { _HeaderPropertyID = value; }
        }

        private string _HeaderPropertyName;
        public string HeaderPropertyName
        {
            get { return _HeaderPropertyName; }
            set { _HeaderPropertyName = value; }
        }

        private string _HeaderPropertyUsage;
        public string HeaderPropertyUsage
        {
            get { return _HeaderPropertyUsage; }
            set { _HeaderPropertyUsage = value; }
        }

        private string _HeaderPropertyUsageDesc;
        public string HeaderPropertyUsageDesc
        {
            get { return _HeaderPropertyUsageDesc; }
            set { _HeaderPropertyUsageDesc = value; }
        }

        private string _HeaderPropertyValueType;
        public string HeaderPropertyValueType
        {
            get { return _HeaderPropertyValueType; }
            set { _HeaderPropertyValueType = value; }
        }

        private string _HeaderPropertyValueTypeDesc;
        public string HeaderPropertyValueTypeDesc
        {
            get { return _HeaderPropertyValueTypeDesc; }
            set { _HeaderPropertyValueTypeDesc = value; }
        }

        private string _HeaderShowProperty;
        public string HeaderShowProperty
        {
            get { return _HeaderShowProperty; }
            set { _HeaderShowProperty = value; }
        }

    }
}