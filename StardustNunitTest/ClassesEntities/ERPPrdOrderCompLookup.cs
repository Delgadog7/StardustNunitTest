﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class ERPPrdOrderComponentLookup
    {

        private string _CatalogNumber;
        public string CatalogNumber
        {
            get { return _CatalogNumber; }
            set { _CatalogNumber = value; }
        }

        private string _Quantity;
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        private string _Quantity_UOM;
        public string Quantity_UOM
        {
            get { return _Quantity_UOM; }
            set { _Quantity_UOM = value; }
        }

        private string _MaterialTypeID;
        public string MaterialTypeID
        {
            get { return _MaterialTypeID; }
            set { _MaterialTypeID = value; }
        }


    }
}