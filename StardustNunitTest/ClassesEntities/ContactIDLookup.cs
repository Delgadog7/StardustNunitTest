﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class ContactIDLookup
    {

        private string _SerialNumber;
        public string SerialNumber
        {
            get { return _SerialNumber; }
            set { _SerialNumber = value; }
        }

        private string _ContactName;
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        private string _ContactPhone;
        public string ContactPhone
        {
            get { return _ContactPhone; }
            set { _ContactPhone = value; }
        }

        private string _ContactEmail;
        public string ContactEmail
        {
            get { return _ContactEmail; }
            set { _ContactEmail = value; }
        }

        private string _ContactID;
        public string ContactID
        {
            get { return _ContactID; }
            set { _ContactID = value; }
        }


    }
}