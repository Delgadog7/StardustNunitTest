﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Get_Property
    {
        public enum Status { Property_Value_Found, Property_Value_Not_Found }
        public Get_Property() { }
        public string Property_Value { get; set; }
        public bool Property_Value_Found { get; set; }
        public Status Return_Status { get; set; }
    }
}