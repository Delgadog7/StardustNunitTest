﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class GenerateMeasurementText
    {

        private string _NumericValue;
        public string NumericValue
        {
            get { return _NumericValue; }
            set { _NumericValue = value; }
        }

        private string _UnitType;
        public string UnitType
        {
            get { return _UnitType; }
            set { _UnitType = value; }
        }

        private string _LanguageID;
        public string LanguageID
        {
            get { return _LanguageID; }
            set { _LanguageID = value; }
        }

        private string _MeasurementText;
        public string MeasurementText
        {
            get { return _MeasurementText; }
            set { _MeasurementText = value; }
        }

    }
}