﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Category_Property_XRef
    {
        public Category_Property_XRef() { }
        public int Instance_Index { get; set; }
        public string Category_ID { get; set; }
        public string Property_ID { get; set; }
        public DateTime? Beginning_Date { get; set; }
        public DateTime? Ending_Date { get; set; }
        public bool Record_Active { get; set; }
        public DateTime? Maintenance_Timestamp { get; set; }
        public string Maintenance_User { get; set; }

    }
}