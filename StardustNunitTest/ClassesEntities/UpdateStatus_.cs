﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class UpdateStatus_
    {

        private string _StatusKey1;
        public string StatusKey1
        {
            get { return _StatusKey1; }
            set { _StatusKey1 = value; }
        }

        private string _StatusKey2;
        public string StatusKey2
        {
            get { return _StatusKey2; }
            set { _StatusKey2 = value; }
        }

        private string _StatusKey3;
        public string StatusKey3
        {
            get { return _StatusKey3; }
            set { _StatusKey3 = value; }
        }

        private string _StatusKey4;
        public string StatusKey4
        {
            get { return _StatusKey4; }
            set { _StatusKey4 = value; }
        }

        private string _EventTypeID;
        public string EventTypeID
        {
            get { return _EventTypeID; }
            set { _EventTypeID = value; }
        }

        private string _EventTypeValueID;
        public string EventTypeValueID
        {
            get { return _EventTypeValueID; }
            set { _EventTypeValueID = value; }
        }

        private string _UserID;
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _LocalDeliveryNumber;
        public string LocalDeliveryNumber
        {
            get { return _LocalDeliveryNumber; }
            set { _LocalDeliveryNumber = value; }
        }

        private int _LocalInstanceIndex;
        public int LocalInstanceIndex
        {
            get { return _LocalInstanceIndex; }
            set { _LocalInstanceIndex = value; }
        }

        private int _LocalSalesItemNumber;
        public int LocalSalesItemNumber
        {
            get { return _LocalSalesItemNumber; }
            set { _LocalSalesItemNumber = value; }
        }

        private string _LocalSalesOrderNumber;
        public string LocalSalesOrderNumber
        {
            get { return _LocalSalesOrderNumber; }
            set { _LocalSalesOrderNumber = value; }
        }

        private string _LocalProductionOrderNumber;
        public string LocalProductionOrderNumber
        {
            get { return _LocalProductionOrderNumber; }
            set { _LocalProductionOrderNumber = value; }
        }

        private Boolean _ReturnStatus;
        public Boolean ReturnStatus
        {
            get { return _ReturnStatus; }
            set { _ReturnStatus = value; }
        }

        private Boolean _DataTransfer;
        public Boolean DataTransfer
        {
            get { return _DataTransfer; }
            set { _DataTransfer = value; }
        }

        private string _LocalPartnerID;
        public string LocalPartnerID
        {
            get { return _LocalPartnerID; }
            set { _LocalPartnerID = value; }
        }

        private string _LocalPartnerOrderNumber;
        public string LocalPartnerOrderNumber
        {
            get { return _LocalPartnerOrderNumber; }
            set { _LocalPartnerOrderNumber = value; }
        }

        private string _LocalCustomerBuildID;
        public string LocalCustomerBuildID
        {
            get { return _LocalCustomerBuildID; }
            set { _LocalCustomerBuildID = value; }
        }

        private string _LocalMaterialID;
        public string LocalMaterialID
        {
            get { return _LocalMaterialID; }
            set { _LocalMaterialID = value; }
        }

        private string _LocalManufacturingBuildInstanceID;
        public string LocalManufacturingBuildInstanceID
        {
            get { return _LocalManufacturingBuildInstanceID; }
            set { _LocalManufacturingBuildInstanceID = value; }
        }

        private string _LocalConfigurationID;
        public string LocalConfigurationID
        {
            get { return _LocalConfigurationID; }
            set { _LocalConfigurationID = value; }
        }

        private string _SiteID;
        public string SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }
    }
}