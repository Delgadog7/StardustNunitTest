﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Build_Header
    {
        public Build_Header() { }

        public string Configuration_ID { get; set; }
        public string Customer_Build_ID { get; set; }
        public int Customer_Build_Revision { get; set; }
        public string Build_Description { get; set; }
        public int Event_Type_Value_ID { get; set; }
    }
}