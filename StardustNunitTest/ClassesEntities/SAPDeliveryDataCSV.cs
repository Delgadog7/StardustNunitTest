﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class SAPDeliveryDataCSV
    {

        private string _DeliveryNumber;
        public string DeliveryNumber
        {
            get { return _DeliveryNumber; }
            set { _DeliveryNumber = value; }
        }

        private string _ItemNumber;
        public string ItemNumber
        {
            get { return _ItemNumber; }
            set { _ItemNumber = value; }
        }

        private string _SalesOrderNumber;
        public string SalesOrderNumber
        {
            get { return _SalesOrderNumber; }
            set { _SalesOrderNumber = value; }
        }

        private string _SalesItemNumber;
        public string SalesItemNumber
        {
            get { return _SalesItemNumber; }
            set { _SalesItemNumber = value; }
        }

        private string _ProNumber;
        public string ProNumber
        {
            get { return _ProNumber; }
            set { _ProNumber = value; }
        }

        private string _CustomerNumber;
        public string CustomerNumber
        {
            get { return _CustomerNumber; }
            set { _CustomerNumber = value; }
        }

        private string _CustomerName;
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; }
        }

        private string _ShipToAddress1;
        public string ShipToAddress1
        {
            get { return _ShipToAddress1; }
            set { _ShipToAddress1 = value; }
        }

        private string _ShipToAddress2;
        public string ShipToAddress2
        {
            get { return _ShipToAddress2; }
            set { _ShipToAddress2 = value; }
        }

        private string _ShipToCity;
        public string ShipToCity
        {
            get { return _ShipToCity; }
            set { _ShipToCity = value; }
        }
        
        private string _ShipToState;
        public string ShipToState
        {
            get { return _ShipToState; }
            set { _ShipToState = value; }
        }

        private string _ShipToZip;
        public string ShipToZip
        {
            get { return _ShipToZip; }
            set { _ShipToZip = value; }
        }

        private string _ShipToCountry;
        public string ShipToCountry
        {
            get { return _ShipToCountry; }
            set { _ShipToCountry = value; }
        }

        private string _ShipToPOBox;
        public string ShipToPOBox
        {
            get { return _ShipToPOBox; }
            set { _ShipToPOBox = value; }
        }

        private string _ShipToAddress3;
        public string ShipToAddress3
        {
            get { return _ShipToAddress3; }
            set { _ShipToAddress3 = value; }
        }

        private string _CustomerOrderNumber;
        public string CustomerOrderNumber
        {
            get { return _CustomerOrderNumber; }
            set { _CustomerOrderNumber = value; }
        }

        private string _CarrierID;
        public string CarrierID
        {
            get { return _CarrierID; }
            set { _CarrierID = value; }
        }

        private string _CarrierName;
        public string CarrierName
        {
            get { return _CarrierName; }
            set { _CarrierName = value; }
        }

        private string _PackageWeightText;
        public string PackageWeightText
        {
            get { return _PackageWeightText; }
            set { _PackageWeightText = value; }
        }

        private string _BOLNumber;
        public string BOLNumber
        {
            get { return _BOLNumber; }
            set { _BOLNumber = value; }
        }

        private string _InstanceIndex;
        public string InstanceIndex
        {
            get { return _InstanceIndex; }
            set { _InstanceIndex = value; }
        }

        private string _DeliveryInstanceIndex;
        public string DeliveryInstanceIndex
        {
            get { return _DeliveryInstanceIndex; }
            set { _DeliveryInstanceIndex = value; }
        }

        private string _SalesInstanceIndex;
        public string SalesInstanceIndex
        {
            get { return _SalesInstanceIndex; }
            set { _SalesInstanceIndex = value; }
        }

        private string _KeyCustomerIDValue;
        public string KeyCustomerIDValue
        {
            get { return _KeyCustomerIDValue; }
            set { _KeyCustomerIDValue = value; }
        }

    }
}