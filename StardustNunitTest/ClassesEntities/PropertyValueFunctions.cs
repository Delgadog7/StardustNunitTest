﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes { 
    public enum ReturnStatusPVF { Property_Value_Found, Property_Value_Not_Found }

    public enum ReturnStatusSetPVF { Property_Value_Set, Property_Value_Not_Set }
    public class PropertyValueFunctions
    {
        public ReturnStatusAdd Status { get; set; }

        public ReturnStatusProp StatProp { get; set; }

        private string _PropertyID;
        public string PropertyID
        {
            get { return _PropertyID; }
            set { _PropertyID = value; }
        }

        private string _ConfigurationID;
        public string ConfigurationID
        {
            get { return _ConfigurationID; }
            set { _ConfigurationID = value; }
        }

        private string _ManufacturingBuildID;
        public string ManufacturingBuildID
        {
            get { return _ManufacturingBuildID; }
            set { _ManufacturingBuildID = value; }
        }

        private string _ManufacturingBuildInstanceID;
        public string ManufacturingBuildInstanceID
        {
            get { return _ManufacturingBuildInstanceID; }
            set { _ManufacturingBuildInstanceID = value; }
        }

        private string _LocationSequence;
        public string LocationSequence
        {
            get { return _LocationSequence; }
            set { _LocationSequence = value; }
        }

        private string _TAPSequence;
        public string TAPSequence
        {
            get { return _TAPSequence; }
            set { _TAPSequence = value; }
        }

        private string _TetherSequence;
        public string TetherSequence
        {
            get { return _TetherSequence; }
            set { _TetherSequence = value; }
        }

        private string _FiberSequence;
        public string FiberSequence
        {
            get { return _FiberSequence; }
            set { _FiberSequence = value; }
        }

        private string _Category_ID;
        public string Category_ID
        {
            get { return _Category_ID; }
            set { _Category_ID = value; }
        }

        private string _Property_ID;
        public string Property_ID
        {
            get { return _Property_ID; }
            set { _Property_ID = value; }
        }

        private string _Property_Value;
        public string Property_Value
        {
            get { return _Property_Value; }
            set { _Property_Value = value; }
        }

        private bool _Overwrite_Flag;
        public bool Overwrite_Flag
        {
            get { return _Overwrite_Flag; }
            set { _Overwrite_Flag = value; }
        }

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }

    }
}