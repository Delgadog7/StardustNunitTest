﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class SendNotification
    {

        private string _NotificationKey1;
        public string NotificationKey1
        {
            get { return _NotificationKey1; }
            set { _NotificationKey1 = value; }
        }

        private string _NotificationKey2;
        public string NotificationKey2
        {
            get { return _NotificationKey2; }
            set { _NotificationKey2 = value; }
        }

        private string _NotificationKey3;
        public string NotificationKey3
        {
            get { return _NotificationKey3; }
            set { _NotificationKey3 = value; }
        }

        private string _NotificationKey4;
        public string NotificationKey4
        {
            get { return _NotificationKey4; }
            set { _NotificationKey4 = value; }
        }

        private string _NotificationGroup;
        public string NotificationGroup
        {
            get { return _NotificationGroup; }
            set { _NotificationGroup = value; }
        }

        private string _NotificationText;
        public string NotificationText
        {
            get { return _NotificationText; }
            set { _NotificationText = value; }
        }

        private string _UserID;
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
    }
}