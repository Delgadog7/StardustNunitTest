﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Category_Property
    {
        public Category_Property() { }
        public Category_Property_XRef Category_Property_XRef_Row { get; set; }
        public bool Category_Property_XRef_Found { get; set; }
    }
}