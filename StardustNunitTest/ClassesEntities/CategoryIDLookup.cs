﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public enum ReturnStatus { Category_Found, Category_Not_Found }
    public class CategoryIDLookup
    {
        public ReturnStatus Status { get; set; }

        private string _CategoryID;
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
       
    }
}