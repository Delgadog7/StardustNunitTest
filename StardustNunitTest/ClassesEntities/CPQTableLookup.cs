﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public enum TableReturnStatus { DB_Table_Found, DB_Table_Not_Found }
    public class CPQTableLookup
    {
        public TableReturnStatus Status { get; set; }

        private string _DBNAME;
        public string DBNAME
        {
            get { return _DBNAME; }
            set { _DBNAME = value; }
        }
    }
}