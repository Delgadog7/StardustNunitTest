﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class SiteIDLookup
    {
        private string _ConfigurationID;
        public string ConfigurationID
        {
            get { return _ConfigurationID; }
            set { _ConfigurationID = value; }
        }

        private string _LocalSiteID;
        public string LocalSiteID
        {
            get { return _LocalSiteID; }
            set { _LocalSiteID = value; }
        }


        private string _SiteID;
        public string SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }


        private string _BuildAssignmentRS;
        public string BuildAssignmentRS
        {
            get { return _BuildAssignmentRS; }
            set { _BuildAssignmentRS = value; }
        }


        private string _MfgBuildHeaderRS;

        public string MfgBuildHeaderRS
        {
            get { return _MfgBuildHeaderRS; }
            set { _MfgBuildHeaderRS = value; }
        }


    }
}