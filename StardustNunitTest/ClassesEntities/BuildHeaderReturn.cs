﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{

    public enum BuildHeader { Header_Found, Header_Not_Found }
    public class BuildHeaderReturn
    {
 
        public BuildHeader Status { get; set; }

        private string _XMLRootString;
        public string XMLRootString
        {
            get { return _XMLRootString; }
            set { _XMLRootString = value; }
        }

        private string _DocVersion;
        public string DocVersion
        {
            get { return _DocVersion; }
            set { _DocVersion = value; }
        }

    }
}