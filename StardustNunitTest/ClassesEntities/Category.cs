﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Category
    { 
        public Category() { }
        public Category_Effective Category_Row { get; set; }
        public bool Category_Found { get; set; }
    }
}