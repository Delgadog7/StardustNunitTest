﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class PropVal_Customer
    {
        public int Instance_Index { get; set; }
        public string Property_Value_ID { get; set; }
        public string Customer_Market_ID { get; set; }
        public string Customer_Language_ID { get; set; }
        public short? Customer_Type { get; set; }
        public bool Ordering_Thru_Partner { get; set; }
        public string Partner_ID { get; set; }
        public bool Ordering_By_Components { get; set; }
        public bool Waiver_Allowed { get; set; }
        public bool Send_Waiver_To_Contact { get; set; }
        public bool Require_Reel_Tracking { get; set; }
        public DateTime? Beginning_Date { get; set; }
        public DateTime? Ending_Date { get; set; }
        public bool Record_Active { get; set; }
        public DateTime? Maintenance_Timestamp { get; set; }
        public string Maintenance_User { get; set; }
    }
}