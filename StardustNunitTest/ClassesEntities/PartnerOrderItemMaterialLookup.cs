﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class PartnerOrderItemMaterialLookup
    {

        private string _MaterialID;
        public string MaterialID
        {
            get { return _MaterialID; }
            set { _MaterialID = value; }
        }

        private string _Quantity;
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        private string _Quantity_UOM;
        public string Quantity_UOM
        {
            get { return _Quantity_UOM; }
            set { _Quantity_UOM = value; }
        }



    }
}