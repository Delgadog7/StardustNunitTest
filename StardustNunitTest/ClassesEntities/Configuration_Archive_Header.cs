﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Configuration_Archive_Header
    {
        public Configuration_Archive_Header() { }
        public string ConfigurationID { get; set; } = string.Empty;
        public string FileName { get; set; }
    }
}