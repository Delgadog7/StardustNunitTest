﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class ExternalAssignment
    {

        private string _CustomerBuildID;
        public string CustomerBuildID
        {
            get { return _CustomerBuildID; }
            set { _CustomerBuildID = value; }
        }

        private string _SiteID;
        public string SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }

        private string _ActiveConfigurationID;
        public string ActiveConfigurationID
        {
            get { return _ActiveConfigurationID; }
            set { _ActiveConfigurationID = value; }
        }

    }
}