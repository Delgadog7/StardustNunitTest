﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Rule_Set_Header
    {
        public Rule_Set_Header() { }
        public string Rule_Set_ID { get; set; }
        public string Rule_Set_Description { get; set; }
    }
}