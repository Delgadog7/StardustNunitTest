﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public enum ReturnStatusCatalogL { Successful, Unsuccessful }
    public class CatalogNumberLookup
    {
        public ReturnStatusCatalogL returnstatuscatalogl;

        private string _BuildCatalogNumber;
        public string BuildCatalogNumber
        {
            get { return _BuildCatalogNumber; }
            set { _BuildCatalogNumber = value; }
        }
    }
}