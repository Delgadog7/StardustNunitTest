﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public enum ReturnStatusID { Property_ID_Found, Property_ID_Not_Found }
    public class PropertyIDLookup
    {
        public ReturnStatusID Status { get; set; }

        private string _PropertyID;
        public string PropertyID
        {
            get { return _PropertyID; }
            set { _PropertyID = value; }
        }
    }
}