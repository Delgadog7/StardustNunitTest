﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;
using Microsoft.Win32.SafeHandles;

public class FileShareImpersonator : IDisposable
{
    /// <summary>
    ///     The Impersonator class is used to access a network share with other credentials.
    /// </summary>
    // private readonly WindowsImpersonationContext _impersonatedUser;

    // private readonly IntPtr _userHandle;
    private readonly SafeAccessTokenHandle _userHandle = null;

    /// <summary>
    ///     Constructor
    /// </summary>
    /// <param name="username">The user of the network share</param>
    /// <param name="password">The password of the network share</param>
    public FileShareImpersonator(string username, string password, string userDomain = "NA")
    {
        bool returnValue = LogonUser(username, userDomain, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT,
                                     out _userHandle);
        if (!returnValue)
            throw new ApplicationException(
                "The applications wasn't able to connect the user with the specified credentials!");
        // var newId = new WindowsIdentity(_userHandle);
        // _impersonatedUser = newId.Impersonate();
        WindowsIdentity.RunImpersonated(_userHandle, () => { });
    }

    #region IDisposable Members

    public void Dispose()
    {
        // if (_impersonatedUser != null)
        // {
        //     _impersonatedUser.Undo();
        //     CloseHandle(_userHandle);
        // }
    }

    #endregion

    #region Interop imports/constants

    public const int LOGON32_LOGON_INTERACTIVE = 2;
    public const int LOGON32_LOGON_SERVICE = 3;
    public const int LOGON32_PROVIDER_DEFAULT = 0;

    [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        int dwLogonType, int dwLogonProvider, out SafeAccessTokenHandle phToken);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public static extern bool CloseHandle(IntPtr handle);

    #endregion
}