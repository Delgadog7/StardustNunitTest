﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Partner_Order_Item
    {
        public Partner_Order_Item() { }
        public string Partner_ID { get; set; }
        public string Partner_Order_Number { get; set; }
        public string Customer_Build_ID { get; set; }
        public short? Event_Type_Value_ID { get; set; }
    }
}