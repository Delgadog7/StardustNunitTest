﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Add_Audit_Log_EntryOB
    {

        private string _AuditKey1;
        public string AuditKey1
        {
            get { return _AuditKey1; }
            set { _AuditKey1 = value; }
        }

        private string _AuditKey2;
        public string AuditKey2
        {
            get { return _AuditKey2; }
            set { _AuditKey2 = value; }
        }

        private string _AuditKey3;
        public string AuditKey3
        {
            get { return _AuditKey3; }
            set { _AuditKey3 = value; }
        }

        private string _AuditKey4;
        public string AuditKey4
        {
            get { return _AuditKey4; }
            set { _AuditKey4 = value; }
        }

        private string _EventTypeID;
        public string EventTypeID
        {
            get { return _EventTypeID; }
            set { _EventTypeID = value; }
        }

        private string _EventTypeValueID;
        public string EventTypeValueID
        {
            get { return _EventTypeValueID; }
            set { _EventTypeValueID = value; }
        }

        private string _UserID;
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _AuditData;
        public string AuditData
        {
            get { return _AuditData; }
            set { _AuditData = value; }
        }

    }
}