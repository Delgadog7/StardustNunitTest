﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Property_Header
    {
        public Property_Header() { }
        public int Instance_Index { get; set; }
        public string Property_ID { get; set; }
        public string Property_Name { get; set; }
        public short? Property_Usage { get; set; }
        public short? Property_Value_Type { get; set; }
        public string Lookup_Data_Table { get; set; }
        public bool Show_Property { get; set; }
        public DateTime? Beginning_Date { get; set; }
        public DateTime? Ending_Date { get; set; }
        public bool Record_Active { get; set; }
        public DateTime? Maintenance_Timestamp { get; set; }
        public string Maintenance_User { get; set; }

    }
}