﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Partner_Order
    {
        public Partner_Order() { }
        public string Partner_ID { get; set; }
        public string Partner_Order_Number { get; set; }
        public string Customer_ID { get; set; }
        public string Customer_Order_Number { get; set; }
        public string Ship_to_Address { get; set; }
        public string Ship_To_City { get; set; }
        public string Ship_to_State_Province_ID { get; set; }
        public string Ship_to_Country_ID { get; set; }
        public DateTime? Date_Created { get; set; }
        public DateTime? Date_Entered { get; set; }
        public DateTime? Date_Requested { get; set; }
        public short? Event_Type_Value_ID { get; set; }
    }
}