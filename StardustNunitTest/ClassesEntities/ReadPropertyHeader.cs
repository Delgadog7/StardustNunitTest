﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{

    public enum ReturnStatusHeader { Property_Header_Found, Property_Header_Not_Found }
    public class ReadPropertyHeader
    {
 
            public ReturnStatusHeader Status { get; set; }

            private string _PropertyID;
            public string PropertyID
            {
                get { return _PropertyID; }
                set { _PropertyID = value; }
            }

            private DateTime _EffectiveDate;
            public DateTime EffectiveDate
            {
                get { return _EffectiveDate; }
                set { _EffectiveDate = value; }
            }
       
    }
}