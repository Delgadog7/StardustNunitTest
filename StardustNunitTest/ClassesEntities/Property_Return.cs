﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Property_Return
    {
        public Property_Return() { }
        public Property_Header_Effective Header_Row { get; set; }
        public bool Property_Found { get; set; }
    }
}