﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Mfg_Reel_Component
    {
        public Mfg_Reel_Component() { }
        public string Material_ID { get; set; }
        public string MaterialDescription { get; set; }
        public string Site_ID { get; set; }
        public short? Event_Type_Value_ID { get; set; }

    }
}