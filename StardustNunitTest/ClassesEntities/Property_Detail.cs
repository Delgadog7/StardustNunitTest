﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Property_Detail
    {
        public Property_Detail() { }
        public int Instance_Index { get; set; }
        public string Property_ID { get; set; }
        public string Market_ID { get; set; }
        public string Customer_ID { get; set; }
        public string Language_ID { get; set; }
        public string Short_Description { get; set; }
        public string Long_Description { get; set; }
        public DateTime? Beginning_Date { get; set; }
        public DateTime? Ending_Date { get; set; }
        public bool Record_Active { get; set; }
        public DateTime? Maintenance_Timestamp { get; set; }
        public string Maintenance_User { get; set; }
    }
}