﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Property_Override
    {
        public Property_Override() { }
        public string Category_ID { get; set; }
        public string Property_ID { get; set; }
        public int Evaluation_Sequence { get; set; }
        public string Override_Description { get; set; }
        public string Override_Property_Value { get; set; }
        public string Rule_Set_ID { get;set; }
    }
}