﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class PropVal_Customer_Effective
    {
        public PropVal_Customer_Effective() { }
        public String Property_Value_ID { get; set; }
        public String Customer_Market_ID { get; set;}
        public String Customer_Language_ID { get; set;}
        public int Customer_Type { get; set; }
        public String Customer_Type_Description { get; set;}
        public bool Ordering_Thru_Partner { get; set; }
        public String Partner_ID { get; set; }
        public bool Ordering_By_Components { get; set; }
        public bool Waiver_Allowed { get; set; }
        public bool Send_Waiver_To_Contact { get; set; }
        public bool Require_Reel_Tracking { get; set; }
    }
}