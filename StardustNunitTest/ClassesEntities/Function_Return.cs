﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Function_Return
    {
        public Function_Return() { }    
        public String Function_Value { get; set; }
        public bool Function_Found { get; set; }
    }
}