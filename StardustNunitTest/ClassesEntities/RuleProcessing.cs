using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public enum LocalAllowOverrideValueID { Yes, No }
public enum UsageIndicator { Include, Blank, Exclude }
public enum Rule_Set_Result { True, False }
namespace StardustCoreLibs.App_Code.Classes
{
    public class RuleProcessing
    {
        public LocalAllowOverrideValueID valueID { get; set; }
        public UsageIndicator Usage_Indicator { get; set; }
        public Rule_Set_Result Result { get; set; }

        private string _PropertyID;
        public string PropertyID
        {
            get { return _PropertyID; }
            set { _PropertyID = value; }
        }

        private string _RuleSetID;
        public string RuleSetID
        {
            get { return _RuleSetID; }
            set { _RuleSetID = value; }
        }

        private Boolean _ComparisonResult;
        public Boolean ComparisonResult
        {
            get { return _ComparisonResult; }
            set { _ComparisonResult = value; }
        }

        private string _Base_Property_ID;
        public string Base_Property_ID
        {
            get { return _Base_Property_ID; }
            set { _Base_Property_ID = value; }
        }

        private string _Value_Text;
        public string Value_Text
        {
            get { return _Value_Text; }
            set { _Value_Text = value; }
        }

        private decimal _Value_Numeric;
        public decimal Value_Numeric
        {
            get { return _Value_Numeric; }
            set { _Value_Numeric = value; }
        }

        public bool status;

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
    }
}