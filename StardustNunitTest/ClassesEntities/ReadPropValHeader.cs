﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public enum ReturnStatusPropValHeader { PropVal_Header_Found, PropVal_Header_Not_Found }
    public class ReadPropValHeader
    {
        public ReturnStatusPropValHeader Status { get; set; }

        private string _PropertyID;
        public string PropertyID
        {
            get { return _PropertyID; }
            set { _PropertyID = value; }
        }

        private string _ParentValueID;
        public string ParentValueID
        {
            get { return _ParentValueID; }
            set { _ParentValueID = value; }
        }

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
    }
}