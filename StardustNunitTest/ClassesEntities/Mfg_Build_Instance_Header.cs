﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Mfg_Build_Instance_Header
    {
        public Mfg_Build_Instance_Header() { }
        public string Manufacturing_Build_Instance_ID { get; set; }
        public string Manufacturing_Build_ID { get; set; }
        public short Item_Sequence { get; set; }
        public short? Event_Type_Value_ID { get; set; }

    }
}