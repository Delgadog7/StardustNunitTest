﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Mfg_Site_Assignment
    {
        public Mfg_Site_Assignment() { }
        public int Instance_Index { get; set; }
        public string Site_ID { get; set; }
        public short? Evaluation_Sequence { get; set; }
        public string Rule_Set_ID { get; set; }
        public DateTime? Beginning_Date { get; set; }
        public DateTime? Ending_Date { get; set; }
        public bool Record_Active { get; set; }
        public DateTime? Maintenance_Timestamp { get; set; }
        public string Maintenance_User { get; set; }
    }
}