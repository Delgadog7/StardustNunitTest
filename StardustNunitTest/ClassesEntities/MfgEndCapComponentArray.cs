﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class MfgEncCapComponentArray
    {

        private string _CatalogNumber;
        public string CatalogNumber
        {
            get { return _CatalogNumber; }
            set { _CatalogNumber = value; }
        }

        private string _MaterialUsage;
        public string MaterialUsage
        {
            get { return _MaterialUsage; }
            set { _MaterialUsage = value; }
        }

        private string _MaterialDescription;
        public string MaterialDescription
        {
            get { return _MaterialDescription; }
            set { _MaterialDescription = value; }
        }

    }
}