﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Security_Access_Return
    {
        private bool _Access_Granted;
        public bool Access_Granted
        {
            get { return _Access_Granted; }
            set { _Access_Granted = value; }
        }

        private string _Data_Access;
        public string Data_Access
        {
            get { return _Data_Access; }
            set { _Data_Access = value; }
        }

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
    }
}