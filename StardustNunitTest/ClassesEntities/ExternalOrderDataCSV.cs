﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class ExternalOrderDataCSV
    {

        private string _CustomerBuildIDValue;
        public string CustomerBuildIDValue
        {
            get { return _CustomerBuildIDValue; }
            set { _CustomerBuildIDValue = value; }
        }

        private string _MaterialID;
        public string MaterialID
        {
            get { return _MaterialID; }
            set { _MaterialID = value; }
        }

        private string _MaterialQty;
        public string MaterialQty
        {
            get { return _MaterialQty; }
            set { _MaterialQty = value; }
        }

        private string _MaterialQtyUOM;
        public string MaterialQtyUOM
        {
            get { return _MaterialQtyUOM; }
            set { _MaterialQtyUOM = value; }
        }

        private string _MaterialPrice;
        public string MaterialPrice
        {
            get { return _MaterialPrice; }
            set { _MaterialPrice = value; }
        }

        private string _CustomerMaterialID;
        public string CustomerMaterialID
        {
            get { return _CustomerMaterialID; }
            set { _CustomerMaterialID = value; }
        }

        private string _CustomerOrderNumber;
        public string CustomerOrderNumber
        {
            get { return _CustomerOrderNumber; }
            set { _CustomerOrderNumber = value; }
        }

        private string _ShipToAddress;
        public string ShipToAddress
        {
            get { return _ShipToAddress; }
            set { _ShipToAddress = value; }
        }

        private string _ShipToState;
        public string ShipToState
        {
            get { return _ShipToState; }
            set { _ShipToState = value; }
        }

        private string _PartnerOrderNumber;
        public string PartnerOrderNumber
        {
            get { return _PartnerOrderNumber; }
            set { _PartnerOrderNumber = value; }
        }

        private DateTime _DateEntered;
        public DateTime DateEntered
        {
            get { return _DateEntered; }
            set { _DateEntered = value; }
        }

        private DateTime _DateRequested;
        public DateTime DateRequested
        {
            get { return _DateRequested; }
            set { _DateRequested = value; }
        }

        private string _RecordType;
        public string RecordType
        {
            get { return _RecordType; }
            set { _RecordType = value; }
        }

        private string _SiteID;
        public string SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }

        private string _CustomerID;
        public string CustomerID
        {
            get { return _CustomerID; }
            set { _CustomerID = value; }
        }

        private string _EventTypeValueID;
        public string EventTypeValueID
        {
            get { return _EventTypeValueID; }
            set { _EventTypeValueID = value; }
        }

        private string _BuildStatus;
        public string BuildStatus
        {
            get { return _BuildStatus; }
            set { _BuildStatus = value; }
        }

        private string _OrderStatus;
        public string OrderStatus
        {
            get { return _OrderStatus; }
            set { _OrderStatus = value; }
        }

        private string _PrevSiteID;
        public string PrevSiteID
        {
            get { return _PrevSiteID; }
            set { _PrevSiteID = value; }
        }

        private string _PrevPartnerOrderNumber;
        public string PrevPartnerOrderNumber
        {
            get { return _PrevPartnerOrderNumber; }
            set { _PrevPartnerOrderNumber = value; }
        }

        private string _PrevCustomerIDValue;
        public string PrevCustomerIDValue
        {
            get { return _PrevCustomerIDValue; }
            set { _PrevCustomerIDValue = value; }
        }

        private string _KeySiteID;
        public string KeySiteID
        {
            get { return _KeySiteID; }
            set { _KeySiteID = value; }
        }

        private string _KeyPartnerOrderNumber;
        public string KeyPartnerOrderNumber
        {
            get { return _KeyPartnerOrderNumber; }
            set { _KeyPartnerOrderNumber = value; }
        }

        private string _KeyCustomerIDValue;
        public string KeyCustomerIDValue
        {
            get { return _KeyCustomerIDValue; }
            set { _KeyCustomerIDValue = value; }
        }

        private string _ShipToCity;
        public string ShipToCity
        {
            get { return _ShipToCity; }
            set { _ShipToCity = value; }
        }

        private string _ShipToCountry;
        public string ShipToCountry
        {
            get { return _ShipToCountry; }
            set { _ShipToCountry = value; }
        }


    }
}