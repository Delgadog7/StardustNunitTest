﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class PropertyValueDescriptionLookup

    {
        public PropertyValueDescriptionLookup() { }

        public string PropertyID { get; set; }
        public string PropertyValueID { get; set; }
        public string ParentValueID { get; set; }
        public string MarketID { get; set; }
        public string CustomerID { get; set; }
        public string LanguagueID { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public Boolean ReturnStatus { get; set; }
        public DateTime Effective_Date { get; set; }
    }
}