﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Build_Key_Lookup

    {
        public Build_Key_Lookup() { }
        public string Configuration_ID { get; set; }
        public string Manufacturing_Build_ID { get; set; }
        public string Manufacturing_Build_Instance_ID { get; set; }
    }
}