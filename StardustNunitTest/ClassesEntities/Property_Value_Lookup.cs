﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Property_Value_Lookup

    {
        //public enum Status { Property_Value_Found, Property_Value_Not_Found }
        public enum Object_Type { Mfg_Build_Instance, Mfg_Build, Build, Undetermined }
        public Property_Value_Lookup() { }

        public string Property_Value_Text { get; set; }
        public decimal Property_Value_Numeric { get; set; }
        public Object_Type Property_Value_Type { get; set; }
        public DataTable Lookup_Data_Table { get; set; }
        public string Status { get; set; }
    }
}