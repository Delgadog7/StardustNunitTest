﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class DetailsArray
    {
        //private string _Configuration_ID;
        //public string Configuration_ID
        //{
        //    get { return _Configuration_ID; }
        //    set { _Configuration_ID = value; }
        //}

        private string _Property_ID;
        public string Property_ID
        {
            get { return _Property_ID; }
            set { _Property_ID = value; }
        }

        private string _Include_Exclude_Indicator;
        public string Include_Exclude_Indicator
        {
            get { return _Include_Exclude_Indicator; }
            set { _Include_Exclude_Indicator = value; }
        }

        private string _Category_ID;
        public string Category_ID
        {
            get { return _Category_ID; }
            set { _Category_ID = value; }
        }

        //private string _Manufacturing_Build_ID;
        //public string Manufacturing_Build_ID
        //{
        //    get { return _Manufacturing_Build_ID; }
        //    set { _Manufacturing_Build_ID = value; }
        //}

        //private string _Manufacturing_Build_Instance_ID;
        //public string Manufacturing_Build_Instance_ID
        //{
        //    get { return _Manufacturing_Build_Instance_ID; }
        //    set { _Manufacturing_Build_Instance_ID = value; }
        //}

        //private string _Location_Sequence;
        //public string Location_Sequence
        //{
        //    get { return _Location_Sequence; }
        //    set { _Location_Sequence = value; }
        //}

        //private string _Tap_Sequence;
        //public string Tap_Sequence
        //{
        //    get { return _Tap_Sequence; }
        //    set { _Tap_Sequence = value; }
        //}

        //private string _Tether_Sequence;
        //public string Tether_Sequence
        //{
        //    get { return _Tether_Sequence; }
        //    set { _Tether_Sequence = value; }
        //}

        //private string _Leg_Sequence;
        //public string Leg_Sequence
        //{
        //    get { return _Leg_Sequence; }
        //    set { _Leg_Sequence = value; }
        //}

        //private string _Fiber_Sequence;
        //public string Fiber_Sequence
        //{
        //    get { return _Fiber_Sequence; }
        //    set { _Fiber_Sequence = value; }
        //}


        //private string _Property_Valude_Text;
        //public string Property_Valude_Text
        //{
        //    get { return _Property_Valude_Text; }
        //    set { _Property_Valude_Text = value; }
        //}

        //private string _Property_Valude_Numeric;
        //public string Property_Valude_Numeric
        //{
        //    get { return _Property_Valude_Numeric; }
        //    set { _Property_Valude_Numeric = value; }
        //}

        //private string _Property_Valude_Type;
        //public string Property_Valude_Type
        //{
        //    get { return _Property_Valude_Type; }
        //    set { _Property_Valude_Type = value; }
        //}

        //private string _Lookup_Data_Table;
        //public string Lookup_Data_Table
        //{
        //    get { return _Lookup_Data_Table; }
        //    set { _Lookup_Data_Table = value; }
        //}

        //private string _Status;
        //public string Status
        //{
        //    get { return _Status; }
        //    set { _Status = value; }
        //}
    }
}