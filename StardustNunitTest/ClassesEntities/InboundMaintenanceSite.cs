﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{

    public enum InboundMaint { Header_Found, Header_Not_Found }
    public class InboundMaintenanceSite
    {
 
        public BuildHeader Status { get; set; }

        private string _ConfigurationID;
        public string ConfigurationID
        {
            get { return _ConfigurationID; }
            set { _ConfigurationID = value; }
        }

        private string _FileName;
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }


        private string _CustomerBuildID;
        public string CustomerBuildID
        {
            get { return _CustomerBuildID; }
            set { _CustomerBuildID = value; }
        }

        private string _CustomerBuildRevision;
        public string CustomerBuildRevision
        {
            get { return _CustomerBuildRevision; }
            set { _CustomerBuildRevision = value; }
        }

        private string _BuildDescription;
        public string BuildDescription
        {
            get { return _BuildDescription; }
            set { _BuildDescription = value; }
        }

        private string _EventTypeValueID;
        public string EventTypeValueID
        {
            get { return _EventTypeValueID; }
            set { _EventTypeValueID = value; }
        }

        private Boolean _OverrideButtonEnabled;
        public Boolean OverrideButtonEnabled
        {
            get { return _OverrideButtonEnabled; }
            set { _OverrideButtonEnabled = value; }
        }

        private Boolean _ViewButtonEnabled;
        public Boolean ViewButtonEnabled
        {
            get { return _ViewButtonEnabled; }
            set { _ViewButtonEnabled = value; }
        }

        private Boolean _AcceptButtonEnabled;
        public Boolean AcceptButtonEnabled
        {
            get { return _AcceptButtonEnabled; }
            set { _AcceptButtonEnabled = value; }
        }

        private Boolean _RejectButtonEnabled;
        public Boolean RejectButtonEnabled
        {
            get { return _RejectButtonEnabled; }
            set { _RejectButtonEnabled = value; }
        }

        private DateTime? _CreatedTimestamp;
        public DateTime? CreatedTimeStamp
        {
            get { return _CreatedTimestamp; }
            set { _CreatedTimestamp = value; }
        }

        private Boolean _StatusFromFunc;
        public Boolean StatusFromFunc
        {
            get { return _StatusFromFunc; }
            set { _StatusFromFunc = value; }
        }

        private string _MessageDescription;
        public string MessageDescription
        {
            get { return _MessageDescription; }
            set { _MessageDescription = value; }
        }

    }
}