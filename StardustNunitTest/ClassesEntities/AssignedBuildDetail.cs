﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class AssignedBuildDetail

    {
        public enum Status { Category_Found, Category_Not_Found }
        public AssignedBuildDetail() { }
        public string ProjectID { get; set; }
        public string Customer_Name { get; set; }
        public string Customer_Build_ID { get; set; }
        public string Build_Extended_Price { get; set; }
        public string Assigned_Site { get; set; }
        public string OpComm_Build_ID { get; set; }
        public string Build_City { get; set; }
        public string Build_State_Province { get; set; }
        public string Build_Country { get; set; }
        public string Cable_Name { get; set; }
        public string Created_Date { get; set; }
        public string Catalog_Number { get; set; }
        public string Component_Material { get; set; }
        public string Material_Type { get; set; }
        public string Component_Quantity { get; set; }
        public string UOM { get; set; }
        public string Component_Description { get; set; }
        public string Customer_Material_ID { get; set; }
        public string Component_Extended_Price { get; set; }
        public string Customer_ID { get; set; }


    }
}