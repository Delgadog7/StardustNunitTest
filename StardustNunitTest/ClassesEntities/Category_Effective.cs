﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Category_Effective
    {
        public Category_Effective() { }
        public string Category_ID { get; set; }
        public string Category_Description { get; set; }
        public short? Category_Type { get; set; }
        public string Category_Type_Description { get; set; }

    }
}