﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stardust.CoreLibs.Classes
{
    public class BuildMaintenance
    {
        private string _ConfigurationID;
        public string ConfigurationID
        {
            get { return _ConfigurationID; }
            set { _ConfigurationID = value; }
        }

        private string _CustomerBuildID;
        public string CustomerBuildID
        {
            get { return _CustomerBuildID; }
            set { _CustomerBuildID = value; }
        }

        private string _CustomerBuildRevision;
        public string CustomerBuildRevision
        {
            get { return _CustomerBuildRevision; }
            set { _CustomerBuildRevision = value; }
        }

        private string _BuildDescription;
        public string BuildDescription
        {
            get { return _BuildDescription; }
            set { _BuildDescription = value; }
        }

        private string _EventTypeValueID;
        public string EventTypeValueID
        {
            get { return _EventTypeValueID; }
            set { _EventTypeValueID = value; }
        }

        private string _SiteID;
        public string SiteID
        {
            get { return _SiteID; }
            set { _SiteID = value; }
        }

        private Boolean _ViewButtonEnabled;
        public Boolean ViewButtonEnabled
        {
            get { return _ViewButtonEnabled; }
            set { _ViewButtonEnabled = value; }
        }

        private Boolean _ResetButtonEnabled;
        public Boolean ResetButtonEnabled
        {
            get { return _ResetButtonEnabled; }
            set { _ResetButtonEnabled = value; }
        }

        private Boolean _ExcludeButtonEnabled;
        public Boolean ExcludeButtonEnabled
        {
            get { return _ExcludeButtonEnabled; }
            set { _ExcludeButtonEnabled = value; }
        }

        private Boolean _DeleteButtonEnabled;
        public Boolean DeleteButtonEnabled
        {
            get { return _DeleteButtonEnabled; }
            set { _DeleteButtonEnabled = value; }
        }

        private string _CreatedTimestamp;
        public string CreatedTimeStamp
        {
            get { return _CreatedTimestamp; }
            set { _CreatedTimestamp = value; }
        }
    }
}
