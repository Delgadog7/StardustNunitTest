﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Fetch_Property_Value
    {
        public enum Status { Property_Value_Found, Property_Value_Not_Found }
        public enum Object_Type { Mfg_Build_Instance, Mfg_Build, Build, Undetermined }
        public Fetch_Property_Value() { }
        public string Property_Value_Text { get; set; }
        public decimal Property_Value_Numeric { get; set; }
        public Object_Type Property_Value_Type { get; set; }
        public bool Property_Value_Found { get; set; }
        public Status Return_Status { get; set; }
    }
}