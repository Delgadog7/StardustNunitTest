﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Rule_Detail_Effective
    {
        public Rule_Detail_Effective() { }
        public string Rule_ID { get; set; }
        public int Evaluation_Sequence { get; set; }
        public string Category_ID { get; set; }
        public string Property_ID { get; set; }
        public string Property_Value { get; set; }
        public int Comparison_Test { get; set; }
        public string Comparison_Test_Description { get; set; }
        public bool Match_Case_for_Text { get; set; }
        public bool Allow_Override_Value { get; set; }
        public int Include_Exclude_Indicator { get; set; }
        public string Include_Exclude_Indicator_Description { get; set; }
    }
}