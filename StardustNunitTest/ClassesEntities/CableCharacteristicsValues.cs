using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{ 
    public class CableCharacteristicsValues
    {
        public ReturnStatus Status { get; set; }

        private string _CableTypeID;
        public string CableTypeID
        {
            get { return _CableTypeID; }
            set { _CableTypeID = value; }
        }

        private string _ArmorTypeID;
        public string ArmorTypeID
        {
            get { return _ArmorTypeID; }
            set { _ArmorTypeID = value; }
        }

        private string _FlameRetardantTypeID;
        public string FlameRetardantTypeID
        {
            get { return _FlameRetardantTypeID; }
            set { _FlameRetardantTypeID = value; }
        }

        private string _FiberTypeID;
        public string FiberTypeID
        {
            get { return _FiberTypeID; }
            set { _FiberTypeID = value; }
        }

        private string _FillTypeID;
        public string FillTypeID
        {
            get { return _FillTypeID; }
            set { _FillTypeID = value; }
        }

        private string _SupportTypeID;
        public string SupportTypeID
        {
            get { return _SupportTypeID; }
            set { _SupportTypeID = value; }
        }

        private string _ToneableTypeID;
        public string ToneableTypeID
        {
            get { return _ToneableTypeID; }
            set { _ToneableTypeID = value; }
        }

        private string _FiberCountID;
        public string FiberCountID
        {
            get { return _FiberCountID; }
            set { _FiberCountID = value; }
        }

        private string _LengthMartikingTypeID;
        public string LengthMartikingTypeID
        {
            get { return _LengthMartikingTypeID; }
            set { _LengthMartikingTypeID = value; }
        }

        private string _EnvironmentTypeID;
        public string EnvironmentTypeID
        {
            get { return _EnvironmentTypeID; }
            set { _EnvironmentTypeID = value; }
        }

        private string _InstallationTypeID;
        public string InstallationTypeID
        {
            get { return _InstallationTypeID; }
            set { _InstallationTypeID = value; }
        }

        private string _TAPTypeID;
        public string TAPTypeID
        {
            get { return _TAPTypeID; }
            set { _TAPTypeID = value; }
        }

        private string _CategoryID;
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }

        private string _FurcationTypeID;
        public string FurcationTypeID
        {
            get { return _FurcationTypeID; }
            set { _FurcationTypeID = value; }
        }

        private string _ConnectorTypeID;
        public string ConnectorTypeID
        {
            get { return _ConnectorTypeID; }
            set { _ConnectorTypeID = value; }
        }

        private string _LegTypeID;
        public string LegTypeID
        {
            get { return _LegTypeID; }
            set { _LegTypeID = value; }
        }

        private string _TetherFiberCountID;
        public string TetherFiberCountID
        {
            get { return _TetherFiberCountID; }
            set { _TetherFiberCountID = value; }
        }

        private string _LoopbackID;
        public string LoopbackID
        {
            get { return _LoopbackID; }
            set { _LoopbackID = value; }
        }

        private string _LegLengthID;
        public string LegLengthID
        {
            get { return _LegLengthID; }
            set { _LegLengthID = value; }
        }

        private string _LengthUOMID;
        public string LengthUOMID
        {
            get { return _LengthUOMID; }
            set { _LengthUOMID = value; }
        }

        private string _TetherTypeID;
        public string TetherTypeID
        {
            get { return _TetherTypeID; }
            set { _TetherTypeID = value; }
        }

        private string _DuctSizeCode;
        public string DuctSizeCode
        {
            get { return _DuctSizeCode; }
            set { _DuctSizeCode = value; }
        }

        private string _FiberModeCode;
        public string FiberModeCode
        {
            get { return _FiberModeCode; }
            set { _FiberModeCode = value; }
        }

        private Boolean _CO_PreTerm;
        public Boolean CO_PreTerm
        {
            get { return _CO_PreTerm; }
            set { _CO_PreTerm = value; }
        }

        private Boolean _Field_PreTerm;
        public Boolean Field_PreTerm
        {
            get { return _Field_PreTerm; }
            set { _Field_PreTerm = value; }
        }

        private Boolean _Tether_PreTerm;
        public Boolean Tether_PreTerm
        {
            get { return _Tether_PreTerm; }
            set { _Tether_PreTerm = value; }
        }

        private Boolean _PreTerm_Allowed;
        public Boolean PreTerm_Allowed
        {
            get { return _PreTerm_Allowed; }
            set { _PreTerm_Allowed = value; }
        }

        private Boolean _InstallationFound;
        public Boolean InstallationFound
        {
            get { return _InstallationFound; }
            set { _InstallationFound = value; }
        }

        private string _ConnectorTypeCode_COSide;
        public string ConnectorTypeCode_COSide
        {
            get { return _ConnectorTypeCode_COSide; }
            set { _ConnectorTypeCode_COSide = value; }
        }

        private string _FurcationTypeCode_COSide;
        public string FurcationTypeCode_COSide
        {
            get { return _FurcationTypeCode_COSide; }
            set { _FurcationTypeCode_COSide = value; }
        }

        private string _LegTypeCode_COSide;
        public string LegTypeCode_COSide
        {
            get { return _LegTypeCode_COSide; }
            set { _LegTypeCode_COSide = value; }
        }

        private string _ConnectorTypeCode_FieldSide;
        public string ConnectorTypeCode_FieldSide
        {
            get { return _ConnectorTypeCode_FieldSide; }
            set { _ConnectorTypeCode_FieldSide = value; }
        }

        private string _FurcationTypeCode_FieldSide;
        public string FurcationTypeCode_FieldSide
        {
            get { return _FurcationTypeCode_FieldSide; }
            set { _FurcationTypeCode_FieldSide = value; }
        }

        private string _LegTypeCode_FieldSide;
        public string LegTypeCode_FieldSide
        {
            get { return _LegTypeCode_FieldSide; }
            set { _LegTypeCode_FieldSide = value; }
        }


        private string _ConnectorTypeCode_Temp;
        public string ConnectorTypeCode_Temp
        {
            get { return _ConnectorTypeCode_Temp; }
            set { _ConnectorTypeCode_Temp = value; }
        }


        private string _FurcationTypeCode_Temp;
        public string FurcationTypeCode_Temp
        {
            get { return _FurcationTypeCode_Temp; }
            set { _FurcationTypeCode_Temp = value; }
        }

        private string _FiberSequence;
        public string FiberSequence
        {
            get { return _FiberSequence; }
            set { _FiberSequence = value; }
        }

        private int _TetherCount;
        public int TetherCount
        {
            get { return _TetherCount; }
            set { _TetherCount = value; }
        }

        private int _LocationSequence;
        public int LocationSequence
        {
            get { return _LocationSequence; }
            set { _LocationSequence = value; }
        }

        private int _Counter;
        public int Counter
        {
            get { return _Counter; }
            set { _Counter = value; }
        }

        private int _Field_Slack_Minimum;
        public int Field_Slack_Minimum
        {
            get { return _Field_Slack_Minimum; }
            set { _Field_Slack_Minimum = value; }
        }

        private int _Tether_Combination_ID;
        public int Tether_Combination_ID
        {
            get { return _Tether_Combination_ID; }
            set { _Tether_Combination_ID = value; }
        }

        private int _Field_Span_Minimum;
        public int Field_Span_Minimum
        {
            get { return _Field_Slack_Minimum; }
            set { _Field_Slack_Minimum = value; }
        }

        private string _LocationID;
        public string LocationID
        {
            get { return _LocationID; }
            set { _LocationID = value; }
        }

        private string _FiberAssignmentMethod;
        public string FiberAssignmentMethod
        {
            get { return _FiberAssignmentMethod; }
            set { _FiberAssignmentMethod = value; }
        }

        private int _SpanDistance;
        public int SpanDistance
        {
            get { return _SpanDistance; }
            set { _SpanDistance = value; }
        }

        private int _SlackStorage;
        public int SlackStorage
        {
            get { return _SlackStorage; }
            set { _SlackStorage = value; }
        }

        private Boolean _TAPSegmentPresent;
        public Boolean TAPSegmentPresent
        {
            get { return _TAPSegmentPresent; }
            set { _TAPSegmentPresent = value; }
        }

        private Boolean _FirstTap;
        public Boolean FirstTap
        {
            get { return _FirstTap; }
            set { _FirstTap = value; }
        }

        private string _LegTypeCode_Temp;
        public string LegTypeCode_Temp
        {
            get { return _LegTypeCode_Temp; }
            set { _LegTypeCode_Temp = value; }
        }

        private string _FiberAccessMC;
        public string FiberAccessMC
        {
            get { return _FiberAccessMC; }
            set { _FiberAccessMC = value; }
        }

        private string _FiberAssignmentFactor;
        public string FiberAssignmentFactor
        {
            get { return _FiberAssignmentFactor; }
            set { _FiberAssignmentFactor = value; }
        }

        private int _TerminationCountMax;
        public int TerminationCountMax
        {
            get { return _TerminationCountMax; }
            set { _TerminationCountMax = value; }
        }

        private int _PriorFiber;
        public int PriorFiber
        {
            get { return _PriorFiber; }
            set { _PriorFiber = value; }
        }

        private int _CurrentLayerSequence;
        public int CurrentLayerSequence
        {
            get { return _CurrentLayerSequence; }
            set { _CurrentLayerSequence = value; }
        }

        private int _PriorFirstFiber;
        public int PriorFirstFiber
        {
            get { return _PriorFirstFiber; }
            set { _PriorFirstFiber = value; }
        }

        private int _LocationTAPCountMax;
        public int LocationTAPCountMax
        {
            get { return _LocationTAPCountMax; }
            set { _LocationTAPCountMax = value; }
        }

        private int _TAP_Tether_Count_MAX;
        public int TAP_Tether_Count_MAX
        {
            get { return _TAP_Tether_Count_MAX; }
            set { _TAP_Tether_Count_MAX = value; }
        }

        private int _TAP_Subunit_Count_MAX;
        public int TAP_Subunit_Count_MAX
        {
            get { return _TAP_Subunit_Count_MAX; }
            set { _TAP_Subunit_Count_MAX = value; }
        }

        private int _TAP_Tethers_Fibers_MAX;
        public int TAP_Tethers_Fibers_MAX
        {
            get { return _TAP_Tethers_Fibers_MAX; }
            set { _TAP_Tethers_Fibers_MAX = value; }
        }

        private int _Field_Installation_Req;
        public int Field_Installation_Req
        {
            get { return _Field_Installation_Req; }
            set { _Field_Installation_Req = value; }
        }

        private int _CO_Installation_Req;
        public int CO_Installation_Req
        {
            get { return _CO_Installation_Req; }
            set { _CO_Installation_Req = value; }
        }

        private int _LayerSequence;
        public int LayerSequence
        {
            get { return _LayerSequence; }
            set { _LayerSequence = value; }
        }

        private Boolean _SkippedFibers;
        public Boolean SkippedFibers
        {
            get { return _SkippedFibers; }
            set { _SkippedFibers = value; }
        }

        private Boolean _Tether_Combination_Found;
        public Boolean Tether_Combination_Found
        {
            get { return _Tether_Combination_Found; }
            set { _Tether_Combination_Found = value; }
        }

        private Boolean _FirstLocation;
        public Boolean FirstLocation
        {
            get { return _FirstLocation; }
            set { _FirstLocation = value; }
        }

        private Boolean _FirstFiber;
        public Boolean FirstFiber
        {
            get { return _FirstFiber; }
            set { _FirstFiber = value; }
        }

        private Boolean _FirstTether;
        public Boolean FirstTether
        {
            get { return _FirstTether; }
            set { _FirstTether = value; }
        }

        private int _CO_Slack;
        public int CO_Slack
        {
            get { return _CO_Slack; }
            set { _CO_Slack = value; }
        }

        private int _Field_Slack;
        public int Field_Slack
        {
            get { return _Field_Slack; }
            set { _Field_Slack = value; }
        }

        private int _Cable_Planned_Fibers;
        public int Cable_Planned_Fibers
        {
            get { return _Cable_Planned_Fibers; }
            set { _Cable_Planned_Fibers = value; }
        }

        private int _TAP_Planned_Fibers;
        public int TAP_Planned_Fibers
        {
            get { return _TAP_Planned_Fibers; }
            set { _TAP_Planned_Fibers = value; }
        }

        private int _TAP_Subunit_Count;
        public int TAP_Subunit_Count
        {
            get { return _TAP_Subunit_Count; }
            set { _TAP_Subunit_Count = value; }
        }

        private int _Tether_Planned_Fibers;
        public int Tether_Planned_Fibers
        {
            get { return _Tether_Planned_Fibers; }
            set { _Tether_Planned_Fibers = value; }
        }

        private int _Tether_Subunit_Count;
        public int Tether_Subunit_Count
        {
            get { return _Tether_Subunit_Count; }
            set { _Tether_Subunit_Count = value; }
        }



        private Boolean _TAP_Subunits_Adjacent;
        public Boolean TAP_Subunits_Adjacent
        {
            get { return _TAP_Subunits_Adjacent; }
            set { _TAP_Subunits_Adjacent = value; }
        }

        private Boolean _Cable_Material_Found;
        public Boolean Cable_Material_Found
        {
            get { return _Cable_Material_Found; }
            set { _Cable_Material_Found = value; }
        }


    }
}