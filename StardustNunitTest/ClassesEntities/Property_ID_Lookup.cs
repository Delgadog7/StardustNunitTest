﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public enum Status { Property_ID_Found, Property_ID_Not_Found }
    public class Property_ID_Lookup

    {
        public Property_ID_Lookup() { }
        public string Property_ID { get; set; }
        public string Manufacturing_Build_ID { get; set; }
        public Status Return_Status { get; set; } // added due Property_ID_Lookup file.
    }
}