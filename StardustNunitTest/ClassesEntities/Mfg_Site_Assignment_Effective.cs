﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class Mfg_Site_Assignment_Effective
    {
        public Mfg_Site_Assignment_Effective() { }
        public string Site_ID { get; set; }
        public int Evaluation_Sequence { get; set; }
        public string Rule_Set_ID { get; set;}
        
    }
}