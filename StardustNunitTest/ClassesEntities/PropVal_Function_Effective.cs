﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StardustCoreLibs.App_Code.Classes
{
    public class PropVal_Function_Effective
    {
        public PropVal_Function_Effective() { }
        public string Property_ID { get; set; }
        public string Function_Name { get; set;}
    }
}