﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustNunitTest.DataAccess;
namespace StardustNunitTest.Classess
{
    public class ClassEvent_Type_Lookup
    {
        DataAccesscGF DA = new DataAccesscGF();
        public DataTable Event_Type_Lookup(ENUMDetailLookup objEN)
        {
            DataTable DtEnumLookup = (DA.Event_Type_Lookup(objEN));

            ENUMDetailLookup objENP = new ENUMDetailLookup();
            int intAu = DtEnumLookup.Rows.Count;
            if (intAu > 0)
            {
                objENP.Status = EventReturnStatus.Event_Type_Found;
            }
            else
            {
                objENP.Status = EventReturnStatus.Event_Type_Not_Found;
            }

            return DtEnumLookup;
        }
    }
    public enum EventReturnStatus { Event_Type_Found, Event_Type_Not_Found }
    public class ENUMDetailLookup
    {
        public EventReturnStatus Status { get; set; }

        private string _EnumID;
        public string EnumID
        {
            get { return _EnumID; }
            set { _EnumID = value; }
        }

        private string _EnumValueName;
        public string EnumValueName
        {
            get { return _EnumValueName; }
            set { _EnumValueName = value; }
        }

        private string _EffectiveDate;
        public string EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }

        private string _EventTypeKey;
        public string EventTypeKey
        {
            get { return _EventTypeKey; }
            set { _EventTypeKey = value; }
        }

        private string _EventTypeID;
        public string EventTypeID
        {
            get { return _EventTypeID; }
            set { _EventTypeID = value; }
        }

        private string _Event_Type_Value_ID;
        public string Event_Type_Value_ID
        {
            get { return _Event_Type_Value_ID; }
            set { _Event_Type_Value_ID = value; }
        }

        private string _ENUMFieldName;
        public string ENUMFieldName
        {
            get { return _ENUMFieldName; }
            set { _ENUMFieldName = value; }
        }

    }
}
