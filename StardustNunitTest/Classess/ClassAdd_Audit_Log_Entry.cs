﻿using StardustNunitTest.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StardustNunitTest.Classess
{
    public class ClassAdd_Audit_Log_Entry
    {
        DataAccesscGF DA = new DataAccesscGF();
        AddAuditLogEntry objAuL = new AddAuditLogEntry();

        public bool Add_Audit_Log_Entry(string Event_Type_ID)
        {
            DataTable DTAuditLog = DA.Lookup_Audit_Log_Entry(objAuL.AuditKey1, objAuL.AuditKey2, objAuL.AuditKey3, objAuL.AuditKey4, Event_Type_ID, objAuL.EventTypeValueID, objAuL.UserID, objAuL.AuditData);
            bool a = false;
            if (DTAuditLog.Rows.Count > 0)
            {
                objAuL.AuditData = DTAuditLog.Rows[0]["Audit_Event"].ToString();

                if (objAuL.AuditData == "True")
                {
                    //objgf.InsertAuditLogEntry(objAuL);
                    a = true;
                }
                else
                { a = false; }
            }
            return a;
        }
    }
    public class AddAuditLogEntry
    {

        private string _AuditKey1;
        public string AuditKey1
        {
            get { return _AuditKey1; }
            set { _AuditKey1 = value; }
        }

        private string _AuditKey2;
        public string AuditKey2
        {
            get { return _AuditKey2; }
            set { _AuditKey2 = value; }
        }

        private string _AuditKey3;
        public string AuditKey3
        {
            get { return _AuditKey3; }
            set { _AuditKey3 = value; }
        }

        private string _AuditKey4;
        public string AuditKey4
        {
            get { return _AuditKey4; }
            set { _AuditKey4 = value; }
        }

        private string _EventTypeID;
        public string EventTypeID
        {
            get { return _EventTypeID; }
            set { _EventTypeID = value; }
        }

        private string _EventTypeValueID;
        public string EventTypeValueID
        {
            get { return _EventTypeValueID; }
            set { _EventTypeValueID = value; }
        }

        private string _UserID;
        public string UserID
        {
            get { return _UserID; }
            set { _UserID = value; }
        }

        private string _AuditData;
        public string AuditData
        {
            get { return _AuditData; }
            set { _AuditData = value; }
        }

    }
}
