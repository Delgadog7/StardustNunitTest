﻿using StardustNunitTest.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StardustNunitTest.Classess
{
    public class ClassSales_Order_Instance_Index
    {
        DataAccesscGF DA = new DataAccesscGF();
        public int GetSalesOrderInstanceIndex(string SalesOrderNumber, string CustomerID)
        {
            int InstanceIndex = 0;
            DataTable DTERP_Sales_Order_1_RS = (DA.ERPSalesOrder1RS(SalesOrderNumber, CustomerID));
            int interp1 = DTERP_Sales_Order_1_RS.Rows.Count;
            if (interp1 > 0)
            {

                InstanceIndex = Convert.ToInt32(DTERP_Sales_Order_1_RS.Rows[0]["Instance_index"]);
            }
            else
            {
                DataTable DTERP_Sales_Order_2_RS = (DA.ERPSalesOrder2RS(SalesOrderNumber));
                int interp2 = DTERP_Sales_Order_2_RS.Rows.Count;
                if (interp2 > 0)
                {

                    InstanceIndex = Convert.ToInt32(DTERP_Sales_Order_2_RS.Rows[0]["Instance_index"]) + 1;
                }
                else
                {
                    InstanceIndex = 1;
                }
            }
            return InstanceIndex;

        }
    }
}
