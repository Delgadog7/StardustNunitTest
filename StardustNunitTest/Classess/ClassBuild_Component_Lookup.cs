﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustNunitTest.DataAccess;

namespace StardustNunitTest.Classess
{
    public class ClassBuild_Component_Lookup
    {
        DataAccesscGF DA = new DataAccesscGF();
        ClassProperties ClassProperties = new ClassProperties();
        public List<ComponentArray> BuildComponentLookup(String ConfigurationID)
        {
            List<ComponentArray> parts = new List<ComponentArray>();
            DataTable DtCompLook = (DA.BuildComponentLookup(ConfigurationID));

            if (DtCompLook.Rows.Count > 0)
            {
                foreach (DataRow row in DtCompLook.Rows)
                {
                    var singleComponent = new ComponentArray();
                    singleComponent.CatalogNumber = row["Catalog_Number"].ToString();
                    singleComponent.MaterialTypeID = row["Material_Type_ID"].ToString();

                    if (row["Material_Type_ID"].ToString() == "CBL")
                    {
                        singleComponent.Quantity = GetCableQuantityFeet(ConfigurationID);
                        singleComponent.QuantityUOM = "FOT";
                    }
                    else
                    {
                        singleComponent.Quantity = row["Material_Quantity"].ToString();
                        singleComponent.QuantityUOM = "PCE";

                    }
                    parts.Add(singleComponent);

                }
            }

            return parts;
        }
        public string GetCableQuantityFeet(string ConfigurationID)
        {
            string Build_UOM = null;
            double Material_Qty = 0, Distribution_Qty = 0,
                CO_Slack_Qty = 0, Field_Slack_Qty = 0;

            Build_UOM = ClassProperties.GetBuildEnteredUOM(ConfigurationID);
            CO_Slack_Qty = ClassProperties.GetCOSideSlack(ConfigurationID);
            Field_Slack_Qty = ClassProperties.GetFieldSideSlack(ConfigurationID);
            Distribution_Qty = GetDistributionQuantity(ConfigurationID);

            if (Build_UOM == "MTR")
            {
                Material_Qty = (CO_Slack_Qty + Field_Slack_Qty + Distribution_Qty) * 3.2808;
            }
            else
            {
                Material_Qty = (CO_Slack_Qty + Field_Slack_Qty + Distribution_Qty);
            }

            return Material_Qty.ToString();
        }
        public int GetDistributionQuantity(string ConfigurationID)
        {
            int Dist_qty = 0, Span = 0;

            DataTable DtLocHeader = (DA.Build_Location_Header(ConfigurationID));


            if (DtLocHeader.Rows.Count > 0)
            {
                String Location_Sequence = null;
                foreach (DataRow row in DtLocHeader.Rows)
                {
                    Location_Sequence = row["Location_Sequence"].ToString();
                    Span = ClassProperties.GetLocationNextSpanDistance(ConfigurationID, Location_Sequence);

                    if (Span > 0)
                    {
                        Dist_qty = Dist_qty + Span;
                    }
                }
            }

            return Dist_qty;
        }
    }
}
