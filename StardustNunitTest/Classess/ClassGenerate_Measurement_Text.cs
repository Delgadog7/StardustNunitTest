﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StardustNunitTest.Classess
{
    public class ClassGenerate_Measurement_Text
    {
        public string Generate_Measurement_Text(double NumericValue, int UnitType, string lang)
        {
            GenerateMeasurementText objGmt = new GenerateMeasurementText();
            objGmt.MeasurementText = "";

            if (objGmt.UnitType == "Imperial")
            {
                objGmt.MeasurementText = Generate_Text_Imperial(NumericValue, objGmt.LanguageID);
            }
            else
            {
                objGmt.MeasurementText = Generate_Text_Metric(NumericValue, objGmt.LanguageID);
            }

            return objGmt.MeasurementText;
        }
        public string Generate_Text_Imperial(double NumericValue, String LanguageID)
        {
            GenerateMeasurementText objGmt = new GenerateMeasurementText();
            double Measurement_Foot = 0, Measurement_Decimal = 0, Temp_value = 0, Measurement_Inch_Fraction = 0;
            int Measurement_Inch_Integer = 0;
            string Unit_1_UOM = null, Unit_2_UOM = null, Unit_1_UOM_Desc = null, Unit_2_UOM_Desc = null;

            Measurement_Foot = Math.Truncate(NumericValue);
            Measurement_Decimal = NumericValue - Measurement_Foot;
            Temp_value = Math.Round(Measurement_Decimal * 96, 0);
            Measurement_Inch_Integer = Convert.ToInt32(Temp_value / 8);

            if (Measurement_Inch_Integer == 12)
            {
                Measurement_Foot = Measurement_Foot + 1;
                Measurement_Inch_Integer = 0;
                Measurement_Inch_Fraction = 0;
            }
            else
            {
                Measurement_Inch_Fraction = (Temp_value % 8);
            }

            if (Measurement_Foot == 1)
            {
                Unit_1_UOM = "Foot";
            }
            else
            {
                Unit_1_UOM = "Feet";
            }

            if (Measurement_Inch_Integer == 0)
            {
                Unit_2_UOM = "Inch";
            }
            else
            {
                if (Measurement_Inch_Integer == 1)
                {
                    if (Measurement_Inch_Fraction == 0)
                    {
                        Unit_2_UOM = "Inch";
                    }
                    else
                    {
                        Unit_2_UOM = "Inches";
                    }
                }
                else
                {
                    Unit_2_UOM = "Inches";
                }
            }

            //PlaceHolder Lookup Unit_1_UOM Description based on LanguageID = Unit_1_UOM_Desc
            //PlaceHolder Lookup Unit_2_UOM Description based on LanguageID = Unit_2_UOM_Desc

            if (Measurement_Foot > 0)
            {
                if (Measurement_Inch_Integer > 0)
                {
                    if (Measurement_Inch_Fraction > 0)
                    {
                        objGmt.MeasurementText = Measurement_Foot + " " + Unit_1_UOM_Desc + " " + Measurement_Inch_Integer + " " + Measurement_Inch_Fraction + "/8" + Unit_2_UOM_Desc;
                    }
                    else
                    {
                        objGmt.MeasurementText = Measurement_Foot + " " + Unit_1_UOM_Desc + " " + Measurement_Inch_Integer + " " + Unit_2_UOM_Desc;
                    }
                }
                else
                {
                    if (Measurement_Inch_Fraction > 0)
                    {
                        objGmt.MeasurementText = Measurement_Foot + " " + Unit_1_UOM_Desc + " " + Measurement_Inch_Fraction + "/8" + Unit_2_UOM_Desc;
                    }
                    else
                    {
                        objGmt.MeasurementText = Measurement_Foot + " " + Unit_1_UOM_Desc;
                    }
                }
            }
            else
            {
                if (Measurement_Inch_Integer > 0)
                {
                    if (Measurement_Inch_Fraction > 0)
                    {
                        objGmt.MeasurementText = Measurement_Inch_Integer + " " + Measurement_Inch_Fraction + "/8" + Unit_2_UOM_Desc;
                    }
                    else
                    {
                        objGmt.MeasurementText = Measurement_Inch_Integer + " " + Unit_2_UOM_Desc;
                    }
                }
                else
                {
                    if (Measurement_Inch_Fraction > 0)
                    {
                        objGmt.MeasurementText = Measurement_Inch_Fraction + "/8" + Unit_2_UOM_Desc;
                    }
                    else
                    {
                        objGmt.MeasurementText = "";
                    }
                }
            }

            return objGmt.MeasurementText;
        }

        public string Generate_Text_Metric(double NumericValue, String LanguageID)
        {
            GenerateMeasurementText objGmt = new GenerateMeasurementText();
            double Measurement_Meter = 0, Measurement_Decimal = 0;
            string Unit_UOM = null, Unit_UOM_Desc = null;

            Measurement_Meter = Math.Truncate(NumericValue);
            Measurement_Decimal = NumericValue - Measurement_Meter;

            if (Measurement_Meter == 1)
            {
                if (Measurement_Decimal == 0)
                {
                    Unit_UOM = "Meter";
                }
                else
                {
                    Unit_UOM = "Meters";
                }
            }
            else
            {
                Unit_UOM = "Meters";
            }

            //PlaceHolder Lookup Unit_1_UOM Description based on LanguageID = Unit_UOM_Desc

            if (Measurement_Decimal > 0)
            {
                objGmt.MeasurementText = NumericValue + " " + Unit_UOM_Desc;
            }
            else
            {
                objGmt.MeasurementText = Measurement_Meter + " " + Unit_UOM_Desc;
            }

            return objGmt.MeasurementText;
        }
    }
    public class GenerateMeasurementText
    {

        private string _NumericValue;
        public string NumericValue
        {
            get { return _NumericValue; }
            set { _NumericValue = value; }
        }

        private string _UnitType;
        public string UnitType
        {
            get { return _UnitType; }
            set { _UnitType = value; }
        }

        private string _LanguageID;
        public string LanguageID
        {
            get { return _LanguageID; }
            set { _LanguageID = value; }
        }

        private string _MeasurementText;
        public string MeasurementText
        {
            get { return _MeasurementText; }
            set { _MeasurementText = value; }
        }

    }
}
