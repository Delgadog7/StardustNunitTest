﻿using StardustNunitTest.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StardustNunitTest.Classess
{
    public class ClassGet_Production_Instance_Index
    {
        DataAccesscGF DA = new DataAccesscGF();
        public int GetProductionInstanceIndex(string ProductionOrderNumber, string CatalogNumber)
        {
            int InstanceIndex = 0;
            DataTable DTERP_Production_1_RS = (DA.ERPProductionOrder1RS(ProductionOrderNumber, CatalogNumber));
            int interp1 = DTERP_Production_1_RS.Rows.Count;
            if (interp1 > 0)
            {
                InstanceIndex = Convert.ToInt32(DTERP_Production_1_RS.Rows[0]["Instance_Index"]);
            }
            else
            {
                DataTable DTERP_Production_2_RS = (DA.ERPProductionOrder2RS(ProductionOrderNumber));
                int interp2 = DTERP_Production_2_RS.Rows.Count;
                if (interp2 > 0)
                {

                    InstanceIndex = Convert.ToInt32(DTERP_Production_2_RS.Rows[0]["Instance_index"]) + 1;
                }
                else
                {
                    InstanceIndex = 1;
                }
            }
            return InstanceIndex;
        }
    }
}
