﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using StardustNunitTest.DataAccess;
using StardustCoreLibs.App_Code.Classes;

namespace StardustNunitTest.Classess
{
    public class ClassBuild_Key_Lookup
    {
        DataAccesscGF DA = new DataAccesscGF();
        public KeyLookup BuildKeyLookup(String InputKey)
        {
            KeyLookup objKl = new KeyLookup();
            string ManBuildID = null, localConfID = null;
            if (!String.IsNullOrEmpty(InputKey))
            {
                DataTable DtKeyLook = (DA.Build_Instance_Header(InputKey));

                int intKL = DtKeyLook.Rows.Count;
                if (intKL > 0)
                {
                    objKl.Manufacturing_Build_Instance_ID = InputKey;
                    ManBuildID = DtKeyLook.Rows[0]["Manufacturing_Build_ID"].ToString();
                }
                else
                {
                    ManBuildID = InputKey;
                }

                DataTable DtMfgBuildHeader = (DA.MFG_Build_Header(ManBuildID));

                int intBH = DtMfgBuildHeader.Rows.Count;
                if (intBH > 0)
                {
                    objKl.Manufacturing_Build_ID = ManBuildID;
                    localConfID = DtMfgBuildHeader.Rows[0]["Configuration_ID"].ToString();
                }
                else
                {
                    localConfID = InputKey;
                }


                DataTable DtBuildHeader = (DA.Build_Header(localConfID));

                int intBHe = DtBuildHeader.Rows.Count;
                if (intBHe > 0)
                {
                    objKl.ConfigurationID = localConfID;
                }

            }
            return objKl;
        }

    }
}
