﻿using System;
using StardustNunitTest.DataAccess;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace StardustNunitTest.Classess
{
    public class ClassAssignment_DeterminationD
    {
        DataAccessPM PM = new DataAccessPM();
        DataAccesscGF DA = new DataAccesscGF();
        ClassCustomer_ID_Lookup CCuIL = new ClassCustomer_ID_Lookup();
        ClassBuild_Component_Lookup BCL = new ClassBuild_Component_Lookup();
        public bool? Assigment_Determination(String ConfigurationID, DateTime EffectiveDate)
        {
            bool Assignment_Allowed = false;

           PropVal_Customer_Effective PropVal_Customer_Effective =
               GetCustomerRequirement(ConfigurationID, EffectiveDate);
            if (PropVal_Customer_Effective != null)
            {
                if (PropVal_Customer_Effective.Ordering_Thru_Partner)
                {
                    Partner_Order_Item Partner_Order_Item =
                        CheckPartnerOrder(ConfigurationID, PropVal_Customer_Effective);
                    if (Partner_Order_Item != null)
                    {
                        if (PropVal_Customer_Effective.Ordering_By_Components)
                        {
                            bool Requirements_Match = CompareOrder_BuildMaterials(Partner_Order_Item.Customer_Build_ID,
                                Partner_Order_Item);
                            if (Requirements_Match)
                                Assignment_Allowed = true;
                            else
                                Assignment_Allowed = false;
                        }
                        else
                            Assignment_Allowed = true;
                    }
                    else
                        Assignment_Allowed = false;
                }
                else
                    Assignment_Allowed = true;
            }
            else
                Assignment_Allowed = false;

            return Assignment_Allowed;
        }
        internal Partner_Order_Item CheckPartnerOrder(string ConfigurationID, PropVal_Customer_Effective PropVal_Customer_Effective)
        {
            bool Partner_Order_Found = false;
            Partner_Order_Item Partner_Order_Item = null;
            List<Build_Header> lstBuildHeader = PM.ConvertTo<Build_Header>(PM.Build_Header(ConfigurationID, false));
            if (lstBuildHeader.Count > 0)
            {
                List<Partner_Order_Item> lstPartner_Order = PM.ConvertTo<Partner_Order_Item>(PM.PartnerOrderDetail(PropVal_Customer_Effective.Partner_ID, lstBuildHeader[0].Customer_Build_ID));
                foreach (var item in lstPartner_Order)
                {
                    List<Partner_Order> lstPartner_Order_Item = PM.ConvertTo<Partner_Order>(PM.PartnerOrderItem(item.Partner_ID, item.Partner_Order_Number));
                    if (lstPartner_Order_Item.Count > 0)
                    {
                        Partner_Order_Found = true;
                        Partner_Order_Item = item;
                        break;
                    }
                }
            }

            return Partner_Order_Item;
        }
        internal PropVal_Customer_Effective GetCustomerRequirement(string ConfigurationID, DateTime EffectiveDate)
        {
            PropVal_Customer_Effective PropVal_Customer_Effective = null;
            string CustomerID = CCuIL.Customer_ID_Lookup(ConfigurationID, "");
            if (!String.IsNullOrEmpty(CustomerID))
            {
                PropVal_Customer_Effective = PM.PropVal_Customer_Effective(EffectiveDate, CustomerID);
                return PropVal_Customer_Effective;
            }
            else
                return PropVal_Customer_Effective;
        }
        internal bool CompareOrder_BuildMaterials(string Customer_Build_ID, Partner_Order_Item Partner_Order_Item)
        {
            return Build_Partner_Order_Requirement_Match(Customer_Build_ID, Partner_Order_Item.Partner_ID,
                Partner_Order_Item.Partner_Order_Number);
        }
        public Boolean Build_Partner_Order_Requirement_Match(string CustomerBuildID, string PartnerID, string PartnerOrderNumber)
        {
            Boolean RequirementsMatch = false;

            List<PartnerOrderItemMaterialLookup> MaterialListArray = PartnerOrderItemMaterialLookup(PartnerID, PartnerOrderNumber, CustomerBuildID);

            if (MaterialListArray.Count > 0)
            {
                DataTable DTPartOrd = PM.BuildPartnerOrderRequirementMatch(CustomerBuildID);

                if (DTPartOrd.Rows.Count > 0)
                {
                    String ConfigurationID = DTPartOrd.Rows[0]["Configuration_ID"].ToString();

                    List<ComponentArray> ComponentListArray = BCL.BuildComponentLookup(ConfigurationID);

                    if (ComponentListArray.Count > 0)
                    {
                        Boolean Build_Order_Match = CompareBuildToOrder(PartnerID, PartnerOrderNumber, CustomerBuildID,
                            MaterialListArray, ComponentListArray);

                        if (Build_Order_Match)
                        {
                            Boolean Order_Build_Match = CompareOrderToBuild(PartnerID, PartnerOrderNumber,
                                CustomerBuildID, MaterialListArray, ComponentListArray);

                            if (Order_Build_Match)
                            {
                                RequirementsMatch = true;

                                //objPo.PartnerOrderItemMatched(PartnerID, PartnerOrderNumber, "System", CustomerBuildID);
                            }
                            else
                            {
                                //objPo.PartnerOrderItemMismatched(PartnerID, PartnerOrderNumber, "System",CustomerBuildID);
                            }

                        }
                        else
                        {
                            //objPo.PartnerOrderItemMismatched(PartnerID, PartnerOrderNumber, "System", CustomerBuildID);
                        }
                    }
                }
            }

            return RequirementsMatch;
        }
        public List<PartnerOrderItemMaterialLookup> PartnerOrderItemMaterialLookup(String PartnerOrderNumber, String PartnerID, String CustomerBuildID)
        {
            DataTable DtPartnerOIML = (DA.PartnerOrderItemLookup(PartnerOrderNumber, PartnerID, CustomerBuildID));
            List<PartnerOrderItemMaterialLookup> parts = new List<PartnerOrderItemMaterialLookup>();

            int intAu = DtPartnerOIML.Rows.Count;
            if (intAu > 0)
            {
                DataTable DtPartnerOIL = (DA.PartnerOrderItemMaterialLookup(PartnerOrderNumber, PartnerID, CustomerBuildID));
                int intAux = DtPartnerOIL.Rows.Count;
                if (intAux > 0)
                {
                    foreach (DataRow row in DtPartnerOIL.Rows)
                    {
                        var singleComponent = new PartnerOrderItemMaterialLookup();
                        singleComponent.MaterialID = row["Material_ID"].ToString();
                        singleComponent.Quantity = row["Material_Quantity"].ToString();
                        singleComponent.Quantity_UOM = row["Material_Quantity_UOM"].ToString();

                        parts.Add(singleComponent);
                    }
                }
            }

            return parts;
        }
        private bool CompareBuildToOrder(string Partner_ID, string Partner_Order_Number, string Customer_Build_ID, List<PartnerOrderItemMaterialLookup> Material_Array, List<ComponentArray> Component_Array)
        {
            bool Build_Order_Match = true;

            for (int i = 0; i < Component_Array.Count - 1; i++)
            {
                int Material_Index = Search_Material_Array(Material_Array, Component_Array[i].CatalogNumber);
                if (Material_Index > -1)
                {
                    if (Component_Array[i].MaterialTypeID == "CBL")
                    {
                        double Quantity = double.Parse(Material_Array[Material_Index].Quantity);
                        if (Material_Array[Material_Index].Quantity_UOM != "FT")
                        {
                            Material_Array[Material_Index].Quantity = (Quantity * 3.2808).ToString();
                        }

                        bool acceptable = (Math.Abs(double.Parse(Material_Array[Material_Index].Quantity) -
                                                    double.Parse(Component_Array[i].Quantity)) <= 3);
                        if (!acceptable)
                        {
                            Build_Order_Match = false;
                            break;
                        }
                    }
                    else
                    {
                        if (Material_Array[Material_Index].Quantity != Component_Array[i].Quantity)
                        {
                            Build_Order_Match = false;
                            break;
                        }
                    }
                }
                else
                {
                    Build_Order_Match = false;
                    break;
                }
            }

            return Build_Order_Match;
        }
        private int Search_Material_Array(List<PartnerOrderItemMaterialLookup> Material_Array, string Catalog_Number)
        {
            int Material_Index = -1;
            for (int i = 0; i < Material_Array.Count - 1; i++)
            {
                if (Material_Array[i].MaterialID == Catalog_Number)
                {
                    Material_Index = i;
                    break;
                }
            }

            return Material_Index;
        }
        private bool CompareOrderToBuild(string Partner_ID, string Partner_Order_Number, string Customer_Build_ID, List<PartnerOrderItemMaterialLookup> Material_Array, List<ComponentArray> Component_Array)
        {
            bool Order_Build_Match = true;
            string Local_Material_Status = String.Empty;
            int Material_Index = 0;
            for (int i = 0; i < Material_Array.Count - 1; i++)
            {
                int Component_Index = Search_Component_Array(Component_Array, Material_Array[i].MaterialID);
                if (Component_Index > -1)
                {
                    if (Component_Array[Component_Index].MaterialTypeID == "CBL")
                    {
                        double Quantity = double.Parse(Material_Array[i].Quantity);
                        if (Material_Array[i].Quantity_UOM != "FT")
                        {
                            Material_Array[i].Quantity = (Quantity * 3.2808).ToString();
                        }

                        bool acceptable = (Math.Abs(double.Parse(Material_Array[i].Quantity) -
                                                    double.Parse(Component_Array[Component_Index].Quantity)) <= 3);
                        if (acceptable)
                        {
                            Local_Material_Status = "Matched";
                            Material_Index = i;
                            break;
                        }
                        else
                        {
                            Order_Build_Match = false;
                            Local_Material_Status = "Mismatch";
                            Material_Index = i;
                            break;
                        }
                    }
                    else
                    {
                        if (Material_Array[i].Quantity == Component_Array[Component_Index].Quantity)
                        {
                            Local_Material_Status = "Matched";
                            Material_Index = i;
                            break;
                        }
                        else
                        {
                            Order_Build_Match = false;
                            Local_Material_Status = "Mismatch";
                            Material_Index = i;
                            break;
                        }
                    }
                }
                else
                {
                    Order_Build_Match = false;
                    Local_Material_Status = "Mismatch";
                    Material_Index = i;
                    break;
                }
            }

            if (Local_Material_Status == "Matched")
            {
                //Order_Build_Match = objPo.PartnerOrderItemMaterialMatched(Partner_ID, Partner_Order_Number, Customer_Build_ID, Material_Array[Material_Index].MaterialID, "System");
            }
            else
            {
                //Order_Build_Match = objPo.PartnerOrderItemMaterialMismatch(Partner_ID, Partner_Order_Number, Customer_Build_ID, Material_Array[Material_Index].MaterialID, "System");
            }

            return Order_Build_Match;

        }
        private int Search_Component_Array(List<ComponentArray> Component_Array, string Material_ID)
        {
            int Component_Index = -1;
            for (int i = 0; i < Component_Array.Count - 1; i++)
            {
                if (Component_Array[i].MaterialTypeID == Material_ID)
                {
                    Component_Index = i;
                    break;
                }
            }

            return Component_Index;
        }

    }

    public class PropVal_Customer_Effective
        {
            public PropVal_Customer_Effective()
            {
            }

            public String Property_Value_ID { get; set; }
            public String Customer_Market_ID { get; set; }
            public String Customer_Language_ID { get; set; }
            public int Customer_Type { get; set; }
            public String Customer_Type_Description { get; set; }
            public bool Ordering_Thru_Partner { get; set; }
            public String Partner_ID { get; set; }
            public bool Ordering_By_Components { get; set; }
            public bool Waiver_Allowed { get; set; }
            public bool Send_Waiver_To_Contact { get; set; }
            public bool Require_Reel_Tracking { get; set; }
        }

        public class Partner_Order_Item
        {
            public Partner_Order_Item()
            {
            }

            public string Partner_ID { get; set; }
            public string Partner_Order_Number { get; set; }
            public string Customer_Build_ID { get; set; }
            public short? Event_Type_Value_ID { get; set; }
        }

        public class Build_Header
        {
            public Build_Header()
            {
            }

            public string Configuration_ID { get; set; }
            public string Customer_Build_ID { get; set; }
            public int Customer_Build_Revision { get; set; }
            public string Build_Description { get; set; }
            public int Event_Type_Value_ID { get; set; }
        }

        public class Partner_Order
        {
            public Partner_Order()
            {
            }

            public string Partner_ID { get; set; }
            public string Partner_Order_Number { get; set; }
            public string Customer_ID { get; set; }
            public string Customer_Order_Number { get; set; }
            public string Ship_to_Address { get; set; }
            public string Ship_To_City { get; set; }
            public string Ship_to_State_Province_ID { get; set; }
            public string Ship_to_Country_ID { get; set; }
            public DateTime? Date_Created { get; set; }
            public DateTime? Date_Entered { get; set; }
            public DateTime? Date_Requested { get; set; }
            public short? Event_Type_Value_ID { get; set; }
        }

        public class PartnerOrderItemMaterialLookup
        {

            private string _MaterialID;

            public string MaterialID
            {
                get { return _MaterialID; }
                set { _MaterialID = value; }
            }

            private string _Quantity;

            public string Quantity
            {
                get { return _Quantity; }
                set { _Quantity = value; }
            }

            private string _Quantity_UOM;

            public string Quantity_UOM
            {
                get { return _Quantity_UOM; }
                set { _Quantity_UOM = value; }
            }



        }

        public class ComponentArray
        {

            private string _CatalogNumber;

            public string CatalogNumber
            {
                get { return _CatalogNumber; }
                set { _CatalogNumber = value; }
            }

            private string _MaterialTypeID;

            public string MaterialTypeID
            {
                get { return _MaterialTypeID; }
                set { _MaterialTypeID = value; }
            }

            private string _Quantity;

            public string Quantity
            {
                get { return _Quantity; }
                set { _Quantity = value; }
            }

            private string _QuantityUOM;

            public string QuantityUOM
            {
                get { return _QuantityUOM; }
                set { _QuantityUOM = value; }
            }

        }
    }


