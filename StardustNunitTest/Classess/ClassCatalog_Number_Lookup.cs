﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustNunitTest.DataAccess;
using StardustCoreLibs.App_Code.Classes;

namespace StardustNunitTest.Classess
{
    public class ClassCatalog_Number_Lookup
    {
        DataAccesscGF DA = new DataAccesscGF();
        ClassBuild_Key_Lookup BKL = new ClassBuild_Key_Lookup();
        KeyLookup _kl = new KeyLookup();
        public CatalogNumberLookup Catalog_Number_Lookup(String Input_Key, DateTime Effective_Date)
        {
            string Customer_BuildID = null;
            CatalogNumberLookup objNl = new CatalogNumberLookup();
            if (!string.IsNullOrEmpty(Input_Key))
            {
                _kl = BKL.BuildKeyLookup(Input_Key);
                DataTable DtCatalogNumberL = (DA.CatalogNumberLookupBH(_kl.ConfigurationID));

                int intAu = DtCatalogNumberL.Rows.Count;
                if (intAu > 0)
                {
                    Customer_BuildID = DtCatalogNumberL.Rows[0]["Customer_Build_ID"].ToString();
                    DataTable DtCatalogNumberlb = (DA.CatalogNumberLookupBStEf(Customer_BuildID, Effective_Date));

                    int intlb = DtCatalogNumberlb.Rows.Count;
                    if (intlb > 0)
                    {
                        objNl.BuildCatalogNumber = DtCatalogNumberlb.Rows[0]["Catalog_Number"].ToString();
                    }
                    else
                    {
                        objNl.BuildCatalogNumber = "BLD-" + Customer_BuildID;
                    }
                    objNl.returnstatuscatalogl = ReturnStatusCatalogL.Successful;
                }
                else
                {
                    objNl.BuildCatalogNumber = "";
                    objNl.returnstatuscatalogl = ReturnStatusCatalogL.Unsuccessful;
                }
            }
            else
            {
                objNl.BuildCatalogNumber = "";
                objNl.returnstatuscatalogl = ReturnStatusCatalogL.Unsuccessful;
            }
            return objNl;
        }
    }

    public enum ReturnStatusCatalogL { Successful, Unsuccessful }
    public class CatalogNumberLookup
    {
        public ReturnStatusCatalogL returnstatuscatalogl;

        private string _BuildCatalogNumber;
        public string BuildCatalogNumber
        {
            get { return _BuildCatalogNumber; }
            set { _BuildCatalogNumber = value; }
        }
    }
}
