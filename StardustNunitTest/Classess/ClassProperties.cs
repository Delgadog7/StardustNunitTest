﻿using StardustCoreLibs.App_Code.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustNunitTest.Classess;
namespace StardustNunitTest.Classess
{
    internal class ClassProperties
    {
        ClassCustomer_ID_Lookup CL = new ClassCustomer_ID_Lookup();
        ClassCategory_ID_Lookup CCL = new ClassCategory_ID_Lookup();
        public String GetBuildEnteredUOM(string Configuration_ID)
        {
            String EnteredUOM = null;
           
                PropertyIDLookup objLookup = new PropertyIDLookup();
                CategoryIDLookup objCategoryIdLookup = new CategoryIDLookup();
                objLookup = CL.PropertyIDLookup("EnteredUOM", DateTime.Now);

                String PropertyID = objLookup.PropertyID;
            if (String.IsNullOrEmpty(PropertyID))
            {
                objCategoryIdLookup = CCL.Category_ID_Lookup("Customer", DateTime.Now);

                String CategoryID = objCategoryIdLookup.CategoryID;
                if (String.IsNullOrEmpty(CategoryID))
                {
                    PropertyValueLookup objPvL = CL.PropertyValueLookup(new PropertyValueLookup
                    {
                        ConfigurationID = Configuration_ID,
                        LocationSequence = "0",
                        TapSequence = "0",
                        TetherSequence = "0",
                        FiberSequence = "0",
                        CategoryID = CategoryID,
                        PropertyID = PropertyID,
                        AllowOverrideValueID = "N",
                        BasePropertyID = "",
                        EffectiveDate = DateTime.Now

                    });

                    if (objPvL.Status == PropertyValueLookupReturn.Value_ID_Found)
                    {
                        EnteredUOM = objPvL.PropertyValueText;
                    }
                }
            }
            return EnteredUOM;
        }

        public int GetCOSideSlack(string Configuration_ID)
        {
            int Slack = -1;


            PropertyIDLookup objLookup = new PropertyIDLookup();
            CategoryIDLookup objCategoryIdLookup = new CategoryIDLookup();

            objLookup = CL.PropertyIDLookup("Slack", DateTime.Now);

            String PropertyID = objLookup.PropertyID;
            if (String.IsNullOrEmpty(PropertyID))
            {
                objCategoryIdLookup = CCL.Category_ID_Lookup("CO_Side", DateTime.Now);

                String CategoryID = objCategoryIdLookup.CategoryID;
                if (String.IsNullOrEmpty(CategoryID))
                {
                    PropertyValueLookup objPvL = CL.PropertyValueLookup(new PropertyValueLookup
                    {
                        ConfigurationID = Configuration_ID,
                        LocationSequence = "0",
                        TapSequence = "0",
                        TetherSequence = "0",
                        FiberSequence = "0",
                        CategoryID = CategoryID,
                        PropertyID = PropertyID,
                        AllowOverrideValueID = "N",
                        BasePropertyID = "",
                        EffectiveDate = DateTime.Now
                    });

                    if (objPvL.Status == PropertyValueLookupReturn.Value_ID_Found)
                    {
                        Slack = objPvL.PropertyValueNumber;
                    }
                }
            }
            return Slack;
        }

        public int GetFieldSideSlack(string Configuration_ID)
        {
            int Slack = -1;

            PropertyIDLookup objLookup = new PropertyIDLookup();
            CategoryIDLookup objCategoryIdLookup = new CategoryIDLookup();

              objLookup  = CL.PropertyIDLookup("Slack", DateTime.Now);

              String PropertyID = objLookup.PropertyID;
            if (String.IsNullOrEmpty(PropertyID))
            {
                objCategoryIdLookup =  CCL.Category_ID_Lookup("Field_Side", DateTime.Now);
                String CategoryID = objCategoryIdLookup.CategoryID;


                if (String.IsNullOrEmpty(CategoryID))
                {
                    PropertyValueLookup objPvL = CL.PropertyValueLookup(new PropertyValueLookup
                    {
                        ConfigurationID = Configuration_ID,
                        LocationSequence = "0",
                        TapSequence = "0",
                        TetherSequence = "0",
                        FiberSequence = "0",
                        CategoryID = CategoryID,
                        PropertyID = PropertyID,
                        AllowOverrideValueID = "N",
                        BasePropertyID = "",
                        EffectiveDate = DateTime.Now
                    });

                    if (objPvL.Status == PropertyValueLookupReturn.Value_ID_Found)
                    {
                        Slack = objPvL.PropertyValueNumber;
                    }
                }
            }
            return Slack;
        }
        public int GetLocationNextSpanDistance(string Configuration_ID, string LocationSequence)
        {
            int NextSpanDist = 0;

            PropertyIDLookup objLookup = new PropertyIDLookup();
            CategoryIDLookup objCategoryIdLookup = new CategoryIDLookup();

            objLookup = CL.PropertyIDLookup("NextSpanDist", DateTime.Now);
             String PropertyID = objLookup.PropertyID;
            if (String.IsNullOrEmpty(PropertyID))
            {
                objCategoryIdLookup = CCL.Category_ID_Lookup("Location", DateTime.Now);
                 String CategoryID = objCategoryIdLookup.CategoryID;
                if (String.IsNullOrEmpty(CategoryID))
                {
                    PropertyValueLookup objPvL = CL.PropertyValueLookup(new PropertyValueLookup
                    {
                        ConfigurationID = Configuration_ID,
                        LocationSequence = LocationSequence,
                        TapSequence = "0",
                        TetherSequence = "0",
                        FiberSequence = "0",
                        CategoryID = CategoryID,
                        PropertyID = PropertyID,
                        AllowOverrideValueID = "N",
                        BasePropertyID = "",
                        EffectiveDate = DateTime.Now
                    });

                    if (objPvL.Status == PropertyValueLookupReturn.Value_ID_Found)
                    {
                        NextSpanDist = objPvL.PropertyValueNumber;
                    }
                }
            }
            return NextSpanDist;
        }
    }
}
