﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustNunitTest.DataAccess;
using StardustNunitTest.Classess;
using StardustCoreLibs.App_Code.Classes;
using System.Data;

namespace StardustNunitTest.Classess
{
    public class ClassCustomer_ID_Lookup
    {
        DataAccesscGF DA = new DataAccesscGF();
        ClassCategory_ID_Lookup CIL = new ClassCategory_ID_Lookup();
        ClassBuild_Key_Lookup BKL = new ClassBuild_Key_Lookup();
        public enum SQLAction { Insert, Update, Delete };
        internal string Customer_ID_Lookup(string Input_Key, string ManufacturingBuildID = "")
        {
            string CustomerID = String.Empty, Local_Configuration_ID;
            if (!String.IsNullOrEmpty(Input_Key))
            {
                KeyLookup objKeyLookup = new KeyLookup();
                PropertyIDLookup objPropertyIDLookup = new PropertyIDLookup();
                CategoryIDLookup objCategoryIdLookup = new CategoryIDLookup();
                PropertyValueLookup objPropertyValueLookup = new PropertyValueLookup();

                objKeyLookup = BKL.BuildKeyLookup(Input_Key);
                Local_Configuration_ID = objKeyLookup.ConfigurationID;

                objPropertyIDLookup = PropertyIDLookup("CustomerID", DateTime.Now);
                if (objPropertyIDLookup.Status == ReturnStatusID.Property_ID_Found)
                {
                    objCategoryIdLookup = CIL.Category_ID_Lookup("Customer", DateTime.Now);
                    if (objCategoryIdLookup.Status == ReturnStatus.Category_Found)
                    {
                        objPropertyValueLookup.ConfigurationID = Local_Configuration_ID;
                        objPropertyValueLookup.ManufacturingBuildID = "";
                        objPropertyValueLookup.ItemSequence = "0";
                        objPropertyValueLookup.LocationSequence = "0";
                        objPropertyValueLookup.TapSequence = "0";
                        objPropertyValueLookup.TetherSequence = "0";
                        objPropertyValueLookup.LegSequence = "0";
                        objPropertyValueLookup.FiberSequence = "0";
                        objPropertyValueLookup.FiberDirection = "N/A";
                        objPropertyValueLookup.CategoryID = objCategoryIdLookup.CategoryID;
                        objPropertyValueLookup.PropertyID = objPropertyValueLookup.PropertyID;
                        objPropertyValueLookup.AllowOverrideValueID = "N";
                        objPropertyValueLookup.BasePropertyID = "Blank";
                        objPropertyValueLookup.EffectiveDate = DateTime.Now;

                            objPropertyValueLookup = PropertyValueLookup(objPropertyValueLookup);
                        if (objPropertyValueLookup.FunctionF == Function_Found.Yes)
                            CustomerID = objPropertyValueLookup.PropertyValueText;
                    }
                }
            }
            return CustomerID;
        }
        public PropertyIDLookup PropertyIDLookup(String PropertyKey, DateTime EffectiveDate)
        {
            PropertyIDLookup objCpq = new PropertyIDLookup();
            String PropertyID = "";

            DataTable DtPropertyIDLookup = PropertyIDLookupByN(PropertyKey, EffectiveDate);

            int intPL = DtPropertyIDLookup.Rows.Count;
            if (intPL > 0)
            {
                objCpq.PropertyID = DtPropertyIDLookup.Rows[0]["Property_ID"].ToString();
                objCpq.Status = ReturnStatusID.Property_ID_Found;
            }
            else
            {
                objCpq.Status = ReturnStatusID.Property_ID_Not_Found;
            }

            if (objCpq.Status == ReturnStatusID.Property_ID_Not_Found)
            {
                DataTable DtPropertyIDMLookupD = PropertyIDLookupByDS(PropertyKey, EffectiveDate);

                int intIDMD = DtPropertyIDMLookupD.Rows.Count;
                if (intIDMD > 0)
                {
                    objCpq.PropertyID = DtPropertyIDMLookupD.Rows[0]["Property_ID"].ToString();
                    objCpq.Status = ReturnStatusID.Property_ID_Found;
                }
                else
                {
                    DataTable DtPropertyIDMLookupDS = PropertyIDLookupByD(PropertyKey, EffectiveDate);

                    int intIDMDS = DtPropertyIDMLookupDS.Rows.Count;
                    if (intIDMDS > 0)
                    {
                        objCpq.PropertyID = DtPropertyIDMLookupDS.Rows[0]["Property_ID"].ToString();
                        objCpq.Status = ReturnStatusID.Property_ID_Found;
                    }
                    else
                    {
                        objCpq.Status = ReturnStatusID.Property_ID_Not_Found;
                    }
                }
            }

            return objCpq;
        }
        public DataTable PropertyIDLookupByN(String PropertyKey, DateTime EffectiveDate)
        {

            DataTable DtPropertyIDLookup = (DA.PropertyIDLookupByName(PropertyKey, EffectiveDate));

            return DtPropertyIDLookup;

        }
        public DataTable PropertyIDLookupByD(String PropertyKey, DateTime EffectiveDate)
        {

            DataTable DtPropertyIDLookupLD = (DA.PropertyIDLookupByDescLD(PropertyKey, EffectiveDate));

            return DtPropertyIDLookupLD;

        }
        public DataTable PropertyIDLookupByDS(String PropertyKey, DateTime EffectiveDate)
        {

            DataTable DtPropertyIDLookupLD = (DA.PropertyIDLookupByDescSD(PropertyKey, EffectiveDate));

            return DtPropertyIDLookupLD;

        }
        public PropertyValueLookup PropertyValueLookup(PropertyValueLookup objPVL)
        {
            DataRow dr = null, ds = null, dp = null, dx = null;

            if (objPVL.EffectiveDate == null)
            {
                objPVL.EffectiveDate = DateTime.Now;
            }

            if (string.IsNullOrEmpty(objPVL.ManufacturingBuildInstanceID))
            {
                if (string.IsNullOrEmpty(objPVL.ManufacturingBuildID))
                {
                    if (string.IsNullOrEmpty(objPVL.ConfigurationID))
                    {
                        objPVL.ObjectT = Object_Type.Undetermined;
                    }
                    else
                    {
                        objPVL.ObjectT = Object_Type.Build;
                    }
                }
                else
                {
                    objPVL.ObjectT = Object_Type.Mfg_Build;
                }
            }
            else
            {
                objPVL.ObjectT = Object_Type.Mfg_Build_Instance;
            }

            if (objPVL.ObjectT == Object_Type.Undetermined)
            {
                objPVL.Status = PropertyValueLookupReturn.Value_ID_Not_Found;
            }
            else
            {
                dr = CheckCategory(objPVL);

                if (dr != null)
                {
                    ds = CheckCategoryPropertyXRef(objPVL);

                    if (ds != null)
                    {
                        dx = CheckProperty(objPVL);

                        objPVL.HeaderPropertyID = dx["Property_ID"].ToString();
                        objPVL.HeaderPropertyName = dx["Property_Name"].ToString();
                        objPVL.HeaderPropertyUsage = dx["Property_Usage"].ToString();
                        objPVL.HeaderPropertyUsageDesc = dx["Property_Usage_Description"].ToString();
                        objPVL.HeaderPropertyValueType = dx["Property_Value_Type"].ToString();
                        objPVL.HeaderPropertyValueTypeDesc = dx["Propertry_Value_Type_Description"].ToString();
                        objPVL.PropertyUsage = dx["Property_Usage"].ToString();
                        objPVL.HeaderLookupDataTable = dx["Lookup_Data_Table"].ToString();
                        objPVL.HeaderShowProperty = dx["ShowProperty"].ToString();

                        if (dx != null)
                        {
                            objPVL = FetchPropertyValue(objPVL);

                            if (string.IsNullOrEmpty(objPVL.PropertyValue))
                            {
                                objPVL.Status = PropertyValueLookupReturn.Value_ID_Not_Found;
                            }
                        }
                    }
                }
            }
            return objPVL;
        }
        public DataRow CheckCategory(PropertyValueLookup objPVL)
        {
            DataRow dr = null;
            DataTable DTPropertyValue = DA.CheckCategory(objPVL);

            int intAu = DTPropertyValue.Rows.Count;
            if (intAu > 0)
            {
                dr = DTPropertyValue.Rows[0];
                return dr;
            }

            return dr;
        }
        public DataRow CheckProperty(PropertyValueLookup objPVL)
        {
            DataRow dr = null;
            DataTable DTPropertyValue = DA.CheckProperty(objPVL);

            int intAu = DTPropertyValue.Rows.Count;
            if (intAu > 0)
            {
                dr = DTPropertyValue.Rows[0];
                return dr;
            }

            return dr;
        }
        public DataRow CheckCategoryPropertyXRef(PropertyValueLookup objPVL)
        {
            DataRow dr = null;
            DataTable DTPropertyValueXRef = DA.CheckCategoryPropertyXRef(objPVL);

            int intAu = DTPropertyValueXRef.Rows.Count;
            if (intAu > 0)
            {
                dr = DTPropertyValueXRef.Rows[0];
                return dr;
            }

            return dr;
        }
        public PropertyValueLookup FetchPropertyValue(PropertyValueLookup objPVL)
        {

            if (objPVL.ObjectT == Object_Type.Build)
            {
                objPVL = GetBuildProperty(objPVL);

            }
            else
            {
                if (objPVL.ObjectT == Object_Type.Mfg_Build || objPVL.ObjectT == Object_Type.Mfg_Build_Instance)
                {
                    if (objPVL.ObjectT == Object_Type.Mfg_Build)
                    {
                       BKL.BuildKeyLookup(objPVL.ManufacturingBuildID);
                        GetMfgBuildProperty(objPVL);
                    }

                    if (objPVL.ObjectT == Object_Type.Mfg_Build_Instance)
                    {
                       BKL.BuildKeyLookup(objPVL.ManufacturingBuildInstanceID);
                        GetMfgBuildInstanceProperty(objPVL);

                        if (objPVL.Status != PropertyValueLookupReturn.Value_ID_Found)
                        {
                            GetMfgBuildProperty(objPVL);
                        }
                    }
                }
            }

            if (objPVL.Status == PropertyValueLookupReturn.Value_ID_Found)
            {
                if (objPVL.ObjectT == Object_Type.Build)
                {
                    if (objPVL.AllowOverrideValueID == "Yes")
                    {
                        objPVL = SubProcessPropertyVLookup(objPVL);
                    }
                }

            }
            else
            {
                objPVL = Check_For_Function(objPVL);

                if (objPVL.FunctionF == Function_Found.Yes)
                {
                    objPVL.PropertyValue = objPVL.FunctionValue;
                    objPVL.Status = PropertyValueLookupReturn.Value_ID_Found;
                }
                else
                {
                    if (objPVL.ObjectT == Object_Type.Build)
                    {
                        if (objPVL.AllowOverrideValueID == "Yes")
                        {
                            objPVL = SubProcessPropertyVLookup(objPVL);
                        }
                    }
                    if (objPVL.ObjectT == Object_Type.Mfg_Build || objPVL.ObjectT == Object_Type.Mfg_Build_Instance)
                    {
                        objPVL = checkOverride(objPVL);
                        if (objPVL.Override == LocalOverrideFound.True)
                        {
                            objPVL.PropertyValue = objPVL.OverridePropertyValue;
                            objPVL.Status = PropertyValueLookupReturn.Value_ID_Found;
                            if (objPVL.PropertyUsage != "Customer")
                            {
                                objPVL.OverwriteFlag = true;
                                PropertyValueAddUpdate(objPVL);
                            }
                            objPVL = SubProcessPropertyVLookup_END(objPVL, objPVL.PropertyValue, true);
                        }
                        else
                        {

                        }
                    }
                }
            }

            return objPVL;
        }
        public PropertyValueLookup GetMfgBuildProperty(PropertyValueLookup objPVL)
        {
            DataTable DTMfgBuildProp = DA.GetMfgBuildProperty(objPVL);

            int intAu = DTMfgBuildProp.Rows.Count;
            if (intAu > 0)
            {
                objPVL.PropertyValue = DTMfgBuildProp.Rows[0]["Property_Value"].ToString();
                objPVL.Status = PropertyValueLookupReturn.Value_ID_Found;
            }
            else
            {
                objPVL.PropertyValue = "";
                objPVL.Status = PropertyValueLookupReturn.Value_ID_Not_Found;
            }
            return objPVL;
        }
        public PropertyValueLookup Check_For_Function(PropertyValueLookup objPVL)
        {
            DataTable DTCheckforF = DA.Check_For_Function(objPVL);

            int intAu = DTCheckforF.Rows.Count;
            if (intAu > 0)
            {
                objPVL.FunctionValue = DTCheckforF.Rows[0]["Function_Name"].ToString();
                objPVL.FunctionF = Function_Found.Yes;
            }
            else
            {
                objPVL.PropertyValue = "";
                objPVL.Status = PropertyValueLookupReturn.Value_ID_Not_Found;
            }
            return objPVL;
        }
        public PropertyValueLookup GetMfgBuildInstanceProperty(PropertyValueLookup objPVL)
        {
            DataTable DTMfgBuildInstanceProp = DA.GetMfgBuildProperty(objPVL);

            int intAu = DTMfgBuildInstanceProp.Rows.Count;
            if (intAu > 0)
            {
                objPVL.PropertyValue = DTMfgBuildInstanceProp.Rows[0]["Property_Value"].ToString();
                objPVL.Status = PropertyValueLookupReturn.Value_ID_Found;
            }
            else
            {
                objPVL.PropertyValue = "";
                objPVL.Status = PropertyValueLookupReturn.Value_ID_Not_Found;
            }
            return objPVL;
        }
        public PropertyValueLookup SubProcessPropertyVLookup(PropertyValueLookup objPVL)
        {
            string LocalPropertyValue = null;
            Boolean PropertyValueFound = false;
            objPVL = checkOverride(objPVL);

            if (objPVL.Override == LocalOverrideFound.True)
            {
                LocalPropertyValue = objPVL.OverridePropertyValue;
                PropertyValueFound = true;
            }

            objPVL = SubProcessPropertyVLookup_END(objPVL, LocalPropertyValue, PropertyValueFound);

            return objPVL;
        }
        public PropertyValueLookup SubProcessPropertyVLookup_END(PropertyValueLookup objPVL, string LocalPropertyValue, Boolean PropertyValueFound)
        {
            if (PropertyValueFound)
            {
                if (objPVL.PropertyValueType == "Numeric")
                {
                    int checkint = 0;
                    objPVL.PropertyValueText = "";
                    if (Int32.TryParse(LocalPropertyValue, out checkint))
                    {
                        objPVL.PropertyValueNumber = checkint;
                    }

                }
                else
                {
                    objPVL.PropertyValueText = LocalPropertyValue;
                    objPVL.PropertyValueNumber = 0;
                }
            }
            else
            {
                objPVL.PropertyValueText = null;
                objPVL.PropertyValueNumber = -1;
            }

            return objPVL;
        }
        public PropertyValueLookup checkOverride(PropertyValueLookup objPVL)
        {
            DataTable DTCheckOverride = DA.CheckOverride(objPVL);
            RuleProcessing objR = new RuleProcessing();
            int intAu = DTCheckOverride.Rows.Count;
            if (intAu > 0)
            {
                objPVL.Override = LocalOverrideFound.False;

                foreach (DataRow row in DTCheckOverride.Rows)
                {
                    DataTable DTRuleProcessing = DA.Rule_Proccesing_Header(objR);
                    int intRp = DTRuleProcessing.Rows.Count;
                    if (intRp > 0)
                    {
                        objPVL.OverridePropertyValue = row["Override"].ToString();
                        objPVL.PropertyValueType = row["Type"].ToString();
                        objPVL.Override = LocalOverrideFound.True;
                    }

                }
            }
            return objPVL;
        }
        public PropertyValueLookup GetBuildProperty(PropertyValueLookup objPVL)
        {
            DataTable DTGetBuildProperty = null;
            switch (objPVL.CategoryType.ToString())
            {
                case "Build":
                    DTGetBuildProperty = DA.GetBuildProperty_ByBuild(objPVL);
                    break;

                case "Location":
                    DTGetBuildProperty = DA.GetBuildProperty_ByLocation(objPVL);
                    break;

                case "TAP":
                    DTGetBuildProperty = DA.GetBuildProperty_ByTap(objPVL);
                    break;

                case "Tether":
                    DTGetBuildProperty = DA.GetBuildProperty_ByTether(objPVL);
                    break;

                case "Fiber":
                    DTGetBuildProperty = DA.GetBuildProperty_ByFiber(objPVL);
                    break;

            }

            int intAu = DTGetBuildProperty.Rows.Count;
            if (intAu > 0)
            {
                objPVL.PropertyValue = DTGetBuildProperty.Rows[0]["Property_Value_ID"].ToString();
                objPVL.Status = PropertyValueLookupReturn.Value_ID_Found;
            }

            return objPVL;
        }
        public PropertyValueLookup PropertyValueAddUpdate(PropertyValueLookup objPVL)
        {
            PropertyValueLookup(objPVL);

            if (objPVL.Status == PropertyValueLookupReturn.Value_ID_Not_Found)
            {
                if (!String.IsNullOrEmpty(objPVL.PropertyValue))
                {
                    SetPropertyValue(objPVL, SQLAction.Insert.ToString());
                }
            }
            else
            {
                if (objPVL.OverwriteFlag)
                {
                    if (!String.IsNullOrEmpty(objPVL.PropertyValue))
                    {
                        SetPropertyValue(objPVL, SQLAction.Update.ToString());
                    }
                    else
                    {
                        SetPropertyValue(objPVL, SQLAction.Delete.ToString());
                    }
                }
            }

            return objPVL;
        }
        public PropertyValueLookup SetPropertyValue(PropertyValueLookup objPVL, String SQLAction)
        {

            if (objPVL.ManufacturingBuildInstanceID == "")
            {
                if (objPVL.ManufacturingBuildID == "")
                {
                    if (objPVL.ConfigurationID == "")
                    {
                        objPVL.StatProp = ReturnStatusProp.Property_Value_Not_Set;
                    }
                    else
                    {
                        objPVL = SetBuildProperty(objPVL, SQLAction);
                    }
                }
                else
                {
                    objPVL = SetManufacturingBuildProperty(objPVL, SQLAction);
                }
            }
            else
            {
                objPVL = SetManufacturingBuildInstanceProperty(objPVL, SQLAction);
            }

            return objPVL;
        }
        public PropertyValueLookup SetBuildProperty(PropertyValueLookup objPVL, string SQLAction)
        {
            try
            {
                DataTable DtBuildProperty = (DA.GetCategoryType(objPVL));
                String CategoryType = "";

                int intPL = DtBuildProperty.Rows.Count;
                if (intPL > 0)
                {
                    CategoryType = DtBuildProperty.Rows[0]["Category_Type"].ToString();

                    switch (CategoryType)
                    {
                        case "Build":
                            switch (SQLAction)
                            {
                                case "Insert":
                                    DA.BuildProperty_Insert(objPVL);
                                    break;

                                case "Update":
                                    DA.BuildProperty_Update(objPVL);
                                    break;

                                case "Delete":
                                    DA.BuildProperty_Delete(objPVL);
                                    break;
                            }
                            break;

                        case "Location":
                            switch (SQLAction)
                            {
                                case "Insert":
                                    DA.LocationProperty_Insert(objPVL);
                                    break;

                                case "Update":
                                    DA.LocationProperty_Update(objPVL);
                                    break;

                                case "Delete":
                                    DA.LocationProperty_Delete(objPVL);
                                    break;
                            }
                            break;

                        case "TAP":
                            switch (SQLAction)
                            {
                                case "Insert":
                                    DA.TapProperty_Insert(objPVL);
                                    break;

                                case "Update":
                                    DA.TapProperty_Update(objPVL);
                                    break;

                                case "Delete":
                                    DA.TapProperty_Delete(objPVL);
                                    break;
                            }
                            break;

                        case "Tether":

                            switch (SQLAction)
                            {
                                case "Insert":
                                    DA.TetherProperty_Insert(objPVL);
                                    break;

                                case "Update":
                                    DA.TetherProperty_Update(objPVL);
                                    break;

                                case "Delete":
                                    DA.TetherProperty_Delete(objPVL);
                                    break;
                            }
                            break;

                        case "Fiber":

                            switch (SQLAction)
                            {
                                case "Insert":
                                    DA.FiberProperty_Insert(objPVL);
                                    break;

                                case "Update":
                                    DA.FiberProperty_Update(objPVL);
                                    break;

                                case "Delete":
                                    DA.FiberProperty_Delete(objPVL);
                                    break;
                            }
                            break;
                    }
                    objPVL.StatProp = ReturnStatusProp.Property_Value_Set;
                }

            }
            catch (Exception e)
            {
                objPVL.StatProp = ReturnStatusProp.Property_Value_Not_Set;
            }
            return objPVL;
        }
        public PropertyValueLookup SetManufacturingBuildProperty(PropertyValueLookup objPVL, String SQLAction)
        {
            try
            {
                switch (SQLAction)
                {
                    case "Insert":
                        DA.MfgInstanceProperty_Insert(objPVL);
                        break;

                    case "Update":
                        DA.MfgInstanceProperty_Update(objPVL);
                        break;

                    case "Delete":
                        DA.MfgInstanceProperty_Delete(objPVL);
                        break;
                }

                objPVL.StatProp = ReturnStatusProp.Property_Value_Set;
            }
            catch (Exception e)
            {
                objPVL.StatProp = ReturnStatusProp.Property_Value_Not_Set;
            }
            return objPVL;
        }
        public PropertyValueLookup SetManufacturingBuildInstanceProperty(PropertyValueLookup objPVL, String SQLAction)
        {
            try
            {
                switch (SQLAction)
                {
                    case "Insert":
                        DA.MfgProperty_Insert(objPVL);
                        break;

                    case "Update":
                        DA.MfgProperty_Update(objPVL);
                        break;

                    case "Delete":
                        DA.MfgProperty_Delete(objPVL);
                        break;
                }

                objPVL.StatProp = ReturnStatusProp.Property_Value_Set;
            }
            catch (Exception e)
            {
                objPVL.StatProp = ReturnStatusProp.Property_Value_Not_Set;
            }
            return objPVL;
        }
    }
}
