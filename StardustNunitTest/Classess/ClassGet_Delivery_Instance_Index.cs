﻿using StardustNunitTest.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StardustNunitTest.Classess
{
    public class ClassGet_Delivery_Instance_Index
    {
        DataAccesscGF DA = new DataAccesscGF();
        public int GetDeliveryInstanceIndex(string DeliveryNumber, string CustomerNumber)
        {
            int InstanceIndex = 0;
            DataTable DTERP_Delivery_1_RS = (DA.ERPDelivery1RS(DeliveryNumber, CustomerNumber));
            int interp1 = DTERP_Delivery_1_RS.Rows.Count;
            if (interp1 > 0)
            {
                InstanceIndex = Convert.ToInt32(DTERP_Delivery_1_RS.Rows[0]["Instance_Index"]);
            }
            else
            {
                DataTable DTERP_Delivery_2_RS = (DA.ERPDelivery2RS(DeliveryNumber));
                int interp2 = DTERP_Delivery_2_RS.Rows.Count;
                if (interp2 > 0)
                {

                    InstanceIndex = Convert.ToInt32(DTERP_Delivery_2_RS.Rows[0]["Instance_index"]) + 1;
                }
                else
                {
                    InstanceIndex = 1;
                }
            }
            return InstanceIndex;

        }
    }
}
