﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustNunitTest.DataAccess;

namespace StardustNunitTest.Classess
{
    public class ClassContact_ID_Lookup
    {
        DataAccesscGF DA = new DataAccesscGF();
        public string ContactIDLookup(string Email, string Phone, string Name)
        {
            Contact_ID_Lookup objCt = new Contact_ID_Lookup();
            objCt.ContactEmail = Email;
            objCt.ContactPhone = Phone;
            objCt.ContactName = Name;
            DataTable DtContactIDLookup = (DA.ContactIDLookup(objCt));

            int intAu = DtContactIDLookup.Rows.Count;
            if (intAu > 0)
            {
                objCt.ContactID = DtContactIDLookup.Rows[0]["Contact_ID"].ToString();
            }
            else
            {
                objCt.SerialNumber = Serial_Generator("CONTACT_ID", "", "");
                if (!String.IsNullOrEmpty(objCt.SerialNumber))
                {
                    //DA.InsertContactIDLookup(objCt);
                    objCt.ContactID = objCt.SerialNumber;
                }
            }

            return objCt.ContactID;
        }

        public String Serial_Generator(String SerialNumberSetID, String SerialNumberPrefix, String SerialNumberSuffix)
        {
            SerialGenerator objSG = new SerialGenerator();
            objSG.SerialNumberSetID = SerialNumberSetID;
            objSG.SerialNumberSerialNumberPrefix = SerialNumberPrefix;
            objSG.SerialNumberSerialNumberSuffix = SerialNumberSuffix;
            string Serial_Number_Value = "", Serial_Number_String = "", SerialFormat = "", TempString;
            int Pad_Value = 0;
            bool Serial_Number_Detail;

            DataTable dtSerialNumberHeaderRS = (DA.Serial_Number_Header_RS(DateTime.Now, objSG.SerialNumberSetID));
            int intsg = dtSerialNumberHeaderRS.Rows.Count;
            if (intsg > 0)
            {
                Serial_Number_Detail = false;
                DataTable dtSerialNumberDetailRS = (DA.Serial_Number_Detail_RS(objSG));
                int intsnd = dtSerialNumberHeaderRS.Rows.Count;
                if (intsnd > 0)
                {
                    objSG.Current_Serial_Number = dtSerialNumberDetailRS.Rows[0]["Last_Serial_Value"].ToString() + dtSerialNumberHeaderRS.Rows[0]["Serial_Increment"].ToString();
                    DA.Serial_Number_Detail_RS(objSG);
                    Serial_Number_Detail = true;
                    SerialFormat = dtSerialNumberHeaderRS.Rows[0]["Serial_Format"].ToString();
                    switch (SerialFormat)
                    {
                        case "1":
                        //case "Numeric":
                            Serial_Number_String = objSG.Current_Serial_Number.Trim();

                            break;
                        case "2":
                        //case "Alphanumeric":
                            Serial_Number_String = "";

                            string array = "0123456789ABCDEFGHJKLMNPRSTUVWXYZ";
                            char[] Character_Array = array.ToCharArray();

                            string Working_Dividend = objSG.Current_Serial_Number;
                            int Working_Remainder = 0;
                            if (Working_Remainder == 0)
                            {
                                Working_Remainder = (Working_Dividend.Length % 33);
                            }
                            break;
                        case "3":
                        //case "Hexadecimal":
                            Serial_Number_String = int.Parse(objSG.Current_Serial_Number).ToString("X");
                            break;
                    }
                }
                Pad_Value = Convert.ToInt32(dtSerialNumberHeaderRS.Rows[0]["Serial_Length"].ToString()) - Serial_Number_String.Length;

                if (Pad_Value > 0)
                {
                    TempString = "0" + Pad_Value + Serial_Number_String.Trim();
                }
                else
                {
                    TempString = Serial_Number_String.Substring(dtSerialNumberDetailRS.Rows[0]["Serial_Length"].ToString().Length);
                }
                Serial_Number_Value = dtSerialNumberDetailRS.Rows[0]["Prefix_Text"].ToString() + TempString + dtSerialNumberDetailRS.Rows[0]["Suffix_Text"].ToString();

            }
            return Serial_Number_Value;
        }
    }
    public class Contact_ID_Lookup
    {

        private string _SerialNumber;
        public string SerialNumber
        {
            get { return _SerialNumber; }
            set { _SerialNumber = value; }
        }

        private string _ContactName;
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        private string _ContactPhone;
        public string ContactPhone
        {
            get { return _ContactPhone; }
            set { _ContactPhone = value; }
        }

        private string _ContactEmail;
        public string ContactEmail
        {
            get { return _ContactEmail; }
            set { _ContactEmail = value; }
        }

        private string _ContactID;
        public string ContactID
        {
            get { return _ContactID; }
            set { _ContactID = value; }
        }


    }
    public class SerialGenerator
    {
        private string _SerialNumberSetID;
        public string SerialNumberSetID
        {
            get { return _SerialNumberSetID; }
            set { _SerialNumberSetID = value; }
        }

        private string _SerialNumberSerialNumberPrefix;
        public string SerialNumberSerialNumberPrefix
        {
            get { return _SerialNumberSerialNumberPrefix; }
            set { _SerialNumberSerialNumberPrefix = value; }
        }

        private string _SerialNumberSerialNumberSuffix;
        public string SerialNumberSerialNumberSuffix
        {
            get { return _SerialNumberSerialNumberSuffix; }
            set { _SerialNumberSerialNumberSuffix = value; }
        }



        private string _Current_Serial_Number;
        public string Current_Serial_Number
        {
            get { return _Current_Serial_Number; }
            set { _Current_Serial_Number = value; }
        }



        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return EffectiveDate; }
            set { _EffectiveDate = value; }
        }

        private long _LastSerialValue;
        public long LLastSerialValue
        {
            get { return _LastSerialValue; }
            set { _LastSerialValue = value; }
        }

        private long _SerialIncrement;
        public long SerialIncrement
        {
            get { return _SerialIncrement; }
            set { _SerialIncrement = value; }
        }


    }
}
