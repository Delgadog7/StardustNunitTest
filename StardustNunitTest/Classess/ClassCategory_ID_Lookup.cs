﻿using StardustNunitTest.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StardustNunitTest.Classess
{
    public class ClassCategory_ID_Lookup
    {
        public CategoryIDLookup Category_ID_Lookup(String Category_Description, DateTime Effective_Date)
        {
            DataAccesscGF DA = new DataAccesscGF();
            CategoryIDLookup objKi = new CategoryIDLookup();

            DataTable DtCategoryIDL = (DA.CategoryIDLookup(Category_Description, Effective_Date));

            int intAu = DtCategoryIDL.Rows.Count;
            if (intAu > 0)
            {
                objKi.CategoryID = DtCategoryIDL.Rows[0]["Category_ID"].ToString();
                objKi.Status = ReturnStatus.Category_Found;
            }
            else
            {
                objKi.Status = ReturnStatus.Category_Not_Found;
            }

            return objKi;
        }
    }
    public enum ReturnStatus { Category_Found, Category_Not_Found }
    public class CategoryIDLookup
    {
        public ReturnStatus Status { get; set; }

        private string _CategoryID;
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }

    }
}
