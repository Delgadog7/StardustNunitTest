﻿using StardustNunitTest.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustCoreLibs.App_Code.Classes;

namespace StardustNunitTest.Classess
{
    public class ClassConfiguration_ID_Lookup
    {
        ClassBuild_Key_Lookup CBKL = new ClassBuild_Key_Lookup();
        public String ConfigurationIDLookup(String Input_Key)
        {
            KeyLookup objk = new KeyLookup();
            objk = CBKL.BuildKeyLookup(Input_Key);

            return objk.ConfigurationID;
        }
    }
}
