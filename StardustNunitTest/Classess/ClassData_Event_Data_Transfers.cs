﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustNunitTest.DataAccess;
using StardustCoreLibs.App_Code.Classes;

namespace StardustNunitTest.Classess
{
    public class ClassData_Event_Data_Transfers
    {
        DataAccesscGF DA = new DataAccesscGF();
        public void Data_Event_DTransfer(string EventTypeValueID, string TransferType)
        {
            DataEventDataTransfers objDT = new DataEventDataTransfers();
            objDT.EventTypeValueID = EventTypeValueID;
            DataTable DtDataTransfer = (DA.DataTransferHeaderEffv(objDT));

            int intAu = DtDataTransfer.Rows.Count;
            if (intAu > 0)
            {
                foreach (DataRow row in DtDataTransfer.Rows)
                {
                    objDT.PropertyVID = row["Property_Value_ID"].ToString();
                    objDT.PropertyID = row["Property_ID"].ToString();
                    objDT.EventTypeID = row["Event_Type_ID"].ToString();
                    objDT.EventTypeValueID = row["Event_Type_Value_ID"].ToString();
                    objDT.EventRelevance = "2";
                    objDT.TransferType = TransferType;
                    Data_Transfer(objDT);
                }

            }

        }
        public void Data_Transfer(DataEventDataTransfers objDT)
        {

            KeyLookup objkl = new KeyLookup();
            ClassBuild_Key_Lookup clasbjl = new ClassBuild_Key_Lookup();
            objkl = clasbjl.BuildKeyLookup(objDT.TransferKey1);

            DataTable DtDataTransferH = (DA.DataTransferHeader(objDT));

            int AuTh = DtDataTransferH.Rows.Count;

            if (AuTh > 0)
            {
                foreach (DataRow row in DtDataTransferH.Rows)
                {
                    if (row["Data_Format_Execution_Description"].ToString() == "Synchronous")
                    {
                        ExecuteTransfer(objkl, objDT, row);
                    }
                    else
                    {
                        if (row["Data_Format_Execution_Description"].ToString() == "Asynchronous")
                        {
                            ExecuteTransfer(objkl, objDT, row);
                        }
                    }
                }
            }
        }
        public void ExecuteTransfer(KeyLookup objkl, DataEventDataTransfers objDT, DataRow row)
        {
            if (row["Transfer_Type_Description"].ToString() == "Input")
            {
                DataTable DTProcessTDetail = DA.ProcessTransferDetail(row["Transfer_ID"].ToString(), DateTime.Now);
                objDT.TransferMethod = row["Transfer_Method_Description"].ToString();

                int AUpD = DTProcessTDetail.Rows.Count;

                if (AUpD > 0)
                {
                    foreach (DataRow Srow in DTProcessTDetail.Rows)
                    {
                        Boolean DataDelete = Convert.ToBoolean(row["Remote_Data_Delete"]);
                        objDT = RemoteDataPathName(objkl, objDT, Srow);
                        if (row["Transfer_Type_Description"].ToString() == "Input")
                        {
                            if (objDT.TransferMethod == "Fileshare")
                            {

                            }
                            else
                            {
                                if (objDT.TransferMethod == "ftp")
                                {

                                }
                            }
                        }
                        else
                        {
                            if (row["Transfer_Type_Description"].ToString() == "Output")
                            {

                            }
                        }
                    }
                }
                else
                {

                }
            }
            else
            {
                if (row["Transfer_Type"].ToString() == "Output")
                {

                }
            }
        }

        public DataEventDataTransfers RemoteDataPathName(KeyLookup objkl, DataEventDataTransfers objDT, DataRow Srow)
        {
            String RemoteDPAth = Srow["Remote_Data_Path"].ToString();
            String RemoteDName = Srow["Remote_Data_Name"].ToString();

            List<String> Variable_Name = new List<String> { "*MFGID", "*MFGINSTID", "*BLDID", "*REVID", "*CSTID",
            "*SITEID", "*ISOTD", "*MDYDT", "*YMDDT", "*SYSDT", "*SYSTM", "*PBLDID"};

            foreach (var variables in Variable_Name)
            {
                if (RemoteDPAth.Contains(variables) || RemoteDName.Contains(variables))
                {
                    String variables_Value = GetVariableValue(objkl, objDT, Srow, variables);
                    String TempString = null;
                    if (RemoteDPAth.Contains(variables))
                    {
                        TempString = RemoteDPAth.Replace(variables, variables_Value);
                        RemoteDPAth = TempString;
                    }
                    else
                    {
                        if (RemoteDName.Contains(variables))
                        {
                            TempString = RemoteDName.Replace(variables, variables_Value);
                            RemoteDName = TempString;
                        }
                    }
                }
            }
            objDT.LocalRemoteDataPathValue = RemoteDPAth;
            objDT.LocalRemoteDataName = RemoteDName;
            return objDT;
        }
        public String GetVariableValue(KeyLookup objkl, DataEventDataTransfers objDT, DataRow Srow, String variables)
        {
            String variables_Value = "NA";

            switch (variables)
            {
                case "*MFGID":
                    variables_Value = objkl.Manufacturing_Build_ID;
                    break;

                case "*MFGINSTID":
                    variables_Value = objkl.Manufacturing_Build_Instance_ID;
                    break;

                case "*BLDID":
                    DataTable DTCBuildID = DA.CustomerBuildID(objkl.ConfigurationID);

                    int AuBID = DTCBuildID.Rows.Count;

                    if (AuBID > 0)
                    {
                        variables_Value = DTCBuildID.Rows[0]["Customer_Build_ID"].ToString();
                    }

                    break;

                case "*REVID":
                    DataTable DTCBuildRevision = DA.CustomerBuildRevision(objkl.ConfigurationID);

                    int AuBr = DTCBuildRevision.Rows.Count;

                    if (AuBr > 0)
                    {
                        variables_Value = DTCBuildRevision.Rows[0]["Customer_Build_Revision"].ToString();
                    }
                    break;

                case "*CSTID":
                    //variables_Value = CustomerIDLookup(objkl.ConfigurationID);
                    break;

                case "*SITEID":
                    //variables_Value = Site_ID_Lookup(objkl.ConfigurationID);
                    break;

                case "*ISOTD":
                    variables_Value = objDT.EffectiveDate.ToString("yyyy-MM-dd");
                    break;

                case "*MDYDT":
                    variables_Value = objDT.EffectiveDate.ToString("MMddyy");
                    break;

                case "*YMDDT":
                    variables_Value = objDT.EffectiveDate.ToString("yyMMdd");
                    break;

                case "*SYSDT":
                    variables_Value = DateTime.Now.ToString("yy") + DateTime.Now.DayOfYear.ToString("yy");
                    break;

                case "*SYSTM":
                    variables_Value = DateTime.Now.ToString("HHmmss");
                    break;

                case "*PBLDID":
                    DataTable DTmfgBHeader = DA.mfgBuildHeader(objkl.Manufacturing_Build_ID);

                    int AuMfgB = DTmfgBHeader.Rows.Count;

                    if (AuMfgB > 0)
                    {
                        DataTable DTBuildHCBuild =  DA.BuildHeaderCBuild(DTmfgBHeader.Rows[0]["Configuration_ID"].ToString());
                        int AuBHC = DTBuildHCBuild.Rows.Count;
                        if (AuBHC > 0)
                        {
                            DataTable DTBuildStandard = DA.BuildStandard(DTBuildHCBuild.Rows[0]["Customer_Build_ID"].ToString());
                            int AuBs = DTBuildStandard.Rows.Count;
                            if (AuBs > 0)
                            {
                                variables_Value = DTBuildStandard.Rows[0]["Catalog_Number"].ToString();
                                variables_Value = variables_Value + "_" + objkl.Manufacturing_Build_Instance_ID;
                            }
                            else
                            {
                                variables_Value = DTBuildHCBuild.Rows[0]["Customer_Build_ID"].ToString();
                            }
                        }
                    }
                    else
                    {
                        DataTable DTBuildHCBuild2 = DA.BuildHeaderCBuild(objkl.ConfigurationID);

                        int AuBHC2 = DTBuildHCBuild2.Rows.Count;
                        if (AuBHC2 > 0)
                        {
                            variables_Value = DTBuildHCBuild2.Rows[0]["Customer_Build_ID"].ToString();
                        }
                    }
                    break;
            }
            return variables;
        }
    }

    public enum TransferType { Build, Order, Input, Output }
    public class DataEventDataTransfers
    {

        public TransferType TransferT { get; set; }

        private string _TransferType;
        public string TransferType
        {
            get { return _TransferType; }
            set { _TransferType = value; }
        }

        private string _PropertyVID;
        public string PropertyVID
        {
            get { return _PropertyVID; }
            set { _PropertyVID = value; }
        }

        private string _PropertyID;
        public string PropertyID
        {
            get { return _PropertyID; }
            set { _PropertyID = value; }
        }

        private string _EventTypeID;
        public string EventTypeID
        {
            get { return _EventTypeID; }
            set { _EventTypeID = value; }
        }

        private string _EventTypeValueID;
        public string EventTypeValueID
        {
            get { return _EventTypeValueID; }
            set { _EventTypeValueID = value; }
        }


        private string _EventRelevance;
        public string EventRelevance
        {
            get { return _EventRelevance; }
            set { _EventRelevance = value; }
        }

        private string _TransferKey1;
        public string TransferKey1
        {
            get { return _TransferKey1; }
            set { _TransferKey1 = value; }
        }

        private string _TransferKey2;
        public string TransferKey2
        {
            get { return _TransferKey2; }
            set { _TransferKey2 = value; }
        }

        private string _TransferKey3;
        public string TransferKey3
        {
            get { return _TransferKey3; }
            set { _TransferKey3 = value; }
        }

        private string _TransferKey4;
        public string TransferKey4
        {
            get { return _TransferKey4; }
            set { _TransferKey4 = value; }
        }

        private string _LocalRemoteDataPathValue;
        public string LocalRemoteDataPathValue
        {
            get { return _LocalRemoteDataPathValue; }
            set { _LocalRemoteDataPathValue = value; }
        }

        private string _LocalRemoteDataName;
        public string LocalRemoteDataName
        {
            get { return _LocalRemoteDataName; }
            set { _LocalRemoteDataName = value; }
        }

        private string _TransferMethod;
        public string TransferMethod
        {
            get { return _TransferMethod; }
            set { _TransferMethod = value; }
        }

        private DateTime _EffectiveDate;
        public DateTime EffectiveDate
        {
            get { return _EffectiveDate; }
            set { _EffectiveDate = value; }
        }
    }
}
