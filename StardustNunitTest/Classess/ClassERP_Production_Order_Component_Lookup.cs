﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardustNunitTest.DataAccess;

namespace StardustNunitTest.Classess
{
    public class ClassERP_Production_Order_Component_Lookup
    {
        DataAccesscGF DA = new DataAccesscGF();
        public List<ERPPrdOrderComponentLookup> ERPProductionOrderComponentLookup(String ProductionOrderNumber, int InstanceIndex)
        {
            DataTable DtErpComponentLook = (DA.ERPProductionOrderComponentLookup(ProductionOrderNumber, InstanceIndex));
            List<ERPPrdOrderComponentLookup> parts = new List<ERPPrdOrderComponentLookup>();

            int intAu = DtErpComponentLook.Rows.Count;
            if (intAu > 0)
            {
                foreach (DataRow row in DtErpComponentLook.Rows)
                {
                    var singleComponent = new ERPPrdOrderComponentLookup();
                    singleComponent.CatalogNumber = row["Catalog_Number"].ToString();
                    singleComponent.Quantity = row["Quantity"].ToString();
                    singleComponent.Quantity_UOM = row["Quantity_UOM"].ToString();

                    parts.Add(singleComponent);

                }
            }

            return parts;
        }
    }
    public class ERPPrdOrderComponentLookup
    {

        private string _CatalogNumber;
        public string CatalogNumber
        {
            get { return _CatalogNumber; }
            set { _CatalogNumber = value; }
        }

        private string _Quantity;
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        private string _Quantity_UOM;
        public string Quantity_UOM
        {
            get { return _Quantity_UOM; }
            set { _Quantity_UOM = value; }
        }



    }
}
