﻿using StardustNunitTest.DataAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StardustNunitTest.Classess
{
    public class ClassEvent_Type_Value_Lookup
    {
        DataAccesscGF DA = new DataAccesscGF();
        public DataTable Event_Type_Value_Lookup(ENUMDetailLookup objEN)
        {
            DataTable DtEnumLookup = (DA.Event_Type_Value_Lookup(objEN));

            ENUMDetailLookup objENP = new ENUMDetailLookup();
            int intAu = DtEnumLookup.Rows.Count;
            if (intAu > 0)
            {
                objENP.Status = EventReturnStatus.Event_Type_Found;
            }
            else
            {
                objENP.Status = EventReturnStatus.Event_Type_Not_Found;
            }

            return DtEnumLookup;
        }
    }
}
