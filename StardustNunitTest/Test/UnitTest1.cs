using StardustNunitTest.Classess;
using System.Data;
using Cale = StardustNunitTest.Classess.ClassAdd_Audit_Log_Entry;
using CBCL = StardustNunitTest.Classess.ClassBuild_Component_Lookup;
using CBKL = StardustNunitTest.Classess.ClassBuild_Key_Lookup;
using CCIL = StardustNunitTest.Classess.ClassCategory_ID_Lookup;
using CCNL = StardustNunitTest.Classess.ClassCatalog_Number_Lookup;
using CCoIL = StardustNunitTest.Classess.ClassConfiguration_ID_Lookup;
using CConIL = StardustNunitTest.Classess.ClassContact_ID_Lookup;
using CEPOCL = StardustNunitTest.Classess.ClassERP_Production_Order_Component_Lookup;
using CETL = StardustNunitTest.Classess.ClassEvent_Type_Lookup;
using CETVL = StardustNunitTest.Classess.ClassEvent_Type_Value_Lookup;
using CGDII = StardustNunitTest.Classess.ClassGet_Delivery_Instance_Index;
using CGPII = StardustNunitTest.Classess.ClassGet_Production_Instance_Index;
using CIL = StardustNunitTest.Classess.CategoryIDLookup;
using CNL = StardustNunitTest.Classess.CatalogNumberLookup;
using ConIL = StardustNunitTest.Classess.Contact_ID_Lookup;
using CSOII = StardustNunitTest.Classess.ClassSales_Order_Instance_Index;
using EPOCL = StardustNunitTest.Classess.ClassERP_Production_Order_Component_Lookup;
using ETL = StardustNunitTest.Classess.ENUMDetailLookup;
using KL = StardustCoreLibs.App_Code.Classes.KeyLookup;
using CCuIL = StardustNunitTest.Classess.ClassCustomer_ID_Lookup;
namespace StardustNunitTest.Test
{
    [TestFixture]

    public class Class_Add_Audit_Log_Entry
    {
        private Cale _CALE;
        [SetUp]
        public void Setup()
        {
            _CALE = new Cale();
        }

        [Test]
        //---------|--Function---------|--Scenario-----|---Expected---
        public void Add_Audit_Log_Entry_Event_Type_ID_2_ExpectedInsert()
        {
            var result = _CALE.Add_Audit_Log_Entry("2");
            Assert.That(result, Is.True);
        }

        [Test]
        //---------|--Function---------|--Scenario-----|---Expected---
        public void Add_Audit_Log_Entry_Event_Type_ID_7_ExpectedInsert()
        {
            var result = _CALE.Add_Audit_Log_Entry("7");
            Assert.That(result, Is.False);
        }

    }
    public class Class_Build_Component_Lookup
    {
        private CBCL _CBCL;
        [SetUp]
        public void Setup()
        {
            _CBCL = new CBCL();
        }
        [Test]
        public void Build_Component_Lookup_Input_Key_CAT0722530_Expected_array()
        {
            List<ComponentArray> componentTestList = new List<ComponentArray>();
            var singleComponent = new ComponentArray();

            singleComponent.CatalogNumber = "FNAP-CBL-048EUC";
            singleComponent.MaterialTypeID = "CBL";
            singleComponent.Quantity = "2491.00";
            singleComponent.QuantityUOM = "FOT";

            componentTestList.Add(singleComponent);

           List<ComponentArray> componentArrays = _CBCL.BuildComponentLookup("CAT0722530");
            Assert.AreEqual(componentTestList, componentArrays);
        }
        [Test]
        public void Build_Component_Lookup_Input_Key_xxx_Expected_arrayempty()
        {
            List<ComponentArray> componentTestList = new List<ComponentArray>(); //EMPTY
            
            List<ComponentArray> componentArrays = _CBCL.BuildComponentLookup("xxx");
            Assert.AreEqual(componentTestList, componentArrays);
        }

    }
    public class Class_Build_Key_Lookup
    {
        private CBKL _CBKL;
        private KL _KL;
        [SetUp]
        public void Setup()
        {
            _CBKL = new CBKL();
            _KL = new KL();
        }
        [Test]
        public void Build_Key_Lookup_Input_Key_CAT0722530_Expected_ConfigurationID_CAT0722530()
        {

            _KL = _CBKL.BuildKeyLookup("CAT0722530");
            Assert.AreEqual("CAT0722530", _KL.ConfigurationID);
        }
        [Test]
        public void Build_Key_Lookup_Input_Key_BAL0005664_Expected_ConfigurationID_BAL0005664_ManufacturingBuildID_BAL0005664()
        {
            //_KL = _CBKL.BuildKeyLookup("BAL0005664");
            //Assert.AreEqual("BAL0005664", _KL.ConfigurationID);
            //Assert.AreEqual("BAL0005664", _KL.Manufacturing_Build_ID);
            _KL = _CBKL.BuildKeyLookup("CAT0659182");
            Assert.AreEqual("CAT0659182", _KL.ConfigurationID);
            Assert.AreEqual("CAT0659182", _KL.Manufacturing_Build_ID);
        }
        [Test]
        public void Build_Key_Lookup_Input_Key_BCA0000056_Expected_ConfigurationID_BCA0000056_ManufacturingBuildID_BCA0000056_ManufacturingBuildInstanceID_BCA0000056()
        {
            //_KL = _CBKL.BuildKeyLookup("BCA0000056");
            //Assert.AreEqual("BCA0000056", _KL.ConfigurationID);
            //Assert.AreEqual("BCA0000056", _KL.Manufacturing_Build_ID);
            //Assert.AreEqual("BCA0000056", _KL.Manufacturing_Build_Instance_ID);
            _KL = _CBKL.BuildKeyLookup("VZN0150966");
            Assert.AreEqual("VZN0150966", _KL.ConfigurationID);
            Assert.AreEqual("VZN0150966", _KL.Manufacturing_Build_ID);
            Assert.AreEqual("VZN0150966", _KL.Manufacturing_Build_Instance_ID);
        }
        [Test]
        public void Build_Key_Lookup_Input_Key_xxx_Expected_Empty()
        {
            _KL = _CBKL.BuildKeyLookup("xxx");
            Assert.AreEqual(null, _KL.ConfigurationID);
            Assert.AreEqual(null, _KL.Manufacturing_Build_ID);
            Assert.AreEqual(null, _KL.Manufacturing_Build_Instance_ID);
        }
    }
    public class Class_Catalog_Number_Lookup
    {
        private CCNL _CCNL;
        private CNL _CNL;
        [SetUp]
        public void Setup()
        {
            _CCNL = new CCNL();
            _CNL = new CNL();
        }
        [Test]
        public void Catalog_Number_Lookup_InputKey_CAT0722530_Expected_CatalogNumber_BLDCCS1116606_Status_Successful()
        {
            _CNL = _CCNL.Catalog_Number_Lookup("CAT0722530", DateTime.Now);
            Assert.AreEqual("BLD-CCS1116606", _CNL.BuildCatalogNumber);
            Assert.AreEqual("Successful", _CNL.returnstatuscatalogl.ToString());
        }
        [Test]
        public void Catalog_Number_Lookup_InputKey_CCS0001799_Expected_CatalogNumber_P86B48EV4200FP_Status_Successful()
        {
            _CNL = _CCNL.Catalog_Number_Lookup("ECE0000344", DateTime.Now);
            Assert.AreEqual("BLD-CCS1046316", _CNL.BuildCatalogNumber);
            Assert.AreEqual("Successful", _CNL.returnstatuscatalogl.ToString());
        }
        [Test]
        public void Catalog_Number_Lookup_InputKey_xxx_Expected_CatalogNumber__Status_Unsuccessful()
        {
            _CNL = _CCNL.Catalog_Number_Lookup("xxx", DateTime.Now);
            Assert.AreEqual(string.Empty, _CNL.BuildCatalogNumber);
            Assert.AreEqual("Unsuccessful", _CNL.returnstatuscatalogl.ToString());
        }
        [Test]
        public void Catalog_Number_Lookup_InputKey__Expected_CatalogNumber__Status_Unsuccessful()
        {
            _CNL = _CCNL.Catalog_Number_Lookup("", DateTime.Now);
            Assert.AreEqual(string.Empty, _CNL.BuildCatalogNumber);
            Assert.AreEqual("Unsuccessful", _CNL.returnstatuscatalogl.ToString());
        }
    }
    public class Class_Category_ID_Lookup
    {
        private CCIL _CCIL;
        private CIL _CIL;
        [SetUp]
        public void Setup()
        {
            _CCIL = new CCIL();
            _CIL = new CIL();
        }
        [Test]
        public void Category_ID_Lookup_Category_Description_Location_Expected_Category_ID_LOC_Status_Category_Found()
        {
            _CIL = _CCIL.Category_ID_Lookup("Location", DateTime.Now);
            Assert.AreEqual("LOC", _CIL.CategoryID);
            Assert.AreEqual("Category_Found", _CIL.Status.ToString());
        }
        [Test]
        public void Category_ID_Lookup_Category_Description_xxx_Expected_Category_ID_LOC_Status_Category_Not_Found()
        {
            _CIL = _CCIL.Category_ID_Lookup("xxx", DateTime.Now);
            Assert.AreEqual(null, _CIL.CategoryID);
            Assert.AreEqual("Category_Not_Found", _CIL.Status.ToString());
        }
    }
    public class Class_Contact_ID_Lookup
    {
        private CConIL _CConIL;
        private ConIL _ConIL;
        [SetUp]
        public void Setup()
        {
            _CConIL = new CConIL();
            _ConIL = new ConIL();
        }
        [Test]
        public void Contact_ID_Lookup_Email_Expected_Contact_ID_007474()
        {
            _ConIL.ContactID = _CConIL.ContactIDLookup("barth.hopper@corning.com", "", "");
            Assert.AreEqual("007474", _ConIL.ContactID);
        }
        //[Test]
        //public void Contact_ID_Lookup_Name_Test_Email_Phone_828-555-1212_Expected_New_Row()
        //{
        //    _ConIL.ContactID = _CConIL.ContactIDLookup("x@y.com", "828-555-1212", "Test");
        //    Assert.AreEqual("007474", _ConIL.ContactID);
        //}
    }
    public class Class_Configuration_ID_Lookup
    {
        private CCoIL _CCoIL;
        [SetUp]
        public void Setup()
        {
            _CCoIL = new CCoIL();
        }
        [Test]
        public void Configuration_ID_Lookup_InputKey_CAT0722530_Expected_Configuration_ID_CAT0722530()
        {
            var result = _CCoIL.ConfigurationIDLookup("CAT0722530");
            Assert.AreEqual("CAT0722530", result);
        }
        [Test]
        public void Configuration_ID_Lookup_InputKey_XXX_Expected_Configuration_ID_()
        {
            var result = _CCoIL.ConfigurationIDLookup("XXX");
            Assert.AreEqual(null, result);
        }
    }
    public class Class_Customer_ID_Lookup
    {
        private CCuIL _CCuIL;
        [SetUp]
        public void Setup()
        {
            _CCuIL = new CCuIL();
        }
        [Test]
        public void Customer_ID_Lookup_InputKey_CAT0722530_Expected_Customer_ID_CAT()
        {
            var result = _CCuIL.Customer_ID_Lookup("CAT0722530");
            Assert.AreEqual("CAT", result);
        }
        [Test]
        public void Customer_ID_Lookup_InputKey_XXX_Expected_Customer_ID_()
        {
            var result = _CCuIL.Customer_ID_Lookup("XXX");
            Assert.AreEqual(null, result);
        }
    }
    //public class Class_Data_Event_Data_Transfer
    //{

    //    private CDEDT _CDEDT;
    //    private DEDT _DEDT;
    //    [SetUp]
    //    public void Setup()
    //    {
    //        _CDEDT = new CDEDT();
    //        _DEDT = new DEDT();
    //    }
    //    [Test]
    //    public void Datai_Event_Data_Transfer_EventTypeValueID_36_TransferType_1_Expected_Calls_to_ExternalShippedandSAPDelivery()
    //    {
    //        //PlaceHolder
    //    }
    //}
    public class Class_ERP_Production_Order_Component_Lookup
    {
        private CEPOCL _CEPOCL;
        private EPOCL _EPOCL;
        [SetUp]
        public void Setup()
        {
            _CEPOCL = new CEPOCL();
            _EPOCL = new EPOCL();
        }
        [Test]
        public void ERP_Production_Order_Component_Lookup_ProductionOrderNumber_9005385242_Instanceindez_1_Expected_Array()
        {
            List<ERPPrdOrderComponentLookup> parts = new List<ERPPrdOrderComponentLookup>();

            parts = _CEPOCL.ERPProductionOrderComponentLookup("9005385242", 1);


            Assert.IsNotEmpty(parts);
        }
        [Test]
        public void ERP_Production_Order_Component_Lookup_ProductionOrderNumber_xxx_Instanceindez_1_Expected_Array()
        {
            List<ERPPrdOrderComponentLookup> parts1 = new List<ERPPrdOrderComponentLookup>();

            parts1 = _CEPOCL.ERPProductionOrderComponentLookup("xxx", 1);


            Assert.IsEmpty(parts1);
        }
    }
    public class Class_Event_Type_Lookup
    {
        private CETL _CETL;
        private ETL _ETL;
        [SetUp]
        public void Setup()
        {
            _CETL = new CETL();
            _ETL = new ETL();
        }
        [Test]
        public void EventTypeLookup_EventTypeID_2_Expect_2_Build_Build_Header_True()
        {
            _ETL.EventTypeID = "2";

            DataTable DtETL = _CETL.Event_Type_Lookup(_ETL);

            Assert.AreEqual("2", DtETL.Rows[0]["Event_Type_ID"].ToString());
            Assert.AreEqual("Build", DtETL.Rows[0]["Event_Type_Description"].ToString());
            Assert.AreEqual("Build_Header", DtETL.Rows[0]["Event_Type_Data_Table"].ToString());
            Assert.AreEqual("True", DtETL.Rows[0]["Audit_Event"].ToString());

        }
        [Test]
        public void EventTypeLookup_EventTypeID_0_Expect_Empty()
        {
            _ETL.EventTypeID = "0";

            DataTable DtETL = _CETL.Event_Type_Lookup(_ETL);


            Assert.AreEqual(null, DtETL.Container);

        }
    }
    public class Class_Event_Type_Value_Lookup
    {
        private CETVL _CETVL;
        private ETL _ETL;
        [SetUp]
        public void Setup()
        {
            _CETVL = new CETVL();
            _ETL = new ETL();
        }
        [Test]
        public void EventTypeValueLookup_EventTypeID_2__EventTypeValueID_1_Expect_2_1_Active()
        {
            _ETL.EventTypeID = "2";
            _ETL.Event_Type_Value_ID = "1";

            DataTable DtETL = _CETVL.Event_Type_Value_Lookup(_ETL);

            Assert.AreEqual("2", DtETL.Rows[0]["Event_Type_ID"].ToString());
            Assert.AreEqual("1", DtETL.Rows[0]["Event_Type_Value_ID"].ToString());
            Assert.AreEqual("Active", DtETL.Rows[0]["Event_Type_Value_Description"].ToString());
        }
        [Test]
        public void EventTypeValueLookup_EventTypeID_2_EventTypeValueID_0_Expect_null()
        {
            _ETL.EventTypeID = "2";
            _ETL.Event_Type_Value_ID = "0";

            DataTable DtETL = _CETVL.Event_Type_Value_Lookup(_ETL);

            Assert.AreEqual(null, DtETL.Container);
        }
    }
    //public class Generate_Measurement_Text
    //{
    //    private CGMT _CGMT;
    //    [SetUp]
    //    public void Setup()
    //    {
    //        _CGMT = new CGMT();
    //    }
    //    [Test]
    //    public void EventTypeValueLookup_EventTypeID_2__EventTypeValueID_1_Expect_2_1_Active()
    //    {

    //        var a = _CGMT.Generate_Measurement_Text(15.75, 1, "***");

    //    }
    //}
    public class Class_Get_Delivery_Instance_Index
    {
        private CGDII _CGDII;
        [SetUp]
        public void Setup()
        {
            _CGDII = new CGDII();
        }
        [Test]
        public void GetDeliveryInstanceIndex_DeliveryNumber_200350372_CustomerNumber_205941_Expect_InstanceIndex_1()
        {
            int Result = _CGDII.GetDeliveryInstanceIndex("200350372", "205941");

            Assert.AreEqual(1, Result);
        }
        [Test]
        public void GetDeliveryInstanceIndex_DeliveryNumber_111111_CustomerNumber_111111_Expect_InstanceIndex_1()
        {
            int Result = _CGDII.GetDeliveryInstanceIndex("111111", "111111");

            Assert.AreEqual(1, Result);
        }
        [Test]
        public void GetDeliveryInstanceIndex_DeliveryNumber_200350372_CustomerNumber_111111_Expect_InstanceIndex_2()
        {
            int Result = _CGDII.GetDeliveryInstanceIndex("200350372", "111111");

            Assert.AreEqual(2, Result);
        }
    }
    public class Class_Get_Production_Order_Instance_Index
    {
        private CGPII _CGPII;
        [SetUp]
        public void Setup()
        {
            _CGPII = new CGPII();
        }
        [Test]
        public void GetProductionInstanceIndex_DeliveryNumber_9005359109_CatalogNumber_BLDCCS0534608_Expect_InstanceIndex_1()
        {
            int Result = _CGPII.GetProductionInstanceIndex("9005359109", "BLD-CCS0534608");

            Assert.AreEqual(1, Result);
        }
        [Test]
        public void GetProductionInstanceIndex_DeliveryNumber_111111_CatalogNumber_BLDX_Expect_InstanceIndex_1()
        {
            int Result = _CGPII.GetProductionInstanceIndex("111111", "BLD-X");

            Assert.AreEqual(1, Result);
        }
        [Test]
        public void GetProductionInstanceIndex_DeliveryNumber_9005359109_CatalogNumber_BLDX_Expect_InstanceIndex_1()
        {
            int Result = _CGPII.GetProductionInstanceIndex("9005359109", "BLD-X");

            Assert.AreEqual(2, Result);
        }
    }
    public class Class_Get_Sales_Order_Instance_Index
    {
        private CSOII _CSOII;
        [SetUp]
        public void Setup()
        {
            _CSOII = new CSOII();
        }
        [Test]
        public void GetSalesOrderInstanceIndex_SalesOrderNumber_187098280_CustomerID_219823_Expect_InstanceIndex_1()
        {
            int Result = _CSOII.GetSalesOrderInstanceIndex("187098280", "219823");

            Assert.AreEqual(1, Result);
        }
        [Test]
        public void GetSalesOrderInstanceIndex_SalesOrderNumber_111111_CustomerID_111111_Expect_InstanceIndex_1()
        {
            int Result = _CSOII.GetSalesOrderInstanceIndex("111111", "111111");

            Assert.AreEqual(1, Result);
        }
        [Test]
        public void GetSalesOrderInstanceIndex_SalesOrderNumber_187098280_CustomerID_111111_Expect_InstanceIndex_1()
        {
            int Result = _CSOII.GetSalesOrderInstanceIndex("187098280", "111111");

            Assert.AreEqual(2, Result);
        }
    }
}