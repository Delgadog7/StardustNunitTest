﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using NLog.Internal;
using Microsoft.IdentityModel.Protocols;
using StardustNunitTest.Classess;
using System.Runtime.InteropServices;
using System.Reflection;
using StardustCoreLibs.App_Code.Classes;

namespace StardustNunitTest.DataAccess
{

    public class DataAccesscGF
    {
        public SqlConnection Conect()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=RYAI1SQL2007;Initial Catalog=Stardust;Persist Security Info=True;User ID=shopfloorit;Password=c0rn1ng";

            return conn;
        }

            
        public DataTable Lookup_Audit_Log_Entry(string AuditKey1, string AuditKey2, string AuditKey3, string AuditKey4, string Event_Type_ID, string EventTypeValueID, string UserID, string AuditData)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Audit_Key_1", SqlDbType.VarChar).Value = AuditKey1;
                cmd.Parameters.AddWithValue("@Audit_Key_2", SqlDbType.VarChar).Value = AuditKey2;
                cmd.Parameters.AddWithValue("@Audit_Key_3", SqlDbType.VarChar).Value = AuditKey3;
                cmd.Parameters.AddWithValue("@Audit_Key_4", SqlDbType.VarChar).Value = AuditKey4;
                cmd.Parameters.AddWithValue("@Event_Type_ID", SqlDbType.VarChar).Value = Event_Type_ID;
                cmd.Parameters.AddWithValue("@Event_Type_Value_ID", SqlDbType.VarChar).Value = EventTypeValueID;
                cmd.Parameters.AddWithValue("@User_ID", SqlDbType.VarChar).Value = UserID;
                cmd.Parameters.AddWithValue("@Audit_Data", SqlDbType.VarChar).Value = AuditData;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Lookup_Audit_Log_Entry";
                return GetData(cmd);
            }
        }

        public DataTable Build_Instance_Header(String InputKey)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Input_Key", SqlDbType.VarChar).Value = InputKey;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Mfg_Build_Instance_Header";
                return GetData(cmd);
            }
        }
        public DataTable MFG_Build_Header(String LocalManBuildID)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LocalManBuildID", SqlDbType.VarChar).Value = LocalManBuildID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Mfg_Build_Header";
                return GetData(cmd);
            }
        }
        public DataTable Build_Header(String LocalConfID)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LocalConfID", SqlDbType.VarChar).Value = LocalConfID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Build_Header";
                return GetData(cmd);
            }
        }
        public DataTable CatalogNumberLookupBH(string ConfigurationID)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = ConfigurationID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Catalog_Number_Lookup_BuildH";
                return GetData(cmd);
            }
        }
        public DataTable CatalogNumberLookupBStEf(String Customer_BuildID, DateTime Effective_Date)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@category_description", SqlDbType.VarChar).Value = Customer_BuildID;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.DateTime).Value = Effective_Date;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Catalog_Number_Lookup_BuildStEf";
                return GetData(cmd);
            }
        }
        public DataTable CategoryIDLookup(String Category_Description, DateTime Effective_Date)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@category_description", SqlDbType.VarChar).Value = Category_Description;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.DateTime).Value = Effective_Date;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Category_ID_Lookup";
                return GetData(cmd);
            }
        }
        public DataTable ContactIDLookup(Contact_ID_Lookup ObjCt)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Contact_Email", SqlDbType.VarChar).Value = ObjCt.ContactEmail;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ContactIDLookup";
                return GetData(cmd);
            }
        }
        public DataTable InsertContactIDLookup(Contact_ID_Lookup ObjCt)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Contact_Email", SqlDbType.VarChar).Value = ObjCt.ContactEmail;
                cmd.Parameters.AddWithValue("@Contact_Name", SqlDbType.VarChar).Value = ObjCt.ContactName;
                cmd.Parameters.AddWithValue("@Contact_Phone", SqlDbType.VarChar).Value = ObjCt.ContactPhone;
                cmd.Parameters.AddWithValue("@Serial_Number", SqlDbType.VarChar).Value = ObjCt.SerialNumber;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "InsertContactIDLookup";
                return GetData(cmd);
            }
        }
        public DataTable Serial_Number_Header_RS(DateTime dateTimeEfe, string Serial)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.DateTime).Value = dateTimeEfe;
                cmd.Parameters.AddWithValue("@Serial_Number_Set_ID", SqlDbType.VarChar).Value = Serial;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Serial_Number_Header_RS";
                return GetData(cmd);
            }
        }
        public DataTable Serial_Number_Detail_RS(SerialGenerator objSG)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Serial_Number_Set_ID", SqlDbType.VarChar).Value = objSG.SerialNumberSetID;
                cmd.Parameters.AddWithValue("@Serial_Number_Serial_Number_Prefix", SqlDbType.VarChar).Value = objSG.SerialNumberSerialNumberPrefix;
                cmd.Parameters.AddWithValue("@Serial_Number_Serial_Number_Suffix", SqlDbType.VarChar).Value = objSG.SerialNumberSerialNumberSuffix;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Serial_Number_Detail_RS";
                return GetData(cmd);
            }
        }
        public DataTable DataTransferHeaderEffv(DataEventDataTransfers objDT)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Data", SqlDbType.VarChar).Value = "1"; //Data
                cmd.Parameters.AddWithValue("@EventTypeValueID", SqlDbType.VarChar).Value = objDT.EventTypeValueID;
                cmd.Parameters.AddWithValue("Event_Relevance", SqlDbType.VarChar).Value = "2"; //System
                cmd.Parameters.AddWithValue("@TransferType", SqlDbType.VarChar).Value = objDT.TransferType;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "DataTransferHeaderEffv";
                return GetData(cmd);
            }
        }
        public DataTable DataTransferHeader(DataEventDataTransfers objDT)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.DateTime).Value = objDT.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.DateTime).Value = objDT.PropertyVID;
                cmd.Parameters.AddWithValue("@Event_Type_ID", SqlDbType.DateTime).Value = objDT.EventTypeID;
                cmd.Parameters.AddWithValue("@Event_Type_Value_ID", SqlDbType.DateTime).Value = objDT.EventTypeValueID;
                cmd.Parameters.AddWithValue("@Event_Relevance", SqlDbType.DateTime).Value = objDT.EventRelevance;
                cmd.Parameters.AddWithValue("@Transfer_Type", SqlDbType.DateTime).Value = objDT.TransferT;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.VarChar).Value = objDT.EffectiveDate;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Data_TransferHeader";
                return GetData(cmd);
            }
        }
        public DataTable ProcessTransferDetail(String TransferID, DateTime EffectiveDate)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.DateTime).Value = EffectiveDate;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = TransferID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ProcessTransferDetail";
                return GetData(cmd);
            }
        }
        public DataTable CustomerBuildID(String ConfigurationID)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = ConfigurationID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "CustomerBuildID";
                return GetData(cmd);
            }
        }
        public DataTable CustomerBuildRevision(String ConfigurationID)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = ConfigurationID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "CustomerBuildRevision";
                return GetData(cmd);
            }
        }
        public DataTable mfgBuildHeader(String BuildID)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Build_ID", SqlDbType.VarChar).Value = BuildID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "mfgBuildHeader";
                return GetData(cmd);
            }
        }

        public DataTable BuildStandard(String BuildID)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Build_ID", SqlDbType.VarChar).Value = BuildID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "BuildStandard";
                return GetData(cmd);
            }
        }
        public DataTable BuildHeaderCBuild(String ConfigurationID)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Configuration_ID", SqlDbType.VarChar).Value = ConfigurationID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "BuildHeaderCBuild";
                return GetData(cmd);
            }
        }
        public DataTable ERPProductionOrderComponentLookup(String ProductionOrderNumber, int InstanceIndex)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProductionOrderNumber", SqlDbType.VarChar).Value = ProductionOrderNumber;
                cmd.Parameters.AddWithValue("@InstanceIndex", SqlDbType.VarChar).Value = InstanceIndex;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ERPProductionOrderComponentLookup";
                return GetData(cmd);
            }
        }
        public DataTable Event_Type_Lookup(ENUMDetailLookup objEN)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Event_Type_ID", SqlDbType.VarChar).Value = objEN.EventTypeID;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Event_Type_Lookup";
                return GetData(cmd);
            }
        }
        public DataTable Event_Type_Value_Lookup(ENUMDetailLookup objEN)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Event_Type_ID", SqlDbType.VarChar).Value = objEN.EventTypeID;
                cmd.Parameters.AddWithValue("@Event_Type_Value_ID", SqlDbType.VarChar).Value = objEN.Event_Type_Value_ID;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Event_Type_Value_Lookup";
                return GetData(cmd);
            }
        }
        public DataTable ERPDelivery1RS(string DeliveryNumber, string CustomerNumber)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Delivery_Number", SqlDbType.VarChar).Value = DeliveryNumber;
                cmd.Parameters.AddWithValue("@Customer_Number", SqlDbType.Int).Value = CustomerNumber;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ERP_Delivery_1_RS";
                return GetData(cmd);
            }
        }
        public DataTable ERPDelivery2RS(string DeliveryNumber)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Delivery_Number", SqlDbType.VarChar).Value = DeliveryNumber;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ERP_Delivery_2_RS";
                return GetData(cmd);
            }
        }
        public DataTable ERPProductionOrder1RS(string ProductionOrderNumber, string CatalogNumber)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Production_Order_Number", SqlDbType.VarChar).Value = ProductionOrderNumber;
                cmd.Parameters.AddWithValue("@Catalog_Number", SqlDbType.Int).Value = CatalogNumber;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ERP_Production_Order_1_RS";
                return GetData(cmd);
            }
        }
        public DataTable ERPProductionOrder2RS(string ProductionOrderNumber)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Production_Order_Number", SqlDbType.VarChar).Value = ProductionOrderNumber;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ERP_Production_Order_2_RS";
                return GetData(cmd);
            }
        }
        public DataTable ERPSalesOrder1RS(string ProductionOrderNumber, string CatalogNumber)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Sales_Order_Number", SqlDbType.VarChar).Value = ProductionOrderNumber;
                cmd.Parameters.AddWithValue("@Customer_ID", SqlDbType.Int).Value = CatalogNumber;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ERP_Sales_Order_1_RS";
                return GetData(cmd);
            }
        }
        public DataTable ERPSalesOrder2RS(string ProductionOrderNumber)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Sales_Order_Number", SqlDbType.VarChar).Value = ProductionOrderNumber;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "ERP_Sales_Order_2_RS";
                return GetData(cmd);
            }
        }

        public DataTable PropertyIDLookupByName(String PropertyKey, DateTime EffectiveDate)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PartnerOrderNumber", SqlDbType.VarChar).Value = PropertyKey;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "PropertyIDLookupByName";
                return GetData(cmd);
            }
        }
        public DataTable PropertyIDLookupByDescLD(String PropertyKey, DateTime EffectiveDate)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PartnerOrderNumber", SqlDbType.VarChar).Value = PropertyKey;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "PropertyIDLookupByDescLD";
                return GetData(cmd);
            }
        }
        public DataTable PropertyIDLookupByDescSD(String PropertyKey, DateTime EffectiveDate)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PartnerOrderNumber", SqlDbType.VarChar).Value = PropertyKey;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "PropertyIDLookupByDescSD";
                return GetData(cmd);
            }
        }

        public DataTable CheckCategory(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.VarChar).Value = objPVL.EffectiveDate;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "CheckCategory";
                return GetData(cmd);
            }
        }

        public DataTable CheckProperty(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.VarChar).Value = objPVL.EffectiveDate;
                cmd.Parameters.AddWithValue("@PropertyID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "CheckCategory";
                return GetData(cmd);
            }
        }
        public DataTable CheckCategoryPropertyXRef(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.VarChar).Value = objPVL.EffectiveDate;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@PropertyID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "CheckCategory";
                return GetData(cmd);
            }
        }
        public DataTable GetMfgBuildProperty(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Manufacturing_Build_ID", SqlDbType.VarChar).Value = objPVL.ManufacturingBuildID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Get_Mfg_Build_Property";
                return GetData(cmd);
            }
        }
        public DataTable Check_For_Function(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.VarChar).Value = objPVL.EffectiveDate;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Check_For_Function";
                return GetData(cmd);
            }
        }
        public DataTable CheckOverride(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.VarChar).Value = objPVL.EffectiveDate;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "CheckOverride";
                return GetData(cmd);
            }
        }
        public DataTable Rule_Proccesing_Header(RuleProcessing objR)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@local_Effective_Date", SqlDbType.VarChar).Value = objR.EffectiveDate;
                cmd.Parameters.AddWithValue("@Rule_Set_ID", SqlDbType.VarChar).Value = objR.RuleSetID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Rule_Proccesing_Header";
                return GetData(cmd);
            }
        }
        public DataTable GetBuildProperty_ByBuild(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "GetBuildProperty_ByBuild";
                return GetData(cmd);
            }
        }

        public DataTable GetBuildProperty_ByLocation(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "GetBuildProperty_ByLocation";
                return GetData(cmd);
            }
        }

        public DataTable GetBuildProperty_ByTap(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "GetBuildProperty_ByTap";
                return GetData(cmd);
            }
        }

        public DataTable GetBuildProperty_ByTether(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "GetBuildProperty_ByTether";
                return GetData(cmd);
            }
        }

        public DataTable GetBuildProperty_ByFiber(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "GetBuildProperty_ByFiber";
                return GetData(cmd);
            }
        }
        public DataTable GetCategoryType(PropertyValueLookup ObjPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = ObjPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Effective_Date", SqlDbType.DateTime).Value = ObjPVL.EffectiveDate;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "GetCategoryType";
                return GetData(cmd);
            }
        }
        public DataTable BuildProperty_Insert(PropertyValueLookup ObjPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = ObjPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = ObjPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = ObjPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = ObjPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "BuildProperty_Insert";
                return GetData(cmd);
            }
        }

        public DataTable BuildProperty_Update(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "BuildProperty_Update";
                return GetData(cmd);
            }
        }



        public DataTable BuildProperty_Delete(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "BuildProperty_Delete";
                return GetData(cmd);
            }
        }

        public DataTable LocationProperty_Insert(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "LocationProperty_Insert";
                return GetData(cmd);
            }
        }

        public DataTable TapProperty_Insert(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@TapSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "TapProperty_Insert";
                return GetData(cmd);
            }
        }

        public DataTable TapProperty_Update(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@TapSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "TapProperty_Update";
                return GetData(cmd);
            }
        }

        public DataTable TapProperty_Delete(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@TapSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "TapProperty_Delete";
                return GetData(cmd);
            }
        }

        public DataTable LocationProperty_Update(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "LocationProperty_Update";
                return GetData(cmd);
            }
        }


        public DataTable LocationProperty_Delete(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "LocationProperty_Delete";
                return GetData(cmd);
            }
        }

        public DataTable TetherProperty_Insert(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@TapSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "TetherProperty_Insert";
                return GetData(cmd);
            }
        }

        public DataTable TetherProperty_Update(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@TapSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "TetherProperty_Update";
                return GetData(cmd);
            }
        }

        public DataTable TetherProperty_Delete(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@TapSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "TetherProperty_Delete";
                return GetData(cmd);
            }
        }
        public DataTable FiberProperty_Insert(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "FiberProperty_Insert";
                return GetData(cmd);
            }
        }

        public DataTable FiberProperty_Update(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "FiberProperty_Update";
                return GetData(cmd);
            }
        }

        public DataTable FiberProperty_Delete(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = objPVL.ConfigurationID;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "FiberProperty_Delete";
                return GetData(cmd);
            }
        }
        public DataTable MfgProperty_Insert(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Manufacturing_Build_ID", SqlDbType.VarChar).Value = objPVL.ManufacturingBuildID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "MfgProperty_Insert";
                return GetData(cmd);
            }
        }

        public DataTable MfgProperty_Update(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Manufacturing_Build_ID", SqlDbType.VarChar).Value = objPVL.ManufacturingBuildID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "MfgProperty_Update";
                return GetData(cmd);
            }
        }

        public DataTable MfgProperty_Delete(PropertyValueLookup objPVL)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Manufacturing_Build_ID", SqlDbType.VarChar).Value = objPVL.ManufacturingBuildID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "MfgProperty_Delete";
                return GetData(cmd);
            }
        }

        public DataTable MfgInstanceProperty_Insert(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Manufacturing_Build_Instance_ID", SqlDbType.VarChar).Value = objPVL.@ManufacturingBuildInstanceID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "MfgInstanceProperty_Insert";
                return GetData(cmd);
            }
        }


        public DataTable MfgInstanceProperty_Update(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Manufacturing_Build_Instance_ID", SqlDbType.VarChar).Value = objPVL.@ManufacturingBuildInstanceID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@PropertyValue", SqlDbType.VarChar).Value = objPVL.PropertyValue;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "MfgInstanceProperty_Update";
                return GetData(cmd);
            }
        }


        public DataTable MfgInstanceProperty_Delete(PropertyValueLookup objPVL)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Manufacturing_Build_Instance_ID", SqlDbType.VarChar).Value = objPVL.@ManufacturingBuildInstanceID;
                cmd.Parameters.AddWithValue("@LocationSequence", SqlDbType.VarChar).Value = objPVL.LocationSequence;
                cmd.Parameters.AddWithValue("@TAPSequence", SqlDbType.VarChar).Value = objPVL.TapSequence;
                cmd.Parameters.AddWithValue("@TetherSequence", SqlDbType.VarChar).Value = objPVL.TetherSequence;
                cmd.Parameters.AddWithValue("@FiberSequence", SqlDbType.VarChar).Value = objPVL.FiberSequence;
                cmd.Parameters.AddWithValue("@CategoryID", SqlDbType.VarChar).Value = objPVL.CategoryID;
                cmd.Parameters.AddWithValue("@Property_ID", SqlDbType.VarChar).Value = objPVL.PropertyID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "MfgInstanceProperty_Delete";
                return GetData(cmd);
            }
        }
        public DataTable PartnerOrderItemMaterialLookup(String PartnerOrderNumber, String PartnerID, String CustomerBuildID)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PartnerOrderNumber", SqlDbType.VarChar).Value = PartnerOrderNumber;
                cmd.Parameters.AddWithValue("@PartnerID", SqlDbType.VarChar).Value = PartnerID;
                cmd.Parameters.AddWithValue("@CustomerBuildID", SqlDbType.VarChar).Value = CustomerBuildID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Partner_Order_Item_Material_Lookup";
                return GetData(cmd);
            }
        }
        public DataTable PartnerOrderItemLookup(String PartnerOrderNumber, String PartnerID, String CustomerBuildID)
        {
            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PartnerOrderNumber", SqlDbType.VarChar).Value = PartnerOrderNumber;
                cmd.Parameters.AddWithValue("@PartnerID", SqlDbType.VarChar).Value = PartnerID;
                cmd.Parameters.AddWithValue("@CustomerBuildID", SqlDbType.VarChar).Value = CustomerBuildID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Partner_Order_Item_Lookup";
                return GetData(cmd);
            }
        }
        public DataTable BuildComponentLookup(String ConfigurationID)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = ConfigurationID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Build_Component_Lookup";
                return GetData(cmd);
            }
        }
        public DataTable Build_Location_Header(String ConfigurationID)
        {

            using (SqlCommand cmd = new SqlCommand("SP_GeneralFunctions", Conect()))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = ConfigurationID;
                cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Build_Location_Header";
                return GetData(cmd);
            }
        }
        private static DataTable GetData(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=RYAI1SQL2007;Initial Catalog=Stardust;Persist Security Info=True;User ID=shopfloorit;Password=c0rn1ng";
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = conn;
                    conn.Open();
                    sda.SelectCommand = cmd;
                    sda.Fill(dt);
                    conn.Close();
                    return dt;
                }
        }

    }
}
