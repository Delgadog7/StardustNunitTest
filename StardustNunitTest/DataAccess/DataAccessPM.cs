﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Runtime.InteropServices;
using Stardust.CoreLibs.Classes;
using StardustNunitTest.Classess;

namespace StardustNunitTest.DataAccess
{
    public class DataAccessPM
    {
        public SqlConnection Conect()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = "Data Source=RYAI1SQL2007;Initial Catalog=Stardust;Persist Security Info=True;User ID=shopfloorit;Password=c0rn1ng";

            return conn;
        } 
        //---------------------------------------------------------PRE MANUFACTURING
    internal List<I> ConvertTo<I>(DataTable datatable) where I : class
    {
        List<I> lstRecord = new List<I>();
        try
        {
            List<string> columnsNames = new List<string>();
            foreach (DataColumn DataColumn in datatable.Columns)
                columnsNames.Add(DataColumn.ColumnName);
            lstRecord = datatable.AsEnumerable().ToList().ConvertAll<I>(row => GetObject<I>(row, columnsNames));
            return lstRecord;
        }
        catch
        {
            return lstRecord;
        }

    }
    private I GetObject<I>(DataRow row, List<string> columnsName) where I : class
    {
        I obj = (I)Activator.CreateInstance(typeof(I));
        try
        {
            PropertyInfo[] Properties = typeof(I).GetProperties();
            foreach (PropertyInfo objProperty in Properties)
            {
                string columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                if (!string.IsNullOrEmpty(columnname))
                {
                    object dbValue = row[columnname];
                    if (dbValue != DBNull.Value)
                    {
                        if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                        {
                            objProperty.SetValue(obj, Convert.ChangeType(dbValue, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                        }
                        else
                        {
                            objProperty.SetValue(obj, Convert.ChangeType(dbValue, Type.GetType(objProperty.PropertyType.ToString())), null);
                        }
                    }
                }
            }
            return obj;
        }
        catch (Exception ex)
        {
            return obj;
        }
    }
    internal DataTable Build_Header([Optional] string ConfigurationID, [Optional] bool? InitialPreMFGStep)
    {
        using (SqlCommand cmd = new SqlCommand("SP_PreManufacturing", Conect()))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Build_Header";
            if (ConfigurationID != null) { cmd.Parameters.AddWithValue("@ConfigurationID", SqlDbType.VarChar).Value = ConfigurationID; }
            if (InitialPreMFGStep != null) { cmd.Parameters.AddWithValue("@InitialPreMFGStep", SqlDbType.VarChar).Value = InitialPreMFGStep; }
            return GetData(cmd);
        }
    }
    internal DataTable PartnerOrderDetail(string PartnerID, string CustomerBuildID)
    {
        using (SqlCommand cmd = new SqlCommand("SP_PreManufacturing", Conect()))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Partner_Order_Detail";
            cmd.Parameters.AddWithValue("@PartnerID", SqlDbType.VarChar).Value = PartnerID;
            cmd.Parameters.AddWithValue("@CustomerBuildID", SqlDbType.VarChar).Value = CustomerBuildID;
            cmd.Parameters.AddWithValue("@EventTypeValue", SqlDbType.VarChar).Value = "ESTE ESTARA SETTEADO EN LA BD.";
            return GetData(cmd);
        }
    }
    internal DataTable PartnerOrderItem(string Partner_ID, string Partner_Order_Number)
    {
        using (SqlCommand cmd = new SqlCommand("SP_PreManufacturing", Conect()))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "PropVal_Override_Effective";
            cmd.Parameters.AddWithValue("@PartnerID", SqlDbType.DateTime).Value = Partner_ID;
            cmd.Parameters.AddWithValue("@PartnerOrderNumber", SqlDbType.VarChar).Value = Partner_Order_Number;
            return GetData(cmd);
        }
    }
    internal PropVal_Customer_Effective PropVal_Customer_Effective(DateTime EffectiveDate, string CustomerID)
    {
        using (SqlCommand cmd = new SqlCommand("SP_PreManufacturing", Conect()))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "Get_Manufacturing_Build_Item_Property";
            cmd.Parameters.AddWithValue("@EffectiveDate", SqlDbType.DateTime).Value = EffectiveDate;
            cmd.Parameters.AddWithValue("@CustomerID", SqlDbType.VarChar).Value = CustomerID;

            List<PropVal_Customer_Effective> PropVal_Customer_Effective_RS = ConvertTo<PropVal_Customer_Effective>(GetData(cmd));
            if (PropVal_Customer_Effective_RS.Count > 0) { return PropVal_Customer_Effective_RS[0]; }
            else { return null; }
        }
    }
    public DataTable BuildPartnerOrderRequirementMatch(string CustomerBuildID)
    {
        using (SqlCommand cmd = new SqlCommand("SP_PreManufacturing", Conect()))
        {
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Option", SqlDbType.VarChar).Value = "BuildPartnerOrderRequirementMatch";
            cmd.Parameters.AddWithValue("@CustomerBuildID", SqlDbType.VarChar).Value = CustomerBuildID;


            return GetData(cmd);
        }
    }

        private static DataTable GetData(SqlCommand cmd)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = "Data Source=RYAI1SQL2007;Initial Catalog=Stardust;Persist Security Info=True;User ID=shopfloorit;Password=c0rn1ng";
        using (SqlDataAdapter sda = new SqlDataAdapter())
        {
            cmd.Connection = conn;
            conn.Open();
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            conn.Close();
            return dt;
        }
    }
    }

   
}
